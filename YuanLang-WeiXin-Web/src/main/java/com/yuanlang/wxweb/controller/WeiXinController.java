package com.yuanlang.wxweb.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author hanhq
 * @return {code:success;msg;msgContent:data}
 * @throws {如果有异常说明请填写}
 * @methodname
 * @Description 微信接入
 * @create 2018/6/9
 */
@Controller
@RequestMapping(value = "/wx")
@CrossOrigin(origins = "*", maxAge = 3600) // 设置跨域访问
public class WeiXinController {




}
