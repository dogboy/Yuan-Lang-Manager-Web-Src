package com.yuanlang.dao.mapper.system;

import com.yuanlang.facade.model.system.TsSMS;

public interface TsSMSMapper {
    int deleteByPrimaryKey(Long id);

    int insert(TsSMS record);

    int insertSelective(TsSMS record);

    TsSMS selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(TsSMS record);

    int updateByPrimaryKey(TsSMS record);
}