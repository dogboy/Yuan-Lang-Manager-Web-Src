package com.yuanlang.dao.mapper.im;

import com.yuanlang.facade.model.im.ImUser;
import java.util.List;
import java.util.Map;

public interface ImUserMapper {

    int deleteByPrimaryKey(Long id);

    int insert(ImUser record);

    int insertSelective(ImUser record);

    ImUser selectByPrimaryKey(Long id);

    List<ImUser> queryImUserListByGroupId(Long groupId);

    List<ImUser> queryImUserListByUserName(Map param);


    int updateByPrimaryKeySelective(ImUser record);

    int updateByPrimaryKey(ImUser record);
}