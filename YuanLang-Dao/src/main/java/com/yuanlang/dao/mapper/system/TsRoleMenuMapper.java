package com.yuanlang.dao.mapper.system;

import com.yuanlang.facade.model.system.TsRoleMenu;
import com.yuanlang.facade.model.system.TsUserRole;

import java.util.List;

public interface TsRoleMenuMapper {
    int deleteByPrimaryKey(Long roleMenuId);

    int insert(TsRoleMenu record);

    int insertSelective(TsRoleMenu record);

    TsRoleMenu selectByPrimaryKey(Long roleMenuId);

    int updateByPrimaryKeySelective(TsRoleMenu record);

    int updateByPrimaryKey(TsRoleMenu record);

    List<TsRoleMenu> findTsRoleMenuList(List<TsUserRole> tsRoleMenuList);

    List<TsRoleMenu> findRoleMenuBuRoleId(Long roleId);

    List<TsRoleMenu> findTsRoleMenuListByMenuId(Long menuId);

    int deleteByRoleId(Long roleId);

    int insertByList(List<TsRoleMenu> tsRoleMenuList);
}