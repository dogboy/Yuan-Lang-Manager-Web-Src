package com.yuanlang.dao.mapper.system;


import com.yuanlang.facade.model.system.TsMenu;
import com.yuanlang.facade.model.system.TsRoleMenu;
import com.yuanlang.facade.model.system.TsUser;

import java.util.List;

public interface TsMenuMapper {
    int deleteByPrimaryKey(Long menuId);

    int insert(TsMenu record);

    int insertSelective(TsMenu record);

    TsMenu selectByPrimaryKey(Long menuId);

    List<TsMenu> findTsMenuList(List<TsRoleMenu> tsRoleMenuList);

    List<TsMenu> findTsMenuPage(TsMenu tsMenu);

    int updateByPrimaryKeySelective(TsMenu record);

    int updateByPrimaryKey(TsMenu record);

    List<TsMenu> findByOrders(TsUser tsUser);
}