package com.yuanlang.dao.mapper.system;

import com.yuanlang.facade.model.system.TsDept;

import java.util.List;

public interface TsDeptMapper {

    int deleteByPrimaryKey(List<TsDept> tsDeptList);

    int insert(TsDept record);

    int insertSelective(TsDept record);

    TsDept selectByPrimaryKey(Long deptId);

    List<TsDept> findTsDeptList(TsDept record);

    List<TsDept> queryContainsChildrenTsDept(Long deptId);

    List<TsDept> findListByDeptCode(String deptCode);

    int updateByPrimaryKeySelective(TsDept record);

    int updateByPrimaryKey(TsDept record);


}