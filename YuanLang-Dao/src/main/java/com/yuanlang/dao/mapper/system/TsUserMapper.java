package com.yuanlang.dao.mapper.system;

import com.yuanlang.facade.model.system.TsUser;

import java.util.List;
import java.util.Map;

public interface TsUserMapper {

    int deleteByPrimaryKey(Long userId);

    int insert(TsUser record);

    int insertSelective(TsUser record);

    TsUser selectByPrimaryKey(Long userId);

    TsUser findTsUser(Map param);

    List<TsUser> findTsUserByLoginName(String loginName);

    List<TsUser> findTsUserList(Map<String, Object> map);

    int updateByPrimaryKeySelective(TsUser record);

    int updateByPrimaryKey(TsUser record);

    int deleteByTsUserList(List<TsUser> tsUserList);
}