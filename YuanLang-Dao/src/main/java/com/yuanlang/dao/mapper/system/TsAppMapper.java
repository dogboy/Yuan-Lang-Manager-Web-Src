package com.yuanlang.dao.mapper.system;

import com.yuanlang.facade.model.system.TsApp;

import java.util.List;
import java.util.Map;

/**
 * app 更新
 */
public interface TsAppMapper {

    int deleteByPrimaryKey(Long id);

    int insert(TsApp record);

    int insertSelective(TsApp record);

    TsApp selectByPrimaryKey(Long id);

    TsApp findLastTsApp(String sign);

    List<TsApp> findTsAppList(TsApp record) ;


    int updateByPrimaryKeySelective(TsApp record);

    int updateByPrimaryKey(TsApp record);
}