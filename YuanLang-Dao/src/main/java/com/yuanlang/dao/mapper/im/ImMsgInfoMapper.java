package com.yuanlang.dao.mapper.im;

import com.yuanlang.facade.model.im.ImMsgInfo;

public interface ImMsgInfoMapper {
    int insert(ImMsgInfo record);

    int insertSelective(ImMsgInfo record);
}