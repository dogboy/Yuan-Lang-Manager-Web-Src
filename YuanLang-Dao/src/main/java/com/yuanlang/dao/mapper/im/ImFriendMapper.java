package com.yuanlang.dao.mapper.im;

import com.yuanlang.facade.model.im.ImFriend;

import java.util.List;

public interface ImFriendMapper {
    int deleteByPrimaryKey(String id);

    int insert(ImFriend record);

    int insertSelective(ImFriend record);

    ImFriend selectByPrimaryKey(String id);

    List<ImFriend> queryImFriendList(String userid);

    List<ImFriend> queryGroupImFriendList(String groupId);

    int updateByPrimaryKeySelective(ImFriend record);

    int updateByPrimaryKey(ImFriend record);
}