package com.yuanlang.dao.mapper.im;

import com.yuanlang.facade.model.im.ImGroup;

import java.util.List;

public interface ImGroupMapper {

    int deleteByPrimaryKey(Long id);

    int insert(ImGroup record);

    int insertSelective(ImGroup record);

    ImGroup selectByPrimaryKey(String id);

    ImGroup findMyFriendImGroup(Long ower);

    List<ImGroup> findExcludeMyFriendImGroup(Long ower);

    List<ImGroup> queryImGroupList(Long ower);
    /**
     * 添加群
     * @param groupname
     * @return
     */
    List<ImGroup> queryImGroupListByGroupName(String groupname);


    int updateByPrimaryKeySelective(ImGroup record);

    int updateByPrimaryKey(ImGroup record);
}