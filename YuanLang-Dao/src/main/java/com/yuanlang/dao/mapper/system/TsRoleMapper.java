package com.yuanlang.dao.mapper.system;

import com.yuanlang.facade.model.system.TsRole;
import com.yuanlang.facade.model.system.TsUser;

import java.util.List;
import java.util.Map;

public interface TsRoleMapper {
    int deleteByPrimaryKey(Long roleId);

    int insert(TsRole record);

    int insertSelective(TsRole record);

    TsRole selectByPrimaryKey(Long roleId);

    int updateByPrimaryKeySelective(TsRole record);

    int updateByPrimaryKey(TsRole record);

    List<TsRole> findTsRoleList(Map<String, Object> map);

    List<TsRole> findTsRoleListByTsUser(TsUser tsUser);

    List<TsRole> findTsRoleListByOrgId(TsUser tsUser);
}