package com.yuanlang.dao.mapper.system;

import com.yuanlang.facade.model.system.TsUserRole;

import java.util.List;

public interface TsUserRoleMapper {
    int deleteByPrimaryKey(String userRoleId);

    Integer deleteByUserId(Long userId);

    int insert(TsUserRole record);

    int insertSelective(TsUserRole record);

    TsUserRole selectByPrimaryKey(String userRoleId);

    List<TsUserRole> findListByUserId(Long userId);

    List<TsUserRole> findTsUserRoleListByRoleId(Long roleId);

    List<TsUserRole> findIsAdmin(Long userId);

    int updateByPrimaryKeySelective(TsUserRole record);

    int updateByPrimaryKey(TsUserRole record);
}