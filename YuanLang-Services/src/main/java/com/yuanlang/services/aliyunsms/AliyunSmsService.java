package com.yuanlang.services.aliyunsms;

import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import com.aliyuncs.exceptions.ClientException;

/**
 * company 重庆源狼软件科技有限责任公司
 * FileName AliyunSmsService
 * Package com.yuanlang.services.aliyunsms
 * Description 阿里云-云信通短信发送
 * author Administrator
 * create 2018-06-25 9:57
 * version V1.0
 */
public interface AliyunSmsService {

    /**
     * @param tels          接受短信的电话号码
     * @param templateCode  发送短信的模板code
     * @param templateParam 发送短信的模板变量
     * @param outId         拓展id，返回该参数，查询发送是否成功
     */
    public SendSmsResponse sendSms(String tels, String templateCode, String templateParam, String outId) throws ClientException;

}
