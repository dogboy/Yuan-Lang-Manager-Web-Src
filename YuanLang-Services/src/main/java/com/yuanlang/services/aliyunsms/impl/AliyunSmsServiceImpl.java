package com.yuanlang.services.aliyunsms.impl;

import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.dysmsapi.model.v20170525.QuerySendDetailsResponse;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsRequest;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.profile.DefaultProfile;
import com.aliyuncs.profile.IClientProfile;
import com.yuanlang.facade.model.system.TsSMS;
import com.yuanlang.services.aliyunsms.AliyunSmsService;
import com.yuanlang.services.system.TsSMSService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * company 重庆源狼软件科技有限责任公司
 * FileName AliyunSmsServiceImpl
 * Package com.yuanlang.services.aliyunsms.impl
 * Description 阿里云-云信通短信发送-服务实现
 * author Administrator
 * create 2018-06-25 9:58
 * version V1.0
 */

@Service
public class AliyunSmsServiceImpl implements AliyunSmsService {

    @Autowired
    TsSMSService tsSMSService;

    /**
     * 产品名称:云通信短信API产品,开发者无需替换
     */
    static final String product = "Dysmsapi";
    /**
     * 产品域名,开发者无需替换
     */
    static final String domain = "dysmsapi.aliyuncs.com";
    /**
     * 此处需要替换成开发者自己的AK(在阿里云访问控制台寻找)
     */
    static final String accessKeyId = "LTAIueh0TAHhRfTk";
    static final String accessKeySecret = "EYe5USt55CiSTnxla4Popn3IHxwbQO";
    static final String defaultConnectTimeout = "10000";
    static final String defaultReadTimeout = "10000";
    static final String signName = "黄学乾";
    private static Logger logger = LoggerFactory.getLogger(AliyunSmsServiceImpl.class); // 阿里云信-发送短信

    /**
     * @param bizId
     * @return
     * @throws ClientException
     */
    public static QuerySendDetailsResponse querySendDetails(String bizId) throws ClientException {
/*        //可自助调整超时时间
        System.setProperty("sun.net.client.defaultConnectTimeout", "10000");
        System.setProperty("sun.net.client.defaultReadTimeout", "10000");
        //初始化acsClient,暂不支持region化
        IClientProfile profile = DefaultProfile.getProfile("cn-hangzhou", accessKeyId, accessKeySecret);
        DefaultProfile.addEndpoint("cn-hangzhou", "cn-hangzhou", product, domain);
        IAcsClient acsClient = new DefaultAcsClient(profile);
        //组装请求对象
        QuerySendDetailsRequest request = new QuerySendDetailsRequest();
        //必填-号码
        request.setPhoneNumber("15000000000");
        //可选-流水号
        request.setBizId(bizId);
        //必填-发送日期 支持30天内记录查询，格式yyyyMMdd
        SimpleDateFormat ft = new SimpleDateFormat("yyyyMMdd");
        request.setSendDate(ft.format(new Date()));
        //必填-页大小
        request.setPageSize(10L);
        //必填-当前页码从1开始计数
        request.setCurrentPage(1L);
        //hint 此处可能会抛出异常，注意catch
        QuerySendDetailsResponse querySendDetailsResponse = acsClient.getAcsResponse(request);
        return querySendDetailsResponse;*/
        return null;
    }

    /**
     * @param tels          接受短信的电话号码
     * @param templateCode  发送短信的模板code
     * @param templateParam 发送短信的模板变量
     * @param outId         拓展id，返回该参数，查询发送是否成功
     * @throws ClientException 发送短信 ；
     */
    @Override
    public SendSmsResponse sendSms(String tels, String templateCode, String templateParam, String outId) throws ClientException {
        //可自助调整超时时间
        System.setProperty("sun.net.client.defaultConnectTimeout", String.valueOf(defaultConnectTimeout));
        System.setProperty("sun.net.client.defaultReadTimeout", String.valueOf(defaultReadTimeout));
        //初始化acsClient,暂不支持region化-暂不支持国际化
        IClientProfile profile = DefaultProfile.getProfile("cn-hangzhou", accessKeyId, accessKeySecret);
        DefaultProfile.addEndpoint("cn-hangzhou", "cn-hangzhou", product, domain);
        IAcsClient acsClient = new DefaultAcsClient(profile);
        //组装请求对象-具体描述见控制台-文档部分内容
        SendSmsRequest request = new SendSmsRequest();
        //必填:待发送手机号
        request.setPhoneNumbers(tels);
        //必填:短信签名-可在短信控制台中找到
        request.setSignName(signName);
        //必填:短信模板-可在短信控制台中找到 "SMS_1000000"
        request.setTemplateCode(templateCode);
        //可选:模板中的变量替换JSON串,如模板内容为"亲爱的${name},您的验证码为${code}"时,此处的值为
        //"{\"name\":\"Tom\", \"code\":\"123\"}"
        request.setTemplateParam(templateParam);
        //选填-上行短信扩展码(无特殊需求用户请忽略此字段)
        //request.setSmsUpExtendCode("90997");
        //可选:outId为提供给业务方扩展字段,最终在短信回执消息中将此值带回给调用者
        request.setOutId(outId);
        TsSMS sms = new TsSMS();
        sms.setPhonenumbers(tels);
        sms.setTemplatecode(templateCode);
        sms.setTemplateparam(templateParam);
        sms.setSignname(signName);
        //hint 此处可能会抛出异常，注意catch
        SendSmsResponse response = null;
        try {
            response = acsClient.getAcsResponse(request);
            if(response!=null){
                String code = response.getCode();
                sms.setCode(code);
                String msg = response.getMessage();
                sms.setMessage(msg);
                String requestId = response.getRequestId() ;
                sms.setRequestid(requestId);
                String bizId = response.getBizId() ;
                sms.setBizid(bizId);
            }
        } catch (ClientException e) {
            e.printStackTrace();
            logger.info("短信发送问题出错-->");
        } finally {
            try {
                tsSMSService.addTsSMS(sms);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        // 处理短信发送后逻辑问题;
        return response;
    }

/*    public static void main(String[] args) throws ClientException, InterruptedException {
        //发短信
        SendSmsResponse response = sendSms();
        System.out.println("短信接口返回的数据----------------");
        System.out.println("Code=" + response.getCode());
        System.out.println("Message=" + response.getMessage());
        System.out.println("RequestId=" + response.getRequestId());
        System.out.println("BizId=" + response.getBizId());

        Thread.sleep(3000L);

        //查明细
        if (response.getCode() != null && response.getCode().equals("OK")) {
            QuerySendDetailsResponse querySendDetailsResponse = querySendDetails(response.getBizId());
            System.out.println("短信明细查询接口返回数据----------------");
            System.out.println("Code=" + querySendDetailsResponse.getCode());
            System.out.println("Message=" + querySendDetailsResponse.getMessage());
            int i = 0;
            for (QuerySendDetailsResponse.SmsSendDetailDTO smsSendDetailDTO : querySendDetailsResponse.getSmsSendDetailDTOs()) {
                System.out.println("SmsSendDetailDTO[" + i + "]:");
                System.out.println("Content=" + smsSendDetailDTO.getContent());
                System.out.println("ErrCode=" + smsSendDetailDTO.getErrCode());
                System.out.println("OutId=" + smsSendDetailDTO.getOutId());
                System.out.println("PhoneNum=" + smsSendDetailDTO.getPhoneNum());
                System.out.println("ReceiveDate=" + smsSendDetailDTO.getReceiveDate());
                System.out.println("SendDate=" + smsSendDetailDTO.getSendDate());
                System.out.println("SendStatus=" + smsSendDetailDTO.getSendStatus());
                System.out.println("Template=" + smsSendDetailDTO.getTemplateCode());
            }
            System.out.println("TotalCount=" + querySendDetailsResponse.getTotalCount());
            System.out.println("RequestId=" + querySendDetailsResponse.getRequestId());
        }
    }*/
}
