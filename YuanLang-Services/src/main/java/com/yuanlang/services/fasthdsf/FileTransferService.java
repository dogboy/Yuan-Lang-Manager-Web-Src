package com.yuanlang.services.fasthdsf;

import com.yuanlang.common.ResultMap;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;

/**
 * 文件操作接口
 */
public interface FileTransferService {
    /**
     * @param fileName   文件名称
     * @param fileBase64 文件加密方法
     * @return 加密上传
     */
    public ResultMap fileUpload(String fileName, String fileBase64);


    /**
     * @param file 单文件
     */
    public ResultMap fileUpload(MultipartFile file);

    /**
     * @param myfiles 多文件上传
     */
    public ResultMap fileUpload(MultipartFile[] myfiles);

    /**
     * @param localFileName 上传本地文件
     */
    public ResultMap fileUpload(String localFileName);


    /**
     * @param remoteFileNameWithGroupName 文件删除
     */
    public void deleteFile(String remoteFileNameWithGroupName);

    /**
     * @param remoteFileNameWithGroupName 文件下载
     * @param savePath                    保存路径
     */
    public ResultMap download(String remoteFileNameWithGroupName, String savePath);

    /**
     * @param remoteFileNameWithGroupName 文件下载
     */
    public ResultMap downloadAsBytes(String remoteFileNameWithGroupName);

    public char getPathSeparator();

    /**
     * @param fileURL
     * @param kjbDFS
     * @param response 文件预览
     */
    public void outFileStream(String fileURL, Boolean kjbDFS, HttpServletResponse response);
}
