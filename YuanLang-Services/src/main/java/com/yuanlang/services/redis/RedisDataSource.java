package com.yuanlang.services.redis;

import redis.clients.jedis.ShardedJedis;

/**
 * company 重庆源狼软件科技有限责任公司
 * FileName RedisDataSource
 * Package com.yuanlang.services.redis
 * Description Redis内存数据库
 * author Administrator
 * create 2018-05-12 22:17
 * version V1.0
 */
public interface RedisDataSource {
    /**
     * 取得redis的客户端，可以执行命令了
     *
     * @return
     */
    public abstract ShardedJedis getRedisClient();

    /**
     * 将资源返还给pool
     *
     * @param shardedJedis
     */
    public void returnResource(ShardedJedis shardedJedis);

    /**
     * 出现异常后，将资源返还给pool （其实不需要第二个方法）
     *
     * @param shardedJedis
     * @param broken
     */
    public void returnResource(ShardedJedis shardedJedis, boolean broken);
}
