package com.yuanlang.services.system;

import com.yuanlang.facade.model.system.*;
import javafx.scene.control.TreeView;

import java.util.List;

/**
 * company 重庆源狼软件科技有限责任公司
 * FileName TsRoleMenuService
 * Package com.soft.link.service
 * Description 角色权限接口
 * author Administrator
 * create 2018-03-31 22:38
 * version V1.0
 */

public interface TsRoleMenuService {

    List<TsRoleMenu> findTsRoleMenuList(List<TsUserRole> tsUserRoles) throws Exception;

    List<TsRoleMenu> findRoleMenuBuRoleId(Long roleId) throws Exception;

    List<TsRoleMenu> findTsRoleMenuListByMenuId(Long menuId) throws Exception;

    Integer deleteByRoleId(Long roleId) throws Exception;

    Integer saveRoleMenuList(List<TsRoleMenu> tsRoleMenuList);
}
