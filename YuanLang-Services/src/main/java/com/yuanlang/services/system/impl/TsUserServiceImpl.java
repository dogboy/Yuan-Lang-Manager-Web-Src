package com.yuanlang.services.system.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.yuanlang.common.ResultMap;
import com.yuanlang.common.util.ActiveMqMsgCode;
import com.yuanlang.dao.mapper.system.TsUserMapper;
import com.yuanlang.facade.model.im.ImGroup;
import com.yuanlang.facade.model.im.ImUser;
import com.yuanlang.facade.model.system.TsUser;
import com.yuanlang.services.im.ImGroupService;
import com.yuanlang.services.im.ImUserService;
import com.yuanlang.services.system.TsUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * company 重庆源狼软件科技有限责任公司
 * FileName TsUserServiceImpl
 * Package com.soft.link.impl
 * Description 人员基本信息服务实现
 * author Administrator
 * create 2018-03-31 22:05
 * version V1.0
 */
@Service
public class TsUserServiceImpl implements TsUserService {

    @Autowired(required = false)
    TsUserMapper tsUserMapper;

    @Autowired(required = false)
    ImUserService imUserService ;

    @Value("${isSyncImUser}")
    private String isSyncImUser ;

    @Autowired(required = false)
    ImGroupService groupService ;

    @Autowired(required = false)
    private JmsTemplate jmsTemplate;

    /**
     * @param loginName 登录名
     * @param password  密码
     * @throws {如果有异常说明请填写}
     * @methodname findTsUser
     * @Description {填写方法注释说明}
     * @author 黄学乾
     * @create 2018/3/31/031 22:07
     */
    @Override
    public TsUser findTsUser(String loginName, String password) throws Exception {
        Map param = new HashMap<>();
        param.put("loginName", loginName);
        param.put("password", password);
        TsUser tsUser = tsUserMapper.findTsUser(param);
        return tsUser;
    }

    /**
     * @param userId 人员Id
     * @return
     * @throws Exception
     */
    @Override
    public TsUser findTsUserByUserId(Long userId) throws Exception {
        return tsUserMapper.selectByPrimaryKey(userId);
    }

    @Override
    public List<TsUser> findTsUser(String loginName) throws Exception {
        return tsUserMapper.findTsUserByLoginName(loginName);
    }

    /**
    * @methodname findTsUserList 
    * @Description 根据查询，
    * @author 黄学乾
    * @create 2018/4/5/005 21:52
    * @param  * @param tsUser
    */
    @Override
    public List<TsUser> findTsUserList(TsUser tsUser, String deptCode) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("tsUser", tsUser);
        map.put("deptCode",deptCode);
        return tsUserMapper.findTsUserList(map);
    }

    /**
    * @methodname  findTsUserPage
    * @Description 账户列表分页查询
    * @author 黄学乾
    * @create 2018/4/23/023 15:59
    * @param  * @param tsUser
    * @throws {数据库连接超时和查询失败}
    * @return 
    */
    @Override
    public ResultMap findTsUserPage(TsUser tsUser, String deptCode) throws Exception {
        ResultMap result =  new ResultMap();
        PageHelper.startPage(tsUser.getPage(),tsUser.getLimit());
        List<TsUser> tsUserList = findTsUserList(tsUser,deptCode);
        // 取分页信息
        PageInfo<TsUser> pageInfo = new PageInfo<TsUser>(tsUserList);
        result.setData(pageInfo.getList());
        result.setCount(pageInfo.getTotal());
        return result ;
    }

    /**
     * saveTsUser 用户新增
     * @param tsUser 用户信息
     */
    @Override
    @Transactional
    public Integer saveTsUser(TsUser tsUser) throws Exception {
        Integer code = tsUserMapper.insertSelective(tsUser);
        Long userId = tsUser.getUserId(); // 插入后返回插入的id，然后再插入另一张表中进行关联
        // 新增同步人员信息到LayIM
        if(Boolean.parseBoolean(isSyncImUser)){
            ImUser imUser = new ImUser();
            imUser.setId(userId);
            imUser.setUsername(tsUser.getUserName());
            imUser.setTimestamp(tsUser.getCreateTime());
            imUser.setDatafrom("PC");
            imUserService.saveSyncImUser(imUser);

            ImGroup group = new ImGroup();
            group.setId(userId);
            group.setGroupname("我的好友");
            group.setStatus(1);  // 1:好友分组 ; 2:群聊分组 ；3:私聊分组
            group.setTimestamp(new Date());
            group.setOwer(userId);
            groupService.saveSyncImGroup(group,"add");
            /**
             * 把所有的人插入到我的好友中去 ， 或者 把我插入到所有的人的好友中去
             */
            // 将所有的操作都接入到MQ中去操作， 1：同步我的部门；2同步我的公司；3 同步我的好友
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("ActiveMqMsgCode",ActiveMqMsgCode.BIZ_IM_USER.getResultCode());
            jsonObject.put("ActiveMqMsgData",tsUser);
            jmsTemplate.convertAndSend(JSON.toJSONString(jsonObject));
        }
        return code;
    }

    @Override
    @Transactional
    public Integer updateTsUser(TsUser tsUser) throws Exception {
        // 更新同步人员信息到LayIM
        if(Boolean.parseBoolean(isSyncImUser)){
            ImUser imUser = new ImUser();
            imUser.setId(tsUser.getUserId());
            imUser.setUsername(tsUser.getUserName());
            imUser.setTimestamp(tsUser.getCreateTime());
            imUser.setDatafrom("PC");
            imUserService.updateImUser(imUser);
        }
        return tsUserMapper.updateByPrimaryKeySelective(tsUser);
    }

    @Override
    public Integer delTsUser(List<TsUser> tsUserList) throws Exception {

        return tsUserMapper.deleteByTsUserList(tsUserList);
    }
}
