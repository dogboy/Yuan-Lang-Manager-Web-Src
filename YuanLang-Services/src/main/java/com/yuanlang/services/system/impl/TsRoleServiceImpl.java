package com.yuanlang.services.system.impl;

import com.yuanlang.dao.mapper.system.TsRoleMapper;
import com.yuanlang.facade.model.system.TsRole;
import com.yuanlang.facade.model.system.TsUser;
import com.yuanlang.services.system.TsRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * company 重庆源狼软件科技有限责任公司
 * FileName TsRoleServiceImpl
 * Package com.soft.link.impl
 * Description 角色服务实现类
 * author Administrator
 * create 2018-03-31 22:16
 * version V1.0
 */
@Service
public class TsRoleServiceImpl implements TsRoleService {
    @Autowired(required = false)
    TsRoleMapper tsRoleMapper;

    @Override
    public List<TsRole> findTsRoleList(TsRole tsRole, String deptCode) throws Exception{
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("tsRole",tsRole);
        map.put("deptCode",deptCode);
        return tsRoleMapper.findTsRoleList(map);
    }


    /**
     * 根据ID查询系统角色
     * @param roleId
     * @return
     * @throws Exception
     */
    @Override
    public TsRole findTsRoleById(Long roleId) throws Exception{

        return tsRoleMapper.selectByPrimaryKey(roleId);
    }


    /**
     * 添加/编辑系统角色
     * @param tsRole
     * @return
     * @throws Exception
     */
    @Override
    public Integer addOrEditTsRole(TsRole tsRole) throws Exception{

        if(tsRole.getRoleId() == null){

            return tsRoleMapper.insertSelective(tsRole);
        }else {

            return tsRoleMapper.updateByPrimaryKeySelective(tsRole);
        }

    }


    /**
     * 根据角色ID删除角色
     * @param roleId
     * @return
     * @throws Exception
     */
    @Override
    public Integer delateByRoleId(Long roleId) throws Exception{

        return tsRoleMapper.deleteByPrimaryKey(roleId);
    }

    /**
     * 根据用户ID查询已配置的角色
     * @param tsUser
     * @return
     * @throws Exception
     */
    @Override
    public List<TsRole> findTsRoleListByLoginUser(TsUser tsUser) throws Exception{

        if(tsUser.getAdmin()){//是否为管理员
            // 管理员可查看该机构的所有角色
            return tsRoleMapper.findTsRoleListByOrgId(tsUser);
        }else {
            // 普通人员只能查看自己仅有权限的角色
            return tsRoleMapper.findTsRoleListByTsUser(tsUser);
        }
    }

}
