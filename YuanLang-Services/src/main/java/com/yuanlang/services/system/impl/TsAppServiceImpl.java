package com.yuanlang.services.system.impl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.yuanlang.common.ResultMap;
import com.yuanlang.dao.mapper.system.TsAppMapper;
import com.yuanlang.facade.model.system.TsApp;
import com.yuanlang.facade.model.system.TsDept;
import com.yuanlang.services.system.TsAppService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * company 重庆源狼软件科技有限责任公司
 * FileName TsAppServiceImpl
 * Package com.yuanlang.services.system.impl
 * Description
 * author Administrator
 * create 2018-05-31 14:37
 * version V1.0
 */
@Service
public class TsAppServiceImpl implements TsAppService {
    @Autowired
    TsAppMapper tsAppMapper ;
    @Override
    public int saveTsApp(TsApp tsApp) throws Exception {

        return tsAppMapper.insertSelective(tsApp);
    }

    /**
     * @param id 根据id 查询
     */
    @Override
    public int delTsApp(Long id) throws Exception {
        return tsAppMapper.deleteByPrimaryKey(id);
    }

    @Override
    public TsApp findTsApp(TsApp tsApp) throws Exception {

        return null;
    }

    @Override
    public TsApp findLastTsApp(String sign) throws Exception {
        return tsAppMapper.findLastTsApp(sign);
    }

    @Override
    public ResultMap findTsAppPage(TsApp tsApp) throws Exception {
        ResultMap result =  new ResultMap();
        PageHelper.startPage(tsApp.getPage(),tsApp.getLimit());
        List<TsApp> tsDeptList = tsAppMapper.findTsAppList(tsApp);
        // 取分页信息
        PageInfo<TsApp> pageInfo = new PageInfo<>(tsDeptList);
        result.setData(pageInfo.getList());
        result.setCount(pageInfo.getTotal());
        return result;
    }
}
