package com.yuanlang.services.system;

import com.yuanlang.common.ResultMap;
import com.yuanlang.facade.model.system.TsApp;

import java.math.BigDecimal;

/**
 * company 重庆源狼软件科技有限责任公司
 * FileName TsAppService
 * Package com.yuanlang.services.system
 * Description App管理接口
 * author Administrator
 * create 2018-05-31 14:32
 * version V1.0
 */

public interface TsAppService {
    /**
     * @param   tsApp
     * @return  新增app 更新详情
     * @throws  Exception
     */
    int saveTsApp(TsApp tsApp) throws Exception ;


    int delTsApp(Long id) throws Exception ;

    /**
     * @param tsApp 拼接的信息
     * @return 查询tsApp信息
     */
    TsApp findTsApp(TsApp tsApp) throws Exception ;

    /**
     * @param sign  签名字符串
     */
    TsApp findLastTsApp(String sign) throws Exception ;


    ResultMap findTsAppPage(TsApp tsApp) throws Exception ;
}
