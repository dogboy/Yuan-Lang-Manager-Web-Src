package com.yuanlang.services.system.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.yuanlang.common.ResultMap;
import com.yuanlang.dao.mapper.system.TsDeptMapper;
import com.yuanlang.facade.model.im.ImGroup;
import com.yuanlang.facade.model.im.ImUser;
import com.yuanlang.facade.model.system.TsDept;
import com.yuanlang.facade.model.system.TsUser;
import com.yuanlang.services.im.ImGroupService;
import com.yuanlang.services.system.TsDeptService;
import org.apache.hadoop.classification.InterfaceAudience;
import org.springframework.amqp.rabbit.support.PublisherCallbackChannel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.List;

/**
 * company 重庆源狼软件科技有限责任公司
 * FileName TsDeptServiceImpl
 * Package com.yuanlang.services.system.impl
 * Description 部门系统服务接口实现
 * author Administrator
 * create 2018-05-22 21:11
 * version V1.0
 */
@Service
public class TsDeptServiceImpl implements TsDeptService {

    @Value("${isSyncImUser}")
    private String isSyncImUser ;

    @Autowired(required = false)
    private TsDeptMapper tsDeptMapper ;

    @Autowired(required = false)
    private ImGroupService imGroupService ;
    @Transactional
    @Override
    public int saveTsDeptOrUpdate(TsDept tsDept) throws Exception {
        int result ;
        String status="add";
        if(StringUtils.isEmpty(tsDept.getDeptId())){
            result =  tsDeptMapper.insertSelective(tsDept);
        }else{
            result = tsDeptMapper.updateByPrimaryKeySelective(tsDept);
            status="update";
        }
        // 同步到分组表中去
        if(Boolean.parseBoolean(isSyncImUser)){
            ImGroup group = new ImGroup();
            group.setId(tsDept.getDeptId());
            group.setGroupname(tsDept.getDeptName());
            group.setStatus(2);  // 1:好友分组 ; 2:群聊分组 ；3:私聊分组
            group.setTimestamp(new Date());
            imGroupService.saveSyncImGroup(group,status);
        }

        return result ;
    }

    /**
     * @param tsDeptList 部门信息
     * 删除部门信息
     */
    @Override
    public int delTsDept(List<TsDept> tsDeptList) throws Exception {
        return tsDeptMapper.deleteByPrimaryKey(tsDeptList);
    }

    @Override
    public TsDept findTsDept(Long deptId) throws Exception {
        return tsDeptMapper.selectByPrimaryKey(deptId);
    }

    /**
     * @param customerId 查询当前客户下的所有部门
     * @throws Exception
     */
    @Override
    public List<TsDept> queryTsDeptList(Long customerId) throws Exception {
        return null;
    }

    @Override
    public List<TsDept> queryChildrenTsDept(Long parentId) throws Exception {
        return null;
    }

    /**
     * @param tsDept 部门信息
     * @return
     * @throws Exception
     */
    @Override
    public ResultMap findTsDeptPage(TsDept tsDept) throws Exception {
        ResultMap result =  new ResultMap();
        PageHelper.startPage(tsDept.getPage(),tsDept.getLimit());
        List<TsDept> tsDeptList = tsDeptMapper.findTsDeptList(tsDept);
        // 取分页信息
        PageInfo<TsDept> pageInfo = new PageInfo<>(tsDeptList);
        result.setData(pageInfo.getList());
        result.setCount(pageInfo.getTotal());
        return result;
    }


    /**
     * 根据部门编码查询部门及下属部门
     * @param deptCode
     * @return
     * @throws Exception
     */
    @Override
    public List<TsDept> findDeptAndChildDepts(String deptCode) throws Exception{

        return tsDeptMapper.findListByDeptCode(deptCode);
    }
}
