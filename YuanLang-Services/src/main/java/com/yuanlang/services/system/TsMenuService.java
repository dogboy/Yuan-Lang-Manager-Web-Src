package com.yuanlang.services.system;


import com.yuanlang.facade.model.system.*;
import javafx.scene.control.TreeView;

import java.util.List;

/**
 * company 重庆源狼软件科技有限责任公司
 * FileName TsMenuService
 * Package com.soft.link.service
 * Description 菜单接口
 * author Administrator
 * create 2018-03-31 22:54
 * version V1.0
 */

public interface TsMenuService {

    /**
     * @param * @param tsRoleMenuList 角色菜单关系
     * @methodname findTsMenuList
     * @Description 根据角色菜单关系，查询菜单信息
     * @author 黄学乾
     * @create 2018/3/31/031 23:05
     */
    List<TsMenu> findTsMenuList(List<TsRoleMenu> tsRoleMenuList) throws Exception;

    /**
     * @param * @param tsUser
     * @return
     * @throws {如果有异常说明请填写}
     * @methodname findTsMenuList
     * @Description 根据人员信息，查询人员菜单信息
     * @author 黄学乾
     * @create 2018/3/31/031 23:08
     */
    List<TsMenu> findTsMenuList(TsUser tsUser) throws Exception;

    /**
    * @methodname  根据角色查询 ，角色下菜单信息 
    * @Description {填写方法注释说明}
    * @author 黄学乾
    * @create 2018/4/27/027 11:36
    */
    List<TsMenu> tsMenuService(TsRole tsRole) throws Exception;


    /**
    * @methodname offSet
    * @Description {数据转换}
    * @author 黄学乾
    * @create 2018/4/27/027 11:55
    * @param  * @param 
    * @throws {TsMenu 转换层 TreeView}
    * @return 
    */
    List<TreeView> offSet(List<TsMenu> tsMenuList) throws Exception ;

    /**
     * 根据条件查询菜单列表
     * @param tsMenu
     * @return
     * @throws Exception
     */
    List<TsMenu> findTsMenuPage(TsMenu tsMenu) throws Exception;


    Integer saveOrUpdateTsMenu(TsMenu tsMenu) throws Exception;

    Integer delTsMenu(Long menuId) throws Exception;

    TsMenuV findTsMenuById(Long menuId) throws Exception;

    List<TsMenu> findByOrders(TsUser tsUser) throws Exception;
}
