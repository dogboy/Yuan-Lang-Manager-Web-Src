package com.yuanlang.services.system.impl;

import com.yuanlang.dao.mapper.system.TsRoleMenuMapper;
import com.yuanlang.facade.model.system.TsRoleMenu;
import com.yuanlang.facade.model.system.TsUserRole;
import com.yuanlang.services.system.TsRoleMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * company 重庆源狼软件科技有限责任公司
 * FileName TsRoleMenuServiceImpl
 * Package com.soft.link.impl
 * Description 角色权限接口实现类
 * author Administrator
 * create 2018-03-31 22:40
 * version V1.0
 */
@Service
public class TsRoleMenuServiceImpl implements TsRoleMenuService {
    @Autowired(required = false)
    TsRoleMenuMapper tsRoleMenuMapper;

    @Override
    public List<TsRoleMenu> findTsRoleMenuList(List<TsUserRole> tsUserRoles) throws Exception{
        return null;
    }

    /**
     * 根据角色ID查询角色菜单
     * @param roleId
     * @return
     * @throws Exception
     */
    @Override
    public List<TsRoleMenu> findRoleMenuBuRoleId(Long roleId) throws Exception{

        return tsRoleMenuMapper.findRoleMenuBuRoleId(roleId);
    }

    /**
     * 根据菜单ID查询用户菜单
     * @param menuId
     * @return
     * @throws Exception
     */
    @Override
    public List<TsRoleMenu> findTsRoleMenuListByMenuId(Long menuId) throws Exception {

        return tsRoleMenuMapper.findTsRoleMenuListByMenuId(menuId);
    }


    /**
     * 根据角色ID删除角色菜单
     * @param roleId
     * @return
     * @throws Exception
     */
    @Override
    public Integer deleteByRoleId(Long roleId) throws Exception{

        return tsRoleMenuMapper.deleteByRoleId(roleId);
    }

    /**
     * 批量添加角色菜单
     * @param tsRoleMenuList
     * @return
     */
    @Override
    public Integer saveRoleMenuList(List<TsRoleMenu> tsRoleMenuList){

        return tsRoleMenuMapper.insertByList(tsRoleMenuList);
    }

}
