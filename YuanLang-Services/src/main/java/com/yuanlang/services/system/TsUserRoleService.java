package com.yuanlang.services.system;



import com.yuanlang.facade.model.system.TsRole;
import com.yuanlang.facade.model.system.TsUserRole;

import java.util.List;

/**
 * company 重庆源狼软件科技有限责任公司
 * FileName TsUserRoleService
 * Package com.soft.link.service
 * Description 人员角色接口类
 * author Administrator
 * create 2018-03-31 22:18
 * version V1.0
 */

public interface TsUserRoleService {

    /**
     * @param * @param  userId 人员ID
     * @return
     * @throws {如果有异常说明请填写}
     * @methodname findTsUserRole
     * @Description 根据人员ID 查询 ，人员角色信息
     * @author 黄学乾
     * @create 2018/3/31/031 22:19
     */
    List<TsUserRole> findTsUserRoleList(Long userId) throws Exception ;


    /**
     * @param tsRoles 角色基本信息
     * @throws {数据库连接异常和其他异常}
     * @methodname findTsUserRoleListByTsRole
     * @Description {根据角色，查询人员角色关联信息}
     * @author 黄学乾
     * @create 2018/4/30/030 16:36
     */
    List<TsUserRole> findTsUserRoleListByTsRole(List<TsRole> tsRoles);


    List<TsUserRole> findTsUserRoleListByRoleId(Long roleId) throws Exception;

    Boolean isAdmin(Long userId) throws Exception;

    Integer deleteByUserId(Long userId) throws Exception;

    Integer add(TsUserRole tsUserRole) throws Exception;
}
