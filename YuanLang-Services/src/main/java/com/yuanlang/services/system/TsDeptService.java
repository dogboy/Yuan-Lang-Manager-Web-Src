package com.yuanlang.services.system;

import com.yuanlang.common.ResultMap;
import com.yuanlang.facade.model.system.TsDept;

import java.util.List;

/**
 * company 重庆源狼软件科技有限责任公司
 * FileName TsDeptService
 * Package com.yuanlang.services.system
 * Description 部门系统服务接口
 * author Administrator
 * create 2018-05-22 21:10
 * version V1.0
 */

public interface TsDeptService {

    /**
     * @param tsDept 部门基本信息
     * 保存、修改部门基本信息
     */
    int saveTsDeptOrUpdate (TsDept tsDept) throws Exception ;


    int delTsDept(List<TsDept> tsDeptList) throws Exception ;

    /**
     * @param deptId 部门id
     * @return 部门基本信息
     * @throws Exception
     */
    TsDept findTsDept (Long deptId) throws Exception ;

    /**
     * @param customerId 查询当前客户下的所有代码
     * @return
     * @throws Exception
     */
    List<TsDept> queryTsDeptList(Long customerId) throws Exception ;

    /**
     * @return 查询当前部门的下级部门 （不含当前部门）
     */
    List<TsDept> queryChildrenTsDept(Long parentId) throws Exception ;

    /**
     * @param tsDept
     * 部门分页信息查询
     */
    ResultMap findTsDeptPage(TsDept tsDept) throws Exception ;


    List<TsDept> findDeptAndChildDepts(String deptCode) throws Exception;
}
