package com.yuanlang.services.system.impl;

import com.yuanlang.dao.mapper.system.TsSMSMapper;
import com.yuanlang.facade.model.system.TsSMS;
import com.yuanlang.services.system.TsSMSService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * company 重庆源狼软件科技有限责任公司
 * FileName TsSMSServiceImpl
 * Package com.yuanlang.services.system.impl
 * Description 短信实体
 * author Administrator
 * create 2018-06-25 11:48
 * version V1.0
 */
@Service
public class TsSMSServiceImpl implements TsSMSService {
    @Autowired
    private TsSMSMapper tsSMSMapper ;

    /**
     *
     * @param tsSMS 短信实体
     * @return
     * @throws Exception
     */
    @Override
    public int addTsSMS(TsSMS tsSMS) throws Exception {

        return tsSMSMapper.insertSelective(tsSMS);
    }

    /**
     * @param   tsSMS
     * @return
     */
    @Override
    public int updateTsSMS(TsSMS tsSMS) throws Exception {
        return  0 ;
    }
}
