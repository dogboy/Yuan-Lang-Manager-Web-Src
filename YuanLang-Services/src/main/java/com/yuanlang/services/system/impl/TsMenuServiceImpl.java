package com.yuanlang.services.system.impl;

import com.yuanlang.dao.mapper.system.TsMenuMapper;
import com.yuanlang.facade.model.system.*;
import com.yuanlang.services.system.TsMenuService;
import com.yuanlang.services.system.TsRoleMenuService;
import com.yuanlang.services.system.TsUserRoleService;
import javafx.scene.control.TreeView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * company 重庆源狼软件科技有限责任公司
 * FileName TsMenuServiceImpl
 * Package com.yuanlang.services.system.impl
 * Description 菜单接口实现类
 * author Administrator
 * create 2018-03-31 22:55
 * version V1.0
 */
@Service
public class TsMenuServiceImpl implements TsMenuService {
    public Logger logger = LoggerFactory.getLogger(this.getClass());
    @Autowired(required = false)
    TsMenuMapper tsMenuMapper;
    @Autowired(required = false)
    TsUserRoleService tsUserRoleService;
    @Autowired(required = false)
    TsRoleMenuService tsRoleMenuService;

    /**
     * @param tsRoleMenuList
     * @methodname findTsMenuList
     * @Description 根据角色菜单查询菜单关系
     * @author 黄学乾
     * @create 2018/3/31/031 22:57
     */
    @Override
    public List<TsMenu> findTsMenuList(List<TsRoleMenu> tsRoleMenuList) throws Exception {
        List<TsMenu> tsMenuList = tsMenuMapper.findTsMenuList(tsRoleMenuList);
        return tsMenuList;
    }

    /**
     * @param tsUser 人员基本信息
     * @methodname findTsMenuList
     * @Description 根据人员信息查询人员所属的菜单信息
     * @author 黄学乾
     * @create 2018/3/31/031 23:08
     */
    @Override
    public List<TsMenu> findTsMenuList(TsUser tsUser) throws Exception {
        List<TsMenu> tsMenu = new ArrayList<TsMenu>();
        if (tsUser != null) {
            List<TsUserRole> tsUserRoles = tsUserRoleService.findTsUserRoleList(tsUser.getUserId());
            if (tsUserRoles != null && tsUserRoles.size() > 0) {
                List<TsRoleMenu> tsRoleMenus = tsRoleMenuService.findTsRoleMenuList(tsUserRoles);
                if (tsRoleMenus != null && tsRoleMenus.size() > 0) {
                    tsMenu = this.findTsMenuList(tsRoleMenus);
                }
            }
        }
        return tsMenu;
    }

    /**
     * @param * @param tsRole
     * @return
     * @throws {如果有异常说明请填写}
     * @methodname tsMenuService
     * @Description {填写方法注释说明}
     * @author 黄学乾
     * @create 2018/4/27/027 11:36
     */
    @Override
    public List<TsMenu> tsMenuService(TsRole tsRole) throws Exception {
        List<TsMenu> tsMenus = new ArrayList<TsMenu>();
//        List<TsRoleMenu> tsRoleMenu = tsRoleMenuService.findTsRoleMenuListByTsRole(tsRole);
//        if (tsRoleMenu != null && tsRoleMenu.size() > 0) {
//            tsMenus = this.findTsMenuList(tsRoleMenu);
//        }
        return tsMenus;
    }

    /**
     * @param tsMenuList 菜单树
     * @return
     * @throws {如果有异常说明请填写}
     * @methodname offSet
     * @Description {将菜单列表转成菜单树}
     * @author 黄学乾
     * @create 2018/4/27/027 11:56
     */
    @Override
    public List<TreeView> offSet(List<TsMenu> tsMenuList) throws Exception {
        List<TreeView> treeViewList = new ArrayList<TreeView>();
        /*if(tsMenuList!=null && tsMenuList.size() > 0){
            for(TsMenu tsMenu: tsMenuList){
                TreeView ztreeView = new TreeView();
                ztreeView.setId(tsMenu.getMenuId());
                ztreeView.setText(tsMenu.getMenuName());
                List<TreeView> nodes = getChild(tsMenuList,tsMenu);
                ztreeView.setNodes(nodes);
                treeViewList.add(ztreeView);
            }
        }*/
        return treeViewList;
    }


    /**
     * 根据条件查询菜单列表
     *
     * @param tsMenu
     * @return
     * @throws Exception
     */
    @Override
    public List<TsMenu> findTsMenuPage(TsMenu tsMenu) throws Exception {


        return tsMenuMapper.findTsMenuPage(tsMenu);
    }

    /**
     * 保存菜单信息
     *
     * @param tsMenu
     * @return
     * @throws Exception
     */
    @Override
    public Integer saveOrUpdateTsMenu(TsMenu tsMenu) throws Exception {

        if (StringUtils.isEmpty(tsMenu.getMenuId())) {
            if (tsMenu.getParentMenuId() == null)
                tsMenu.setParentMenuId(-1l);
            return tsMenuMapper.insertSelective(tsMenu);
        } else {
            return tsMenuMapper.updateByPrimaryKeySelective(tsMenu);
        }
    }

    /**
     * 删除菜单信息
     *
     * @param menuId
     * @return
     */
    @Override
    public Integer delTsMenu(Long menuId) throws Exception {

        return tsMenuMapper.deleteByPrimaryKey(menuId);
    }

    /**
     * 根据菜单ID查询菜单详情
     *
     * @param menuId
     * @return
     * @throws Exception
     */
    @Override
    public TsMenuV findTsMenuById(Long menuId) throws Exception {
        TsMenuV tsMenuV = new TsMenuV();
        TsMenu tsMenu = tsMenuMapper.selectByPrimaryKey(menuId);
        BeanUtils.copyProperties(tsMenu, tsMenuV);
        // 查询上级菜单
        if (tsMenu.getParentMenuId() > 0) {
            tsMenu = tsMenuMapper.selectByPrimaryKey(tsMenu.getParentMenuId());
            tsMenuV.setParentMenuName(tsMenu.getMenuName());
        } else {
            tsMenuV.setParentMenuName("");
        }

        return tsMenuV;
    }


    /**
     * 根据父菜单顺序、菜单顺序查询排序
     *
     * @return
     * @throws Exception
     */
    @Override
    public List<TsMenu> findByOrders(TsUser tsUser) throws Exception {

        return tsMenuMapper.findByOrders(tsUser);
    }


}
