package com.yuanlang.services.system;

import com.alibaba.fastjson.JSONObject;
import com.yuanlang.common.ResultMap;
import com.yuanlang.facade.model.system.TsUser;

import java.util.List;

/**
 * company 重庆源狼软件科技有限责任公司
 * FileName TsUserService
 * Package com.soft.link.service
 * Description 人员基本信息表服务
 * author Administrator
 * create 2018-03-31 22:04
 * version V1.0
 */

public interface TsUserService {

    /**
     * @param loginName 根据人员信息登录名 和 密码查询人员
     * @param password
     * @return
     * @throws Exception
     */
    public TsUser findTsUser(String loginName, String password) throws Exception;


    /**
     * @methodname findTsUserByUserId
     * @param userId 人员Id
     * @return 人员基本信息
     */
    public TsUser findTsUserByUserId(Long userId) throws Exception ;

    /**
     * @param loginName 根据登录名查找人员
     */
    public List<TsUser> findTsUser(String loginName) throws Exception ;

    /**
    * @methodname findTsUserList
    * @Description 根据对象查询 账户信息
    * @author 黄学乾
    * @create 2018/4/5/005 21:51
    * @param  * @param tsUser 账号实体
    */
    List<TsUser> findTsUserList(TsUser tsUser, String deptCode) throws Exception;

    /**
    * @methodname   findTsUserPage
    * @Description  人员信息列表分页
    * @author 黄学乾
    * @create 2018/4/23/023 15:57
    * @param  * @param tsUser 人员账户
    */
    public ResultMap findTsUserPage(TsUser tsUser,String deptCode) throws Exception ;


    /**
     * @methodname   saveTsUser
     * @Description  用户新增
     * @author 黄学乾
     * @create 2018/5/16  15:57
     * @param tsUser 用户信息
     */
    Integer saveTsUser(TsUser tsUser) throws Exception ;


    /**
     * @methodname   updateTsUser
     * @Description  用户编辑
     * @author 黄学乾
     * @create 2018/5/16  15:57
     * @param tsUser 用户信息
     */
    Integer updateTsUser(TsUser tsUser) throws Exception ;


    /**
     * @methodname  updateTsUser
     * @Description 用户删除
     * @author 黄学乾
     * @create 2018/5/16  15:57
     * @param tsUserList 用户信息
     */
    Integer delTsUser(List<TsUser> tsUserList) throws Exception ;

}
