package com.yuanlang.services.system;

import com.alibaba.fastjson.JSONObject;
import com.yuanlang.facade.model.system.*;

import java.util.List;

/**
 * company 重庆源狼软件科技有限责任公司
 * FileName TsRoleService
 * Package com.soft.link.service
 * Description 角色服务接口类
 * author Administrator
 * create 2018-03-31 22:16
 * version V1.0
 */

public interface TsRoleService {


    List<TsRole> findTsRoleList(TsRole tsRole, String deptCode) throws Exception;

    TsRole findTsRoleById(Long roleId) throws Exception;

    Integer addOrEditTsRole(TsRole tsRole) throws Exception;

    Integer delateByRoleId(Long roleId) throws Exception;

    List<TsRole> findTsRoleListByLoginUser(TsUser tsUser) throws Exception;
}
