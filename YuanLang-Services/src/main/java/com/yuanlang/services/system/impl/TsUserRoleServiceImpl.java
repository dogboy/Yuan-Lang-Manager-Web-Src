package com.yuanlang.services.system.impl;

import com.yuanlang.dao.mapper.system.TsUserRoleMapper;
import com.yuanlang.facade.model.system.TsRole;
import com.yuanlang.facade.model.system.TsUserRole;
import com.yuanlang.services.system.TsUserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * company 重庆源狼软件科技有限责任公司
 * FileName TsUserRoleServiceImpl
 * Package com.soft.link.impl
 * Description 人员角色接口实现
 * author Administrator
 * create 2018-03-31 22:20
 * version V1.0
 */
@Service
public class TsUserRoleServiceImpl implements TsUserRoleService {
    @Autowired(required = false)
    TsUserRoleMapper tsUserRoleMapper;

    /**
     * @param userId 根据人员ID 查询人员所拥有的角色
     * @methodname findTsUserRoleList
     * @Description 查询当前人员组织机构 ；
     * @author 黄学乾
     * @create 2018/3/31/031 22:36
     */
    @Override
    public List<TsUserRole> findTsUserRoleList(Long userId) throws Exception {
        List<TsUserRole> tsUserRoleList = tsUserRoleMapper.findListByUserId(userId);
        return tsUserRoleList;
    }

    /**
     * @param tsRoles 角色基本信息
     * @throws {数据库连接异常和其他异常}
     * @methodname findTsUserRoleListByTsRole
     * @Description {根据角色，查询人员角色关联信息}
     * @author 黄学乾
     * @create 2018/4/30/030 16:36
     */
    @Override
    public List<TsUserRole> findTsUserRoleListByTsRole(List<TsRole> tsRoles) {
        List<TsUserRole> ts = new ArrayList<>();
        if (tsRoles != null && tsRoles.size() > 0) {

        }
        return ts;
    }

    /**
     * 根据角色ID查询用户角色集合
     * @param roleId
     * @return
     * @throws Exception
     */
    @Override
    public List<TsUserRole> findTsUserRoleListByRoleId(Long roleId) throws Exception{

        return tsUserRoleMapper.findTsUserRoleListByRoleId(roleId);
    }

    /**
     * 判断是否为管理员
     * @param userId
     * @return
     * @throws Exception
     */
    @Override
    public Boolean isAdmin(Long userId) throws Exception{
        List<TsUserRole> tsUserRoleList = tsUserRoleMapper.findIsAdmin(userId);
        if(tsUserRoleList != null && tsUserRoleList.size() > 0){
            return true;
        }else{
            return false;
        }
    }

    /**
     * 根据用户ID删除配置的角色
     * @param userId
     * @return
     * @throws Exception
     */
    @Override
    public Integer deleteByUserId(Long userId) throws Exception{

        return tsUserRoleMapper.deleteByUserId(userId);
    }

    /**
     * 配置用户角色
     * @param tsUserRole
     * @return
     * @throws Exception
     */
    @Override
    public Integer add(TsUserRole tsUserRole) throws Exception{

        return tsUserRoleMapper.insertSelective(tsUserRole);
    }
}
