package com.yuanlang.services.system;

import com.yuanlang.facade.model.system.TsSMS;

/**
 * company 重庆源狼软件科技有限责任公司
 * FileName TsSMSService
 * Package com.yuanlang.services.system
 * Description 短信发送和存储内容
 * author Administrator
 * create 2018-06-25 11:47
 * version V1.0
 */

public interface TsSMSService {

    /**
     * @param tsSMS 短信实体
     */
    int addTsSMS(TsSMS tsSMS) throws Exception ;

    /**
     * @param   tsSMS
     * @return  更新状态
     * @throws  Exception
     */
    int updateTsSMS(TsSMS tsSMS) throws Exception ;
}
