package com.yuanlang.services.im;

import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import java.util.Map;

/**
 * company 重庆源狼软件科技有限责任公司
 * FileName ImMessageHandler
 * Package com.yuanlang.services.im
 * Description layIM消息处理程序
 * author Administrator
 * create 2018-05-19 16:00
 * version V1.0
 */

public interface ImMessageHandlerService {

    /**
     * @param message layIM聊天发送的消息
     * @param users   当前在线用户
     */
     void messageHandler(TextMessage message , Map<Long, WebSocketSession> users) throws Exception ;

}
