package com.yuanlang.services.im.impl;

import com.yuanlang.dao.mapper.im.ImUserMapper;
import com.yuanlang.facade.model.im.ImUser;
import com.yuanlang.services.im.ImUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * company 重庆源狼软件科技有限责任公司
 * FileName ImUserServiceImpl
 * Package com.yuanlang.services.im
 * Description layerImUser服务实现
 * author Administrator
 * create 2018-05-18 18:02
 * version V1.0
 */
@Service
public class ImUserServiceImpl implements ImUserService {
    @Autowired(required = false)
    private ImUserMapper imUserMapper;

    /**
     * @param imUser im 用户信息
     * @return 新增用户信息
     */
    @Override
    public int saveSyncImUser(ImUser imUser) throws Exception {
        return imUserMapper.insertSelective(imUser);
    }

    /**
     * @param imUser layerIm账户信息
     *               更新imUser信息
     */
    @Override
    public int updateImUser(ImUser imUser) throws Exception {
        return imUserMapper.updateByPrimaryKeySelective(imUser);
    }


    /**
     * @param imUserId 用户Id
     * @return 返回IM用户信息
     */
    @Override
    public ImUser findImUser(Long imUserId) throws Exception {
        return imUserMapper.selectByPrimaryKey(imUserId);
    }

    /**
     * @param groupId 根据分组id查询人员账户信息状态
     * @return
     * @throws Exception
     */
    @Override
    public List<ImUser> queryImUserListByGroupId(Long groupId) throws Exception {
        return imUserMapper.queryImUserListByGroupId(groupId);
    }

    /**
     * @param userName 根据姓名查找好友
     */
    @Override
    public List<ImUser> queryImUserListByUserName(String userName) throws Exception {
        Map param = new HashMap();
        param.put("userName",userName);
        return imUserMapper.queryImUserListByUserName(param);
    }
}
