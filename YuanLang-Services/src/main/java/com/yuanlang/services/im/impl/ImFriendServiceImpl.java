package com.yuanlang.services.im.impl;

import com.yuanlang.dao.mapper.im.ImFriendMapper;
import com.yuanlang.facade.model.im.ImFriend;
import com.yuanlang.services.im.ImFriendService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * company 重庆源狼软件科技有限责任公司
 * FileName ImFriendServiceImpl
 * Package com.yuanlang.services.im.impl
 * Description 我的好友信息
 * author Administrator
 * create 2018-05-19 9:44
 * version V1.0
 */
@Service
public class ImFriendServiceImpl implements ImFriendService {
    @Autowired(required = false)
    ImFriendMapper friendMapper ;
    /**
     * @param imUserId 当前登录用户
     * @return 用户相关列表
     */
    @Override
    public List<ImFriend> queryImFriendList(String imUserId) throws Exception {
      return friendMapper.queryImFriendList(imUserId);
    }

    /**
     * @param groupId 分组id 根据组查询人员
     */
    @Override
    public List<ImFriend> queryImFriendListByGroup(String groupId) throws Exception {
        return friendMapper.queryGroupImFriendList(groupId);
    }

    /**
     * @param friend 好友信息， 新增好友
     */
    @Override
    public int saveImFriend(ImFriend friend) throws Exception {

        return friendMapper.insertSelective(friend);
    }
}
