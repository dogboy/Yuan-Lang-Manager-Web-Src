package com.yuanlang.services.im;

import com.yuanlang.facade.model.im.ImFriend;

import java.util.List;

/**
 * company 重庆源狼软件科技有限责任公司
 * FileName ImFriendService
 * Package com.yuanlang.services.im
 * Description 我的好友
 * author Administrator
 * create 2018-05-19 9:42
 * version V1.0
 */

public interface ImFriendService {

    /**
     * queryImFriendList 根据登录用户查询
     *
     * @param imUserId 当前登录用户
     *                 返回 我的好友列表
     */
    List<ImFriend> queryImFriendList(String imUserId) throws Exception;

    /**
     * 根据组查询人员
     */
    List<ImFriend> queryImFriendListByGroup(String groupId) throws Exception ;

    /**
     * 保存好友信息（同步好友操作）
     */
    int saveImFriend(ImFriend friend) throws Exception ;

}
