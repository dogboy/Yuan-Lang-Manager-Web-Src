package com.yuanlang.services.im;

import com.yuanlang.facade.model.im.ImGroup;
import sun.util.resources.cldr.ga.LocaleNames_ga;

import java.util.List;

/**
 * company 重庆源狼软件科技有限责任公司
 * FileName ImGroupService
 * Package com.yuanlang.services.im
 * Description layer用户组群信息
 * author Administrator
 * create 2018-05-19 9:13
 * version V1.0
 */

public interface ImGroupService {

    /**
     * queryImGroupList 查询我的组群
     * @param imUserId 用户id
     */
    List<ImGroup> queryImGroupList(Long imUserId) throws Exception ;


    /**
     * queryImGroupList 查询我的组群
     * @param groupName 用户id
     */
    List<ImGroup> queryImGroupListByGroupName(String groupName) throws Exception ;

    /**
     * @param group 分组信息
     */
    Integer saveSyncImGroup(ImGroup group,String status) throws Exception ;

    /**
     * @param imUserId 用户id
     */
    ImGroup findMyFriendImGroup(Long imUserId) throws Exception ;

    /**
     * @param imUserId
     * 查询排除自己的所有的好友
     */
    List<ImGroup> findExcludeMyFriendImGroup(Long imUserId) throws Exception ;


}
