package com.yuanlang.services.im.impl;

import com.yuanlang.dao.mapper.im.ImGroupMapper;
import com.yuanlang.facade.model.im.ImGroup;
import com.yuanlang.services.im.ImGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * company 重庆源狼软件科技有限责任公司
 * FileName ImGroupServiceImpl
 * Package com.yuanlang.services.im.impl
 * Description 用户组群
 * author Administrator
 * create 2018-05-19 9:15
 * version V1.0
 */

@Service
public class ImGroupServiceImpl implements ImGroupService {
    @Autowired(required = false)
    private ImGroupMapper groupMapper ;
    /**
     * @param imUserId 用户id
     */
    @Override
    public List<ImGroup> queryImGroupList(Long imUserId) throws Exception {
        return groupMapper.queryImGroupList(imUserId);
    }

    /**
     * @param groupName 群组名称
     * @return
     * @throws Exception
     */
    @Override
    public List<ImGroup> queryImGroupListByGroupName(String groupName) throws Exception {
        return groupMapper.queryImGroupListByGroupName(groupName) ;
    }


    /**
     * @param group 分组信息
     * @param status 同步更新小组LayIM主键中去
     */
    @Override
    public Integer saveSyncImGroup(ImGroup group,String status) throws Exception {
        int code = 0 ;
        if(status.equals("add")){
            code = groupMapper.insertSelective(group);
        }else if(status.equals("update")){
            code = groupMapper.updateByPrimaryKeySelective(group);
        }
        return code ;
    }

    @Override
    public ImGroup findMyFriendImGroup(Long imUserId) throws Exception {
        return groupMapper.findMyFriendImGroup(imUserId);
    }

    @Override
    public List<ImGroup> findExcludeMyFriendImGroup(Long imUserId) throws Exception {
        return groupMapper.findExcludeMyFriendImGroup(imUserId);
    }
}
