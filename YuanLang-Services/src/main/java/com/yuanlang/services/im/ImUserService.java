package com.yuanlang.services.im;

import com.yuanlang.facade.model.im.ImUser;

import java.util.List;

/**
 * company 重庆源狼软件科技有限责任公司
 * FileName ImUserService
 * Package com.yuanlang.services.im
 * Description LayerIm用户服务
 * author Administrator
 * create 2018-05-18 18:00
 * version V1.0
 */

public interface ImUserService {

    /**
     * @param imUser im 用户信息
     */
    int saveSyncImUser(ImUser imUser) throws Exception ;

    /**
     * @param imUser
     * @return 更新用户信息， 包括状态，在线，离线 ，删除
     * @throws Exception
     */
    int updateImUser(ImUser imUser) throws Exception ;


    /**
     * @param imUserId
     * @return 返回 Im用户信息
     */
    ImUser findImUser(Long imUserId) throws Exception ;


    /**
     * @param groupId 根据分组id查询人员账户信息状态
     */
    List<ImUser> queryImUserListByGroupId(Long groupId) throws Exception ;

    /**
     * @param userName
     */
    List<ImUser> queryImUserListByUserName(String userName) throws Exception ;



}
