package com.yuanlang.services.im.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.yuanlang.common.util.UniqueUtil;
import com.yuanlang.facade.model.im.ImUserV;
import com.yuanlang.facade.model.im.util.ImSocketMsg;
import com.yuanlang.services.im.ImMessageHandlerService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * company 重庆源狼软件科技有限责任公司
 * FileName ImMessageHandlerServiceImpl layIM消息处理服务
 * Package com.yuanlang.services.im.impl
 * Description
 * author Administrator
 * create 2018-05-19 16:02
 * version V1.0
 */
@Service
public class ImMessageHandlerServiceImpl implements ImMessageHandlerService {



    private static Logger logger = LoggerFactory.getLogger(ImMessageHandlerServiceImpl.class);


    /**
     * socket 接受到的消息
     * @param  message 接受到的消息
     * @param  users   在线的用户集合
     */
    @Override
    public void messageHandler(TextMessage message, Map<Long, WebSocketSession> users) throws Exception {
            String messageStr = message.getPayload();
            if(StringUtils.isNotEmpty(messageStr)){
                ImSocketMsg msg = JSON.parseObject(messageStr,ImSocketMsg.class);
                // 查看是发给个人 还是 给群的消息 ;
                // 记录给数据库，存储消息队列， 在队列中进行数据操作;
                switch (msg.getType()){
                    case "sendMessage":
                        // 发送消息 ;
                        //发送对应的消息
                        dealWith(messageStr,users);
                        break;
                }
            }else{
                logger.info("ImMessageHandlerServiceImpl-------messageHandler-------messageStr:为空");
            }
    }


    /**
     *  消息解析 和 处理
     */
    private void dealWith(String messageStr, Map<Long, WebSocketSession> onLineUsers) {

        JSONObject messageContent = JSON.parseObject(messageStr);
        String type = messageContent.getString("type");
        String data = messageContent.getString("data");

        JSONObject msgObj = JSON.parseObject(data);
        // 获取发送的消息
        String mine = msgObj.getString("mine");
        String to = msgObj.getString("to");
        ImUserV imUserV = JSON.parseObject(mine, ImUserV.class);
        ImUserV toUser = JSON.parseObject(to, ImUserV.class);
        String isGroup = "";
        if (org.springframework.util.StringUtils.isEmpty(toUser.getType())) {
            isGroup = "friend";
        } else {
            isGroup = toUser.getType();
        }

        if (isGroup.equals("friend")) {
            WebSocketSession session = onLineUsers.get(toUser.getId());
            // 转换成接受的消息
            /*
                username: "纸飞机" //消息来源用户名
                ,avatar: "http://tp1.sinaimg.cn/1571889140/180/40030060651/1" //消息来源用户头像
                ,id: "100000" //消息的来源ID（如果是私聊，则是用户id，如果是群聊，则是群组id）
                ,type: "friend" //聊天窗口来源类型，从发送消息传递的to里面获取
                ,content: "嗨，你好！本消息系离线消息。" //消息内容
                ,cid: 0 //消息id，可不传。除非你要对消息进行一些操作（如撤回）
                ,mine: false // 是否我发送的消息，如果为 true，则会显示在右方
                ,fromid: "100000" //消息的发送者id（比如群组中的某个消息发送者），可用于自动解决浏览器多窗口时的一些问题
                ,timestamp: 1467475443306 //服务端时间戳毫秒数。注意：如果你返回的是标准的 unix 时间戳，记得要 *1000
            */
            ImUserV v = new ImUserV();
            v.setUsername(imUserV.getUsername());
            v.setCid(UniqueUtil.uuid());
            v.setId(imUserV.getId());
            v.setContent(imUserV.getContent());
            v.setFromid(imUserV.getId());
            v.setTimestamp(new Date());
            v.setAvatar(imUserV.getAvatar());
            Map<String, Object> emit = new HashMap<>();
            emit.put("emit", "chatMessage");
            emit.put("data", v);
            try {
                if (session != null) {
                    session.sendMessage(new TextMessage(JSON.toJSONString(emit)));
                } else {
                    // 当 session 为空的时候放入采用 离线 logger.info();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else if(toUser.getType().equals("group")){

        }

    }

}
