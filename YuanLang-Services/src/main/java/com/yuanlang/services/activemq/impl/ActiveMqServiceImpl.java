package com.yuanlang.services.activemq.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.yuanlang.facade.model.im.ImFriend;
import com.yuanlang.facade.model.im.ImGroup;
import com.yuanlang.facade.model.system.TsUser;
import com.yuanlang.services.activemq.ActiveMqService;
import com.yuanlang.services.im.ImFriendService;
import com.yuanlang.services.im.ImGroupService;
import com.yuanlang.services.system.TsUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * company 重庆源狼软件科技有限责任公司
 * FileName ActiveMqServiceImpl
 * Package com.yuanlang.services.activemq.impl
 * Description ActiveMq问题处理
 * author Administrator
 * create 2018-05-28 10:40
 * version V1.0
 */
@Service
public class ActiveMqServiceImpl implements ActiveMqService {
    private static Logger logger = LoggerFactory.getLogger(ActiveMqServiceImpl.class);
    @Autowired(required = false)
    TsUserService tsUserService ;
    @Autowired(required = false)
    ImFriendService friendService ;
    @Autowired(required = false)
    ImGroupService groupService ;

    /**
     * @param message 消息信息(接受消息中心)
     * @throws Exception
     */
    @Override
    public void acceptMessageCenter(String message) throws Exception {
        JSONObject jsonObject = JSON.parseObject(message);
        int code = jsonObject.getInteger("ActiveMqMsgCode");
        Object data = jsonObject.get("ActiveMqMsgData");
        dealMessageCenter(code, data);
    }

    /**
     * @param code 处理消息中心
     */
    @Override
    public void dealMessageCenter(int code, Object data) {
        // 消息处理中心
        switch (code) {
            case 1000001:
                // 同步信息，将部门下所有的字符串和部门下所有的数据进行
                try {
                    List<TsUser> tsUserList = tsUserService.findTsUserList(null,null);
                    if(tsUserList != null && tsUserList.size() > 0){
                        TsUser tsUser = JSON.parseObject(JSON.toJSONString(data),TsUser.class);
                        // 查询 1我的好友 2 群组信息 3 私密信息

                        ImGroup group =  groupService.findMyFriendImGroup(tsUser.getUserId());
                        for(TsUser friend:tsUserList){
                            // 将其他人添加成我的好友
                            ImFriend ifd = new ImFriend();
                            ifd.setTimestamp(new Date());
                            ifd.setUserid(friend.getUserId());
                            ifd.setStatus(1); //
                            ifd.setGroupid(group.getId());
                            friendService.saveImFriend(ifd);
                            ImGroup friendImGroup =  groupService.findMyFriendImGroup(friend.getUserId());
                            // 将我添加成其他人的好友 contain / exclude
                            ImFriend other = new ImFriend();
                            other.setTimestamp(new Date());
                            other.setUserid(tsUser.getUserId());
                            other.setStatus(1); //
                            other.setGroupid(friendImGroup.getId());
                            friendService.saveImFriend(other);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                logger.info("111");
                break;
            default:
                logger.info("111");
                break;
        }
    }
}
