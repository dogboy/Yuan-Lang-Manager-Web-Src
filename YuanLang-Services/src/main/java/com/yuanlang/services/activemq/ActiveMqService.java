package com.yuanlang.services.activemq;

/**
 * company 重庆源狼软件科技有限责任公司
 * FileName ActiveMqService
 * Package com.yuanlang.services.activemq.impl
 * Description 消息处理队列
 * author Administrator
 * create 2018-05-28 10:41
 * version V1.0
 */

public interface ActiveMqService {

    /**
     *  消息处理器
     * @param msg
     */
    void acceptMessageCenter(String msg) throws Exception ;


    void dealMessageCenter(int code,Object data);
}
