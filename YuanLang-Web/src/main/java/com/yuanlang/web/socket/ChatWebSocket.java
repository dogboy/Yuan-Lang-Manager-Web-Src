package com.yuanlang.web.socket;


import org.springframework.web.socket.handler.TextWebSocketHandler;

/**
 * 聊天室消息
 *
 * @author 王镇
 * @create 2017-08-04 14:13
 */
public class ChatWebSocket extends TextWebSocketHandler {
}
