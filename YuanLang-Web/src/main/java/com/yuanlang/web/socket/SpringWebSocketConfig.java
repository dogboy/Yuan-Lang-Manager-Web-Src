package com.yuanlang.web.socket;

import com.yuanlang.web.interceptor.HandshakeInterceptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;

/**
 * company 重庆源狼软件科技有限责任公司
 * FileName HandshakeInterceptor
 * Package com.yuanlang.web.interceptor
 * Description 配置hadler  拦截器
 * author huangxueqian
 * create 2018-05-18 9:52
 * version V1.0
 */
/*@Configuration
@EnableWebSocket*/
@Configuration
@EnableWebMvc
@EnableWebSocket
public class SpringWebSocketConfig extends WebMvcConfigurerAdapter implements WebSocketConfigurer {
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    @Override
    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
        registry.addHandler(messageWebSocket(), "/socket/socket.do").setAllowedOrigins("*").addInterceptors(new HandshakeInterceptor());
        logger.info("SpringWebSocketConfig--->registed!");
        registry.addHandler(messageWebSocket(), "/socket/socket.do").setAllowedOrigins("*").addInterceptors(new HandshakeInterceptor()).withSockJS();
    }

    @Bean
    public WebSocketHandler messageWebSocket() {
        return new WebSocketMessageHandler();
    }
}
