package com.yuanlang.web.activemq.listen;

import com.alibaba.fastjson.JSON;
import com.yuanlang.common.util.ActiveMqMsgCode;
import com.yuanlang.services.activemq.ActiveMqService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

/**
 * company 重庆源狼软件科技有限责任公司
 * FileName TopicMessageListen
 * Package com.yuanlang.web.activemq.listen
 * Description
 * author Administrator
 * create 2018-05-18 14:57
 * version V1.0
 */

public class TopicMessageListen implements MessageListener {
    private static Logger logger = LoggerFactory.getLogger(TopicMessageListen.class);
    @Autowired(required = false)
    ActiveMqService activeMqService ;
    public void onMessage(Message message) {
        try {
            TextMessage tm = (TextMessage)(message);
            logger.info("TopicMessageListen---------------->"+tm.getText());
            String msg = tm.getText();
            activeMqService.acceptMessageCenter(msg);
           /* jsonObject.put("ActiveMqMsgCode", ActiveMqMsgCode.BIZ_IM_USER.getResultCode());
            jsonObject.put("ActiveMqMsgData",tsUser);*/
        } catch (Exception e) {
            logger.info("TopicMessageListen---------------->"+ JSON.toJSONString(message)+":------------->"+e.getLocalizedMessage());
            e.printStackTrace();
        }

    }

}