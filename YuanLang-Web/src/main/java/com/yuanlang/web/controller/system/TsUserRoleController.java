package com.yuanlang.web.controller.system;

import com.yuanlang.common.ResultMap;
import com.yuanlang.common.util.SystemConsts;
import com.yuanlang.common.util.SystemMsgCode;
import com.yuanlang.facade.model.BaseModel;
import com.yuanlang.facade.model.system.*;
import com.yuanlang.services.redis.RedisClientTemplate;
import com.yuanlang.services.system.TsRoleService;
import com.yuanlang.services.system.TsUserRoleService;
import com.yuanlang.web.controller.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;

/**
 * company 重庆源狼软件科技有限责任公司
 * FileName TsUserRoleController
 * Package com.soft.link.controller.tsystem
 * Description 人员角色显示层
 * author Administrator
 * create 2018-04-27 11:16
 * version V1.0
 */

@Controller
@RequestMapping("system/tsUserRole")
public class TsUserRoleController extends BaseController {

    // 用户角色服务
    @Autowired(required = false)
    TsUserRoleService tsUserRoleService;
    @Autowired(required = false)
    TsRoleService tsRoleService;
    @Autowired
    RedisClientTemplate redisClientTemplate;


    /**
     * 根据用户ID查询配置角色树
     *
     * @param userId
     * @return
     */
    @RequestMapping("/findRoleTree")
    @ResponseBody
    public List<TreeModel> findRoleTree(Long userId) {
        if (userId == null) return null;

        try {
            // 查询所有角色
            TsUser loginUser = getTsUser();
            List<TsRole> tsRoleList = null;
            tsRoleList = tsRoleService.findTsRoleListByLoginUser(loginUser);
            List<TsUserRole> checkedRoles = tsUserRoleService.findTsUserRoleList(userId);
            List<TreeModel> treeModels = getTreeModel(tsRoleList, checkedRoles);

            return treeModels;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * 用户配置角色
     * @param userId
     * @param checkedRoleIdList
     * @return
     */
    @RequestMapping("/setUserRole")
    @ResponseBody
    public ResultMap setUserRole(Long userId, String checkedRoleIdList){
        ResultMap resultMap = new ResultMap();

        try {
            TsUser tsUser = getTsUser();
            // 删除之前配置的角色
            tsUserRoleService.deleteByUserId(userId);
            TsUserRole tsUserRole = null;
            for (String roleId : checkedRoleIdList.split(",")) {
                tsUserRole = new TsUserRole();
                tsUserRole.setUserId(userId);
                tsUserRole.setRoleId(Long.parseLong(roleId));

                tsUserRole.setCreatorId(tsUser.getUserId());
                tsUserRole.setCreator(tsUser.getUserName());
                tsUserRole.setCreateTime(Calendar.getInstance().getTime());
                tsUserRole.setModifyerId(tsUser.getUserId());
                tsUserRole.setModifyer(tsUser.getUserName());
                tsUserRole.setModifyTime(Calendar.getInstance().getTime());
                tsUserRole.setStatus(SystemConsts.SYS_STATUS_ENABLED);

                tsUserRoleService.add(tsUserRole);
            }
            resultMap.setCode(SystemMsgCode.TS_ROLE_OPER_SUCCESS.getResultCode());
            resultMap.setMsg(SystemMsgCode.TS_ROLE_OPER_SUCCESS.getResultMessage());
        }catch (Exception e){
            resultMap.setCode(SystemMsgCode.TS_ROLE_OPER_FAILED.getResultCode());
            resultMap.setMsg(SystemMsgCode.TS_ROLE_OPER_FAILED.getResultMessage() + e.getLocalizedMessage());
        }

        return resultMap;
    }


    /**
     * 封装角色列表
     *
     * @param tsRoleList
     * @param checkedRoles
     * @return
     */
    private List<TreeModel> getTreeModel(List<TsRole> tsRoleList, List<TsUserRole> checkedRoles) {

        List<TreeModel> treeModels = new ArrayList<TreeModel>();
        List<Long> checkRoleIdList = new ArrayList<Long>();
        for (TsUserRole userRole : checkedRoles) {
            checkRoleIdList.add(userRole.getRoleId());
        }
        for (TsRole tsRole : tsRoleList) {
            TreeModel treeModel = new TreeModel();
            treeModel.setId(tsRole.getRoleId());
            treeModel.setName(tsRole.getRoleName());
            treeModel.setChildren(new ArrayList<TreeModel>());
            if (checkRoleIdList.contains(tsRole.getRoleId())) {
                treeModel.setChecked(true);
            }

            treeModels.add(treeModel);
        }

        return treeModels;
    }
}
