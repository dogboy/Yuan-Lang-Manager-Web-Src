package com.yuanlang.web.controller.core.im;

import com.alibaba.fastjson.JSON;
import com.yuanlang.common.ResultMap;
import com.yuanlang.facade.model.im.ImGroup;
import com.yuanlang.facade.model.im.ImUser;
import com.yuanlang.services.im.ImFriendService;
import com.yuanlang.services.im.ImGroupService;
import com.yuanlang.services.im.ImUserService;
import com.yuanlang.web.controller.BaseController;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * company 重庆源狼软件科技有限责任公司
 * FileName ImController
 * Package com.yuanlang.web.controller.core.im
 * Description LayIM相关操作
 * author Administrator
 * create 2018-05-19 8:59
 * version V1.0
 */

@Controller
@RequestMapping("system/im/api")
public class ImApiController extends BaseController {

    @Autowired(required = false)
    public ImUserService imUserService;   // Im用户信息

    @Autowired(required = false)
    public ImGroupService imGroupService;  // Im群组

    @Autowired(required = false)
    ImFriendService imFriendService;


    /**
     * layerIM插件
     */
    @RequestMapping("/init")
    @ResponseBody
    public ResultMap init() {
        ResultMap result = new ResultMap();
        Long imUserId = getTsUser().getUserId();
        try {
            /**
             *  1、获取我的信息
             */
            ImUser imUser = imUserService.findImUser(imUserId);
            Map<String, Object> init = new HashMap<>();
            init.put("mine", imUser);
            List<ImGroup> groupAllList = imGroupService.queryImGroupList(imUserId);
            List<ImGroup> groupMyList = new ArrayList<>();
            List<ImGroup> groupFriendList = new ArrayList<>();
            if (groupAllList != null && groupAllList.size() > 0) {
                List<ImUser> friendImUserList = new ArrayList<>();
                List<ImUser> groupImUserList = new ArrayList<>();
                for (ImGroup group : groupAllList) {
                    List<ImUser> imUsers = imUserService.queryImUserListByGroupId(group.getId());
                    if(imUsers!=null && imUsers.size() > 0){
                        for(ImUser user:imUsers){
                            if(user.getUserStatus() == 1){
                                friendImUserList.add(user);
                            }else{
                                groupImUserList.add(user);
                            }
                        }
                    }
                    if (group.getStatus() == 1) {
                        //好友 {1：好友；2群聊}
                        group.setList(friendImUserList);
                        groupFriendList.add(group);
                    } else if(group.getStatus() == 2) {
                        group.setList(groupImUserList);
                        //群聊
                        groupMyList.add(group);
                    }
                }
            }
            /**
             *  2、获取我的好友
             */
            init.put("friend", groupFriendList);
            /**
             *  3、获取我的组群
             */
            init.put("group", groupMyList);
            result.setCode(0);
            result.setMsg("初始化数据");
            result.setData(init);

        } catch (Exception e) {
            result.setCode(0);
            result.setMsg("初始化异常" + e.getLocalizedMessage());
            e.printStackTrace();
        }
        logger.info(JSON.toJSONString(result));
        return result;
    }


    /**
     *  获取群组成员
     */
    @RequestMapping("/im/members")
    public ResultMap getMembers(ImGroup group){
        ResultMap result = new ResultMap();
        try {
            List<ImUser> imUsers = imUserService.queryImUserListByGroupId(group.getId());
            result.setCode(0);
            result.setMsg("成功获取群组");
            result.setData(imUsers);
        } catch (Exception e) {
            result.setCode(-1);
            result.setMsg("获取群组失败");
            e.printStackTrace();
        }
        return result ;
    }


    /**
     * @param searchType 搜索类型 friend ; group 查找群或者 查找好友
     * @return
     */
    @RequestMapping("/search")
    @ResponseBody
    public ResultMap search(String searchType,String search){
        ResultMap result = new ResultMap();
        if(StringUtils.isEmpty(searchType)){
            searchType = "friend";
        }
        try {
            result.setCode(0);
            if(searchType.equals("friend")){
                List<ImUser> imUsers =  imUserService.queryImUserListByUserName(search) ;
                result.setData(imUsers);
            }else if(searchType.equals("group")){
                List<ImGroup> groupList = imGroupService.queryImGroupListByGroupName(search);
                result.setData(groupList);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result ;
    }



}
