package com.yuanlang.web.controller.core;

import com.alibaba.fastjson.JSON;
import com.yuanlang.common.ResultMap;
import com.yuanlang.services.fasthdsf.FileTransferService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * company 重庆源狼软件科技有限责任公司
 * FileName FileTransferAction
 * Package com.yuanlang.web.controller.core
 * Description 文件上传下载
 * author Administrator
 * create 2018-05-14 18:13
 * version V1.0
 * 服务器fasthdfs存储的时候， 要配置文件需要配置外网访问的IP，不然是连接不上去的。
 */
@Scope("request")
@Controller
@CrossOrigin(origins = "*", maxAge = 3600)
public class FileTransferController {

    private static Logger logger = LoggerFactory.getLogger(FileTransferController.class);
    // 文件上传服务层写法
    @Autowired
    @Qualifier("fileTransferService")
    private FileTransferService fileTransferService;

    /**
     * @param file 单个文件上传
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/base/fileUpload", method = RequestMethod.POST)
    @ResponseBody
    public ResultMap fileUpload(MultipartFile file,
                                HttpServletRequest request, HttpServletResponse response) {
        logger.info("fileUpload-->文件上传");
        ResultMap result = fileTransferService.fileUpload(file);
        logger.info("fileUpload-->文件上传结束");
        return result;
    }

    /**
     * @param files    多个文件 ---> 客户端传入的文件
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/base/filesUpload", method = RequestMethod.POST)
    @ResponseBody
    public ResultMap filesUpload(@RequestParam(value = "files", required = true) MultipartFile[] files,
                                HttpServletRequest request, HttpServletResponse response) {
        logger.info("fileUpload-->文件上传"+ JSON.toJSONString(files));
        ResultMap result = fileTransferService.fileUpload(files);
        logger.info("fileUpload-->文件上传结束");
        return result;
    }


    @RequestMapping(value = "/base/outFileStream", method = {RequestMethod.POST,RequestMethod.GET})
    public void outFileStream(@RequestParam(value = "fileURL", required = true) String fileURL,
                              @RequestParam(value = "kjbDFS", required = true, defaultValue = "true") Boolean kjbDFS,
                              HttpServletRequest request, HttpServletResponse response) {

        logger.info("文件上传流的形式");

        fileTransferService.outFileStream(fileURL, kjbDFS, response);
    }

    @RequestMapping(value = "/base/fileUploadTest", method = RequestMethod.GET)
    @ResponseBody
    public ResultMap fileUpload() {
        return fileTransferService.fileUpload("D:/原理图.jpg");
    }


    /**
     * @param file
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/base/api/upload", method = RequestMethod.POST)
    @ResponseBody
    public ResultMap upLoad(@RequestParam MultipartFile file,
                                HttpServletRequest request, HttpServletResponse response) {
        logger.info("fileUpload-->文件上传");
        response.setHeader("Access-Control-Allow-Origin", "*");
        logger.info("fileUpload-->文件上传结束getOriginalFilename-->"+file.getOriginalFilename());
        logger.info("fileUpload-->文件上传结束getContentType-->"+file.getContentType());
        ResultMap result = fileTransferService.fileUpload(file);
        //ResultMap result = fileTransferService.fileUpload(file.getOriginalFilename());
        logger.info("fileUpload-->文件上传结束"+JSON.toJSONString(result));
        return result;
    }




}
