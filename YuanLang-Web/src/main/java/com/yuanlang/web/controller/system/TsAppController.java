package com.yuanlang.web.controller.system;

import com.yuanlang.common.ResultMap;
import com.yuanlang.common.util.SystemMsgCode;
import com.yuanlang.facade.model.system.TsApp;
import com.yuanlang.services.system.TsAppService;
import com.yuanlang.web.controller.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * company 重庆源狼软件科技有限责任公司
 * FileName TsAppController
 * Package com.yuanlang.web.controller.system
 * Description App升级管理
 * author Administrator
 * create 2018-05-31 14:50
 * version V1.0
 */
@Controller
@RequestMapping("system/tsApp")
@CrossOrigin(origins = "*", maxAge = 3600)
public class TsAppController extends BaseController{
    @Autowired(required = false)
    TsAppService appService ;
    /**
     * 升级管理列表
     */
    @RequestMapping({"findTsAppPage"})
    @ResponseBody
    public ResultMap findTsAppPage(TsApp tsApp){
        ResultMap  result = null;
        try {
            result = appService.findTsAppPage(tsApp);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result ;
    }

    @RequestMapping({"saveTsApp"})
    @ResponseBody
    public ResultMap saveTsApp(TsApp tsApp){
        ResultMap  result = null;
        try {
            tsApp.setStatus(0);
            int code  = appService.saveTsApp(tsApp);
            if(code > 0){
                result = new ResultMap();
                result.setCode(SystemMsgCode.SYS_ADD_SUCCESS.getResultCode());
                result.setMsg(SystemMsgCode.SYS_ADD_SUCCESS.getResultMessage());
            }else{
                result.setCode(SystemMsgCode.SYS_ADD_FAILED.getResultCode());
                result.setMsg(SystemMsgCode.SYS_ADD_FAILED.getResultMessage());
            }
        } catch (Exception e) {
            result.setCode(SystemMsgCode.SYS_ADD_FAILED.getResultCode());
            result.setMsg(SystemMsgCode.SYS_ADD_FAILED.getResultMessage()+e.getLocalizedMessage());
            e.printStackTrace();
        }
        return result ;
    }


    @RequestMapping({"delTsAppId"})
    @ResponseBody
    public ResultMap delTsAppId(Long id){
        ResultMap  result = null;
        try {
            int code  = appService.delTsApp(id);
            if(code > 0){
                result = new ResultMap();
                result.setCode(SystemMsgCode.SYS_DEL_SUCCESS.getResultCode());
                result.setMsg(SystemMsgCode.SYS_DEL_SUCCESS.getResultMessage());
            }else{
                result.setCode(SystemMsgCode.SYS_DEL_FAILED.getResultCode());
                result.setMsg(SystemMsgCode.SYS_DEL_FAILED.getResultMessage());
            }
        } catch (Exception e) {
            result.setCode(SystemMsgCode.SYS_DEL_FAILED.getResultCode());
            result.setMsg(SystemMsgCode.SYS_DEL_FAILED.getResultMessage()+e.getLocalizedMessage());
            e.printStackTrace();
        }
        return result ;
    }

}
