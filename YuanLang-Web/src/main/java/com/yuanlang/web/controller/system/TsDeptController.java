package com.yuanlang.web.controller.system;

import com.alibaba.fastjson.JSON;
import com.sun.source.tree.Tree;
import com.yuanlang.common.ResultMap;
import com.yuanlang.common.util.SystemMsgCode;
import com.yuanlang.facade.model.system.TreeModel;
import com.yuanlang.facade.model.system.TsDept;
import com.yuanlang.facade.model.system.TsUser;
import com.yuanlang.services.system.TsDeptService;
import com.yuanlang.web.controller.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;

/**
 * company 重庆源狼软件科技有限责任公司
 * FileName TsDeptController
 * Package com.yuanlang.web.controller.system
 * Description 部门展示
 * author Administrator
 * create 2018-05-22 21:09
 * version V1.0
 */
@Controller
@RequestMapping("system/tsDept")
public class TsDeptController extends BaseController{
    @Autowired
    private TsDeptService tsDeptService;


    @RequestMapping("/findTsDept")
    @ResponseBody
    public ResultMap findTsDept(Long deptId) {
        ResultMap result = new ResultMap();
        try {
            TsDept tsDept = tsDeptService.findTsDept(deptId);
            result.setCode(SystemMsgCode.SYS_SUCCEED.getResultCode());
            result.setData(tsDept);
        } catch (Exception e) {
            result.setCode(SystemMsgCode.SYS_FAILED.getResultCode());
            result.setMsg(e.getLocalizedMessage());
            e.printStackTrace();
        }
        return result;
    }

    /**
     * @param tsDept 部门信息
     */
    @RequestMapping("findTsUserPage")
    @ResponseBody
    public ResultMap findTsUserPage(TsDept tsDept) {
        ResultMap result = new ResultMap();
        try {
            TsDept loginDept = getTsDept();
            tsDept.setDeptCode(loginDept.getDeptCode());
            result = tsDeptService.findTsDeptPage(tsDept);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }


    /**
     * @param tsDept 新增部门，或者修改部门
     */
    @RequestMapping("saveTsDept")
    @ResponseBody
    public ResultMap saveTsDept(TsDept tsDept){
        ResultMap result = new ResultMap();
        try {
            if(StringUtils.isEmpty(tsDept.getDeptId())){
                tsDept.setCreatorId(getTsUser().getUserId());
                tsDept.setCreator(getTsUser().getUserName());
                tsDept.setCreateTime(new Date());
                tsDept.setModifyerId(getTsUser().getUserId());
                tsDept.setModifyer(getTsUser().getUserName());
                tsDept.setModifyTime(new Date());
                tsDept.setStatus(0);
            }else{
                tsDept.setModifyerId(getTsUser().getUserId());
                tsDept.setModifyer(getTsUser().getUserName());
                tsDept.setModifyTime(new Date());
            }
            int code = tsDeptService.saveTsDeptOrUpdate(tsDept);
            if(code > 0){
                if(StringUtils.isEmpty(tsDept.getDeptId())){
                    result.setCode(SystemMsgCode.SYS_ADD_SUCCESS.getResultCode());
                    result.setMsg(SystemMsgCode.getSystemMsg(SystemMsgCode.SYS_ADD_SUCCESS.getResultCode()));
                }else{
                    result.setCode(SystemMsgCode.SYS_EDIT_SUCCESS.getResultCode());
                    result.setMsg(SystemMsgCode.getSystemMsg(SystemMsgCode.SYS_EDIT_SUCCESS.getResultCode()));
                }

            }else{
                if(StringUtils.isEmpty(tsDept.getDeptId())){
                    result.setCode(SystemMsgCode.SYS_ADD_FAILED.getResultCode());
                    result.setMsg(SystemMsgCode.getSystemMsg(SystemMsgCode.SYS_ADD_FAILED.getResultCode()));
                }else{
                    result.setCode(SystemMsgCode.SYS_EDIT_FAILED.getResultCode());
                    result.setMsg(SystemMsgCode.getSystemMsg(SystemMsgCode.SYS_EDIT_FAILED.getResultCode()));
                }
            }
        } catch (Exception e) {
            if(StringUtils.isEmpty(tsDept.getDeptId())){
                result.setCode(SystemMsgCode.SYS_ADD_FAILED.getResultCode());
                result.setMsg(SystemMsgCode.getSystemMsg(SystemMsgCode.SYS_ADD_FAILED.getResultCode())+e.getLocalizedMessage());
            }else{
                result.setCode(SystemMsgCode.SYS_EDIT_FAILED.getResultCode());
                result.setMsg(SystemMsgCode.getSystemMsg(SystemMsgCode.SYS_EDIT_FAILED.getResultCode())+e.getLocalizedMessage());
            }
            e.printStackTrace();
        }
        return result ;

    }


    @RequestMapping("delTsDept")
    @ResponseBody
    public ResultMap delTsDept(String tsDept){
        ResultMap result = new ResultMap();
        if(org.apache.commons.lang.StringUtils.isNotEmpty(tsDept)){
                List<TsDept> deptList = JSON.parseArray(tsDept,TsDept.class);
            try {
                // 部门下有人不能删除 ， 添加控制， 释放部门下人员
                // 部门下有角色不能删除
                int code = tsDeptService.delTsDept(deptList);
                if(code > 0){
                    result.setCode(SystemMsgCode.SYS_DEL_SUCCESS.getResultCode());
                    result.setMsg(SystemMsgCode.getSystemMsg(SystemMsgCode.SYS_DEL_SUCCESS.getResultCode()));
                }else{
                    result.setCode(SystemMsgCode.SYS_DEL_FAILED.getResultCode());
                    result.setMsg(SystemMsgCode.getSystemMsg(SystemMsgCode.SYS_DEL_FAILED.getResultCode()));
                }
            } catch (Exception e) {
                result.setCode(SystemMsgCode.SYS_DEL_FAILED.getResultCode());
                result.setMsg(SystemMsgCode.getSystemMsg(SystemMsgCode.SYS_DEL_FAILED.getResultCode())+e.getLocalizedMessage());
                e.printStackTrace();
            }
        }
        return result ;
    }

    /**
     * 获取部门和所有下级部门，树形数据样式
     */
    @RequestMapping("getDeptAndChildDepts")
    @ResponseBody
    public List<TreeModel> getDeptAndChildDepts(){
        TsUser tsUser = getTsUser();
        try {
            TsDept tsDept = tsDeptService.findTsDept(tsUser.getDeptId());
            List<TsDept> tsDeptList = tsDeptService.findDeptAndChildDepts(tsDept.getDeptCode());
            List<TreeModel> treeModels = getChildNode(tsDeptList);
            return treeModels;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    private List<TreeModel> getChildNode(List<TsDept> tsDeptList){

        List<TreeModel> list =  new ArrayList<TreeModel>();
        Map<Long, TreeModel> map = new HashMap<Long, TreeModel>();
        TreeModel treeModel = null;
        for (TsDept tsDept : tsDeptList) {
            treeModel = new TreeModel();
            treeModel.setId(tsDept.getDeptId());
            treeModel.setName(tsDept.getDeptName());
            treeModel.setCode(tsDept.getDeptCode());
            treeModel.setChildren(new ArrayList<TreeModel>());
            map.put(tsDept.getDeptId(), treeModel);
        }

        for (TsDept dept : tsDeptList) {
            if(map.containsKey(dept.getParentId())){
                map.get(dept.getParentId()).getChildren().add(map.get(dept.getDeptId()));
                map.remove(dept.getDeptId());
            }
        }

        for (Long deptId : map.keySet()) {
            list.add(map.get(deptId));
        }

        return list;
    }



}
