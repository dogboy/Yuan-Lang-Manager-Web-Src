package com.yuanlang.web.controller.system;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import com.aliyuncs.exceptions.ClientException;
import com.yuanlang.common.ResultMap;
import com.yuanlang.common.util.GeneratorCodeUtil;
import com.yuanlang.common.util.UniqueUtil;
import com.yuanlang.services.aliyunsms.AliyunSmsService;
import com.yuanlang.services.system.TsSMSService;
import com.yuanlang.web.controller.BaseController;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * company 重庆源狼软件科技有限责任公司
 * FileName TsRegisterController
 * Package com.yuanlang.web.controller.system
 * Description 手机人员注册-包括手机快速注册-微信注册-QQ注册
 * author Administrator
 * create 2018-06-25 10:39
 * version V1.0 // SMS_137860134 -- 验证码 {}
 */
@Controller
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("app/tsRegister")
public class TsRegisterController extends BaseController{

    static final String templateCode = "SMS_137860134"; // 发送短信验证码
    @Autowired(required = false)
    AliyunSmsService aliyunSmsService ;
    @Autowired(required = false)
    TsSMSService smsService;
    @RequestMapping("code")
    @ResponseBody
    public ResultMap TsRegisterCode(String tel){
        ResultMap result = new ResultMap();
        result.setCode(0);
        JSONObject object = new JSONObject() ;
        object.put("code", GeneratorCodeUtil.getCode());
        result.setData(object);
        if(StringUtils.isNotEmpty(tel)){
            String outId = UniqueUtil.uuid();
            try {
                SendSmsResponse response = aliyunSmsService.sendSms(tel,templateCode, JSON.toJSONString(object), outId);
              /* RequestId	String	8906582E-6722	请求ID
                 Code	String	OK	状态码-返回OK代表请求成功,其他错误码详见错误码列表
                 Message	String	请求成功	状态码的描述
                 BizId	String	134523^4351232	发送回执ID,可根据该ID查询具体的发送状态
                */
                String code = response.getCode();
                String msg = response.getMessage();
                String requestId = response.getRequestId() ;
                String bizId = response.getBizId() ;
                logger.info("短信接口返回的数据----------------");
                logger.info("Code=" + code);
                logger.info("Message=" + msg);
                logger.info("RequestId=" + requestId);
                logger.info("BizId=" + bizId);
                logger.info("response---->"+JSON.toJSONString(response));
            } catch (ClientException e) {
                e.printStackTrace();
            }
        }
        return result ;
    }

}
