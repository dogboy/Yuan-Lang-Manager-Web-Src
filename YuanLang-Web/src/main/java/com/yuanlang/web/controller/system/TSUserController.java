package com.yuanlang.web.controller.system;

import com.alibaba.fastjson.JSON;
import com.yuanlang.common.ResultMap;
import com.yuanlang.common.util.SystemMsgCode;
import com.yuanlang.facade.model.system.TsDept;
import com.yuanlang.facade.model.system.TsUser;
import com.yuanlang.services.system.TsUserService;
import com.yuanlang.web.controller.BaseController;
import com.yuanlang.common.util.EncryptUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;
import java.util.List;

/**
 * company 重庆源狼软件科技有限责任公司
 * FileName SUserController
 * Package com.soft.link.controller.suser
 * Description 账号管理
 * author Administrator
 * create 2018-03-24 22:46
 * version V1.0
 */
@Controller
@RequestMapping("system/tsUser")
public class TSUserController extends BaseController{

    @Autowired(required = false)
    private TsUserService tsUserService ;

    /**
     * @ methodname ajaxListSUser
     * @Description 获取用户列表，获取数据列表 ；
     * @author 黄学乾
     * @create 2018/3/24/024 22:58
     * @param tsUser
     */
    @RequestMapping("ajaxListSUser")
    @ResponseBody
    public ResultMap ajaxListSUser(TsUser tsUser) {
        ResultMap resultMap = new ResultMap();
        try {
            TsDept loginDept = getTsDept();
            resultMap = tsUserService.findTsUserPage(tsUser,loginDept.getDeptCode());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resultMap;
    }

    @RequestMapping("ajaxSessionTsUser")
    @ResponseBody
    public ResultMap ajaxSessionTsUser() {
        ResultMap resultMap = new ResultMap();
        try {
            TsUser tsUser = getTsUser();
            if (tsUser != null && !StringUtils.isEmpty(tsUser.getUserId())) {
                resultMap.setCode(0);
                resultMap.setData(tsUser);
            } else {
                resultMap.setCode(1001);
                resultMap.setMsg("获取用户信息失败");
            }
        } catch (Exception e) {
            resultMap.setCode(1001);
            resultMap.setMsg("获取用户信息失败");
            e.printStackTrace();
        }
        return resultMap;
    }


    @RequestMapping("findSUser")
    @ResponseBody
    public ResultMap findSUser(Long userId) {
        ResultMap result = new ResultMap();
        result.setCode(0);
        try {
            TsUser tsUser = tsUserService.findTsUserByUserId(userId);
            result.setData(tsUser);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 用户添加
     * save/del/update/find/query
     */
    @RequestMapping("saveTsUser")
    @ResponseBody
    public ResultMap saveTsUser(TsUser tsUser){
        ResultMap result = new ResultMap();
        try {
            tsUser.setPassword("123456");//默认密码
            tsUser.setStatus(0);
            tsUser.setIslock(0);
            tsUser.setCreateTime(new Date());
            tsUser.setCreator(getTsUser().getUserName());
            tsUser.setCreatorId(getTsUser().getUserId());
            tsUser.setModifyerId(getTsUser().getUserId());
            tsUser.setModifyer(getTsUser().getUserName());
            tsUser.setModifyTime(new Date());
            int save = tsUserService.saveTsUser(tsUser);
            if(save>0){
                result.setCode(0);
                result.setMsg(SystemMsgCode.TS_USER_ADD_SUCCESS.getResultMessage());
            }else{
                result.setCode(SystemMsgCode.TS_USER_ADD_FAILED.getResultCode());
                result.setMsg(SystemMsgCode.TS_USER_ADD_FAILED.getResultMessage());
            }
        } catch (Exception e) {
            result.setCode(SystemMsgCode.TS_USER_ADD_FAILED.getResultCode());
            result.setMsg(SystemMsgCode.TS_USER_ADD_FAILED.getResultMessage()+e.getLocalizedMessage());
            e.printStackTrace();
        }
        return  result;
    }

    /**
     * 用户添加
     * save/del/update/find/query
     */
    @RequestMapping("updateTsUser")
    @ResponseBody
    public ResultMap updateTsUser(TsUser tsUser){
        ResultMap result = new ResultMap();
        try {
            tsUser.setModifyerId(getTsUser().getUserId());
            tsUser.setModifyer(getTsUser().getUserName());
            tsUser.setModifyTime(new Date());
            int update = tsUserService.updateTsUser(tsUser);
            if(update  >0){
                result.setCode(0);
                result.setMsg(SystemMsgCode.TS_USER_UPDATE_SUCCESS.getResultMessage());
            }else{
                result.setCode(SystemMsgCode.TS_USER_UPDATE_FAILED.getResultCode());
                result.setMsg(SystemMsgCode.TS_USER_UPDATE_FAILED.getResultMessage());
            }
        } catch (Exception e) {
            result.setCode(SystemMsgCode.TS_USER_UPDATE_FAILED.getResultCode());
            result.setMsg(SystemMsgCode.TS_USER_UPDATE_FAILED.getResultMessage()+e.getLocalizedMessage());
            e.printStackTrace();
        }
        return  result;
    }

    /**
     * 用户删除
     * save/del/update/find/query
     */
    @RequestMapping("delTsUser")
    @ResponseBody
    public ResultMap delTsUser(String tsUserList){
        int del = 0 ;
        ResultMap result = new ResultMap();
        result.setCode(0);
        if(StringUtils.isEmpty(tsUserList)){
            result.setMsg("未选中用户");
        }else{
            try {
                List<TsUser> tsUsers = JSON.parseArray(tsUserList,TsUser.class);
                del =  tsUserService.delTsUser(tsUsers);
                if(del > 0){
                    result.setMsg("成功删除用户");
                }else{
                    result.setMsg("未删除用户");
                }
            } catch (Exception e) {
                result.setMsg("删除用户异常"+e.getLocalizedMessage());
                e.printStackTrace();
            }
        }
        return  result;
    }

    /**
     * 用户添加
     * save/del/update/find/query
     */
    @RequestMapping("updatePassword")
    @ResponseBody
    public ResultMap updatePassword(String password, String epassword) {
        ResultMap result = new ResultMap();
        try {
            TsUser tsUser = new TsUser();
            tsUser.setUserId(getTsUser().getUserId());
            tsUser.setPassword(password);//默认密码
            tsUser.setModifyerId(getTsUser().getUserId());
            tsUser.setModifyer(getTsUser().getUserName());
            tsUser.setModifyTime(new Date());
            int save = tsUserService.updateTsUser(tsUser);
            if (save > 0) {
                result.setCode(SystemMsgCode.TS_USER_PASSWORD_SUCCESS.getResultCode());
                result.setMsg(SystemMsgCode.TS_USER_PASSWORD_SUCCESS.getResultMessage());
            } else {
                result.setCode(SystemMsgCode.TS_USER_PASSWORD_FAILED.getResultCode());
                result.setMsg(SystemMsgCode.TS_USER_PASSWORD_FAILED.getResultMessage());
            }
        } catch (Exception e) {
            result.setCode(SystemMsgCode.TS_USER_PASSWORD_FAILED.getResultCode());
            result.setMsg(SystemMsgCode.TS_USER_PASSWORD_FAILED.getResultMessage());
            e.printStackTrace();
        }
        return result;
    }


}
