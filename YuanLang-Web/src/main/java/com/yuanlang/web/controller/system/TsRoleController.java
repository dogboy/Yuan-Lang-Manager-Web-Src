package com.yuanlang.web.controller.system;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.yuanlang.common.ResultMap;
import com.yuanlang.common.util.SystemConsts;
import com.yuanlang.common.util.SystemMsgCode;
import com.yuanlang.facade.model.system.TsDept;
import com.yuanlang.facade.model.system.TsRole;
import com.yuanlang.facade.model.system.TsUser;
import com.yuanlang.facade.model.system.TsUserRole;
import com.yuanlang.services.system.TsRoleService;
import com.yuanlang.services.system.TsUserRoleService;
import com.yuanlang.web.controller.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Calendar;
import java.util.List;

/**
 * company 重庆源狼软件科技有限责任公司
 * FileName TsRoleController
 * Package com.soft.link.controller.tsystem
 * Description 角色控制层
 * author hhq
 * create 2018-04-25 8:57
 * version V1.0
 */
@Controller
@RequestMapping("system/tsRole")
public class TsRoleController extends BaseController {


    @Autowired(required = false)
    private TsRoleService tsRoleService;
    @Autowired(required = false)
    private TsUserRoleService tsUserRoleService;



    /**
     * 分页查询系统角色
     *
     * @param tsRole
     * @return
     */
    @RequestMapping("/findTsRolePage")
    @ResponseBody
    public ResultMap findTsRolePage(TsRole tsRole){

        ResultMap resultMap = new ResultMap();
        try {
            PageHelper.startPage(tsRole.getPage(), tsRole.getLimit());
            TsDept loginDept = getTsDept();
            List<TsRole> list = tsRoleService.findTsRoleList(tsRole,loginDept.getDeptCode());
            PageInfo<TsRole> pageInfo = new PageInfo<TsRole>(list);
            resultMap.setCount(pageInfo.getTotal());
            resultMap.setData(list);
        }catch (Exception e){
            e.printStackTrace();
            resultMap.setCode(SystemMsgCode.TS_ROLE_OPER_FAILED.getResultCode());
            resultMap.setMsg(SystemMsgCode.TS_ROLE_OPER_FAILED.getResultMessage() + e.getLocalizedMessage());
        }

        return resultMap;
    }


    /**
     * 根据角色ID查询角色信息
     * @param roleId
     * @return
     */
    @RequestMapping("/findTsRoleById")
    @ResponseBody
    public ResultMap findTsRoleById(Long roleId){

        ResultMap resultMap = new ResultMap();
        if(roleId == null){
            resultMap.setCode(SystemMsgCode.TS_ROLE_ID_CAN_NOT_BE_NULL.getResultCode());
            resultMap.setMsg(SystemMsgCode.TS_ROLE_ID_CAN_NOT_BE_NULL.getResultMessage());
        }else {
            try {
                TsRole role = tsRoleService.findTsRoleById(roleId);
                resultMap.setData(role);
                resultMap.setCode(SystemMsgCode.TS_ROLE_OPER_SUCCESS.getResultCode());
                resultMap.setMsg(SystemMsgCode.TS_ROLE_OPER_SUCCESS.getResultMessage());
            } catch (Exception e) {
                e.printStackTrace();
                resultMap.setCode(SystemMsgCode.TS_ROLE_OPER_FAILED.getResultCode());
                resultMap.setMsg(SystemMsgCode.TS_ROLE_OPER_FAILED.getResultMessage() + e.getLocalizedMessage());
            }
        }

        return resultMap;
    }


    /**
     * 新增角色
     *
     * @param tsRole
     * @return
     */
    @RequestMapping("/add")
    @ResponseBody
    public ResultMap add(TsRole tsRole) {
        ResultMap resultMap = checkNull(tsRole);
        try {
            if (resultMap.getCode() != SystemMsgCode.TS_ROLE_OPER_SUCCESS.getResultCode())
                return resultMap;
            TsUser tsUser = getTsUser();
            tsRole.setDeptId(tsUser.getDeptId());
            tsRole.setDeptName(tsUser.getDeptName());
            tsRole.setCreatorId(tsUser.getUserId());
            tsRole.setCreator(tsUser.getUserName());
            tsRole.setCreateTime(Calendar.getInstance().getTime());
            tsRole.setModifyerId(tsUser.getUserId());
            tsRole.setModifyer(tsUser.getUserName());
            tsRole.setModifyTime(Calendar.getInstance().getTime());

            tsRoleService.addOrEditTsRole(tsRole);
            resultMap.setMsg(SystemMsgCode.TS_ROLE_OPER_SUCCESS.getResultMessage());
        } catch (Exception e) {
            e.printStackTrace();
            resultMap.setCode(SystemMsgCode.TS_ROLE_OPER_FAILED.getResultCode());
            resultMap.setMsg(SystemMsgCode.TS_ROLE_OPER_FAILED.getResultMessage() + e.getLocalizedMessage());
        }

        return resultMap;
    }


    /**
     * 编辑角色
     *
     * @param tsRole
     * @return
     */
    @RequestMapping("/edit")
    @ResponseBody
    public ResultMap edit(TsRole tsRole) {
        ResultMap resultMap = checkNull(tsRole);
        try {
            if (resultMap.getCode() != SystemMsgCode.TS_ROLE_OPER_SUCCESS.getResultCode())
                return resultMap;
            TsUser tsUser = getTsUser();
            tsRole.setModifyerId(tsUser.getUserId());
            tsRole.setModifyer(tsUser.getUserName());
            tsRole.setModifyTime(Calendar.getInstance().getTime());

            tsRoleService.addOrEditTsRole(tsRole);
            resultMap.setMsg(SystemMsgCode.TS_ROLE_OPER_SUCCESS.getResultMessage());
        } catch (Exception e) {
            e.printStackTrace();
            resultMap.setCode(SystemMsgCode.TS_ROLE_OPER_FAILED.getResultCode());
            resultMap.setMsg(SystemMsgCode.TS_ROLE_OPER_FAILED.getResultMessage() + e.getLocalizedMessage());
        }

        return resultMap;
    }


    /**
     * 删除角色
     *
     * @param tsRoleId
     * @return
     */
    @RequestMapping("/delete")
    @ResponseBody
    public ResultMap delete(Long tsRoleId) {

        ResultMap resultMap = new ResultMap();
        if (tsRoleId == null) {
            resultMap.setCode(SystemMsgCode.TS_ROLE_ID_CAN_NOT_BE_NULL.getResultCode());
            resultMap.setMsg(SystemMsgCode.TS_ROLE_ID_CAN_NOT_BE_NULL.getResultMessage());
        } else {
            try {
            //检查是否有用户还在使用此角色
            List<TsUserRole> userRoleList = tsUserRoleService.findTsUserRoleListByRoleId(tsRoleId);
            if (userRoleList != null && userRoleList.size() > 0) {
                resultMap.setCode(SystemMsgCode.TS_ROLE_HAVE_USER_BE_USEED.getResultCode());
                resultMap.setMsg(SystemMsgCode.TS_ROLE_HAVE_USER_BE_USEED.getResultMessage());
                return resultMap;
            }
            TsRole role = new TsRole();
                role.setRoleId(tsRoleId);
                role.setStatus(SystemConsts.SYS_STATUS_DISABLED);
            tsRoleService.addOrEditTsRole(role);
            resultMap.setCode(SystemMsgCode.TS_ROLE_OPER_SUCCESS.getResultCode());
            resultMap.setMsg(SystemMsgCode.TS_ROLE_OPER_SUCCESS.getResultMessage());
            } catch (Exception e) {
                e.printStackTrace();
                resultMap.setCode(SystemMsgCode.TS_ROLE_OPER_FAILED.getResultCode());
                resultMap.setMsg(SystemMsgCode.TS_ROLE_OPER_FAILED.getResultMessage() + e.getLocalizedMessage());
            }
        }

        return resultMap;
    }


    /**
     * 校验角色参数
     *
     * @param tsRole
     * @return
     */
    private ResultMap checkNull(TsRole tsRole) {
        ResultMap resultMap = new ResultMap();
        resultMap.setCode(SystemMsgCode.TS_ROLE_OPER_SUCCESS.getResultCode());
        if (StringUtils.isEmpty(tsRole.getRoleName())) {
            resultMap.setCode(SystemMsgCode.TS_ROLE_NAME_CAN_NOT_BE_NOT.getResultCode());
            resultMap.setCode(SystemMsgCode.TS_ROLE_NAME_CAN_NOT_BE_NOT.getResultCode());
        }

        if (StringUtils.isEmpty(tsRole.getRoleCode())) {
            resultMap.setCode(SystemMsgCode.TS_ROLE_CODE_CAN_NOT_BE_NOT.getResultCode());
            resultMap.setCode(SystemMsgCode.TS_ROLE_CODE_CAN_NOT_BE_NOT.getResultCode());
        }

        return resultMap;
    }

}
