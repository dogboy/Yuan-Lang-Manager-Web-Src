package com.yuanlang.web.controller.app;

import com.yuanlang.common.ResultMap;
import com.yuanlang.common.util.SystemMsgCode;
import com.yuanlang.facade.model.system.TsApp;
import com.yuanlang.services.system.TsAppService;
import com.yuanlang.web.controller.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.math.BigDecimal;

/**
 * company 重庆源狼软件科技有限责任公司
 * FileName TsAppController
 * Package com.yuanlang.web.controller.system
 * Description App升级管理
 * author Administrator
 * create 2018-05-31 14:50
 * version V1.0
 */
@Controller
@RequestMapping("app/tsApp")
@CrossOrigin(origins = "*", maxAge = 3600)
public class AppTsAppController extends BaseController{
    @Autowired(required = false)
    TsAppService appService ;
    /**
     * @param  version  版本号
     * @param  sign 唯一标识，不是签名 , APP名称
     * 用于app 更新
     */
    @RequestMapping(value = {"findLastTsApp"},method = {RequestMethod.POST,RequestMethod.GET}, produces = "application/json")
    @ResponseBody
    public ResultMap findLastTsApp(BigDecimal version, String sign){
        ResultMap  result = new ResultMap();
        try {
            if(sign!=null){
                TsApp lastTsApp  = appService.findLastTsApp(sign);
                result.setCode(SystemMsgCode.TS_APP_FIND_SUCCESS.getResultCode());
                result.setMsg(SystemMsgCode.getSystemMsg(SystemMsgCode.TS_APP_FIND_SUCCESS.getResultCode()));
                result.setData(lastTsApp);
            }
        } catch (Exception e) {
            result.setCode(SystemMsgCode.TS_APP_FIND_FAILED.getResultCode());
            result.setMsg(SystemMsgCode.getSystemMsg(SystemMsgCode.TS_APP_FIND_FAILED.getResultCode()));
            e.printStackTrace();
        }
        return result ;
    }
}
