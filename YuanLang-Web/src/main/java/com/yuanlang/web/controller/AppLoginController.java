package com.yuanlang.web.controller;

import com.alibaba.fastjson.JSON;
import com.yuanlang.common.ResultMap;
import com.yuanlang.common.util.AccessToken;
import com.yuanlang.common.util.SystemMsgCode;
import com.yuanlang.common.util.UniqueUtil;
import com.yuanlang.facade.model.system.TsUser;
import com.yuanlang.services.redis.RedisClientTemplate;
import com.yuanlang.services.system.TsUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * company 重庆源狼软件科技有限责任公司
 * FileName AppLoginController
 * Package com.soft.link.controller
 * Description 移动终端登录都需要添加 app 拦截
 * author Administrator
 * create 2018-03-05 20:03
 * version V1.0
 */
@Controller
@RequestMapping(value = "/login")
@CrossOrigin(origins = "*", maxAge = 3600) // 设置跨域访问
public class AppLoginController extends BaseController {
    @Autowired(required = false)
    RedisClientTemplate redisClientTemplate;
    @Autowired(required = false)
    private TsUserService tsUserService;
    @Value("${sessionTimeOut}")
    private int sessionTimeOut;
    /**
     * @param user
     * @return
     */
    @RequestMapping(value = "app/doLogin", method = {RequestMethod.POST}, produces = "application/json")
    @ResponseBody
    public ResultMap appDoLogin(@RequestBody TsUser user) {
        ResultMap result = new ResultMap();
        try {
            List<TsUser> tsUserList = tsUserService.findTsUser(user.getUserName());
            String app_token = UniqueUtil.uuid(); // 登录的时候，生成uuid进行控制 ;
            if (tsUserList != null && tsUserList.size() > 0) {
                // 存在账号
                for (TsUser tsUser : tsUserList) {
                    if (tsUser.getIslock() == 0) {
                        if (user.getPassword().equals(tsUser.getPassword())) {
                            result.setCode(0);
                            result.setMsg("登录成功");
                            AccessToken accessToken = new AccessToken();
                            accessToken.setAccess_token(app_token);
                            result.setData(accessToken);
                            redisClientTemplate.set(app_token, JSON.toJSONString(tsUser), "NX", "EX", sessionTimeOut);
                        } else {
                            result.setCode(SystemMsgCode.LOGIN_FAILED_ERR_PWD.getResultCode());
                            result.setMsg(SystemMsgCode.getSystemMsg(SystemMsgCode.LOGIN_FAILED_ERR_PWD.getResultCode()));
                        }
                    } else {
                        result.setCode(SystemMsgCode.LOGIN_FAILED_ERR_PWD.getResultCode());
                        result.setMsg(SystemMsgCode.getSystemMsg(SystemMsgCode.LOGIN_FAILED_ERR_PWD.getResultCode()));
                    }
                }
            } else {
                result.setCode(SystemMsgCode.LOGIN_ACCT_NOT_EXISTED.getResultCode());
                // 账号不存在
                result.setMsg(SystemMsgCode.getSystemMsg(SystemMsgCode.LOGIN_ACCT_NOT_EXISTED.getResultCode()));
            }
        } catch (Exception e) {
            result.setCode(SystemMsgCode.LOGIN_FAILED_ERR_PWD.getResultCode());
            // 系统异常
            result.setMsg(SystemMsgCode.getSystemMsg(SystemMsgCode.SYS_EXCEPTION.getResultCode()) + ":" + e.getLocalizedMessage());
        }
        logger.info(JSON.toJSONString(user));
        return result;
    }

}
