package com.yuanlang.web.controller.system;

import com.yuanlang.common.ResultMap;
import com.yuanlang.common.util.SystemConsts;
import com.yuanlang.common.util.SystemMsgCode;
import com.yuanlang.facade.model.system.TreeModel;
import com.yuanlang.facade.model.system.TsMenu;
import com.yuanlang.facade.model.system.TsRoleMenu;
import com.yuanlang.facade.model.system.TsUser;
import com.yuanlang.services.system.TsMenuService;
import com.yuanlang.services.system.TsRoleMenuService;
import com.yuanlang.web.controller.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

/**
 * company 重庆源狼软件科技有限责任公司
 * FileName TsRoleMenuController
 * Package com.soft.link.controller.tsystem
 * Description 菜单角色按钮
 * author Administrator
 * create 2018-04-27 11:19
 * version V1.0
 */
@Controller
@RequestMapping("system/tsRoleMenu")
public class TsRoleMenuController extends BaseController {
    @Autowired
    TsRoleMenuService tsRoleMenuService;
    @Autowired
    TsMenuService tsMenuService;

    /**
     * 根据角色ID查询角色菜单树
     *
     * @param roleId
     * @return
     */
    @RequestMapping("/findRoleMenuTreeByRoleId")
    @ResponseBody
    public List<TreeModel> findRoleMenuTreeByRoleId(Long roleId) {
        if (roleId == null) return null;
        // 查询所有菜单
        try {
            TsUser tsUser = getTsUser();
            List<TsMenu> tsMenuList = tsMenuService.findByOrders(tsUser);
            List<TsRoleMenu> checkedList = tsRoleMenuService.findRoleMenuBuRoleId(roleId);
            List<TreeModel> tree = getTreeModel(tsMenuList, checkedList);
            return tree;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }


    /**
     * 设置角色菜单
     *
     * @param roleId
     * @return
     */
    @RequestMapping("/add")
    @ResponseBody
    public ResultMap add(Long roleId, String menuIdList) {

        ResultMap resultMap = new ResultMap();
        try {
            // 根据角色ID清除之前的记录
            tsRoleMenuService.deleteByRoleId(roleId);
            TsRoleMenu tsRoleMenu = null;
            List<TsRoleMenu> tsRoleMenuList = new ArrayList<TsRoleMenu>();
            TsUser tsUser = getTsUser();
            for (String menuId : menuIdList.split(",")) {
                tsRoleMenu = new TsRoleMenu();

                tsRoleMenu.setMenuId(Long.parseLong(menuId));
                tsRoleMenu.setRoleId(roleId);
                tsRoleMenu.setStatus(SystemConsts.SYS_STATUS_ENABLED);

                tsRoleMenu.setCreatorId(tsUser.getUserId());
                tsRoleMenu.setCreator(tsUser.getUserName());
                tsRoleMenu.setCreateTime(Calendar.getInstance().getTime());
                tsRoleMenu.setModifyerId(tsUser.getUserId());
                tsRoleMenu.setModifyer(tsUser.getUserName());
                tsRoleMenu.setModifyTime(Calendar.getInstance().getTime());

                tsRoleMenuList.add(tsRoleMenu);
            }
            tsRoleMenuService.saveRoleMenuList(tsRoleMenuList);
            resultMap.setCode(SystemMsgCode.TS_ROLE_OPER_SUCCESS.getResultCode());
            resultMap.setMsg(SystemMsgCode.TS_ROLE_OPER_SUCCESS.getResultMessage());
        } catch (Exception e) {
            e.printStackTrace();
            resultMap.setCode(SystemMsgCode.TS_ROLE_OPER_FAILED.getResultCode());
            resultMap.setMsg(SystemMsgCode.TS_ROLE_OPER_FAILED.getResultMessage() + e.getLocalizedMessage());
        }

        return resultMap;
    }


    private List<TreeModel> getTreeModel(List<TsMenu> tsMenuList, List<TsRoleMenu> checkedList) {
        List<TreeModel> list = new ArrayList<TreeModel>();
        List<Long> checkedMenuIds = new ArrayList<Long>();// 存放角色已配置的菜单ID
        for (TsRoleMenu tsRoleMenu : checkedList) {
            checkedMenuIds.add(tsRoleMenu.getMenuId());
        }
        HashMap<Long, TreeModel> map = new HashMap<Long, TreeModel>();
        for (TsMenu tsMenu : tsMenuList) {
            TreeModel treeModel = new TreeModel();
            treeModel.setId(tsMenu.getMenuId());
            treeModel.setName(tsMenu.getMenuName());
            if (checkedMenuIds.contains(tsMenu.getMenuId())) {
                treeModel.setChecked(true);
            }
            treeModel.setChildren(new ArrayList<TreeModel>());
            map.put(tsMenu.getMenuId(), treeModel);
        }
        for (TsMenu tsMenu : tsMenuList) {// 封装子节点到父节点
            if (map.containsKey(tsMenu.getParentMenuId())) {
                map.get(tsMenu.getParentMenuId()).getChildren().add(map.get(tsMenu.getMenuId()));
                map.remove(tsMenu.getMenuId());
            }
        }

        for (Long key : map.keySet()) {
            list.add(map.get(key));
        }

        return list;
    }


}
