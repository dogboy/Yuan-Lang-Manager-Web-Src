package com.yuanlang.web.controller;
import com.yuanlang.facade.model.system.TsMenu;
import com.yuanlang.facade.model.system.TsUser;
import com.yuanlang.services.system.TsMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * company 重庆源狼软件科技有限责任公司
 * FileName HomePage
 * Package com.soft.link.controller
 * Description 系统主页
 * author Administrator
 * create 2018-03-22 23:24
 * version V1.0
 */
@Controller
@RequestMapping("system/homepage")
public class IndexController extends BaseController {

    @Autowired
    HttpServletRequest request ;
    @Autowired
    TsMenuService tsMenuService ;
    /**
    * @methodname 进入主页
    * @Description 登录成功后进入主页
    * @author 黄学乾
    * @create 2018/3/22/022 23:26
    * @param  * @param 
    * @throws {如果有异常说明请填写}
    * @return 
    */
    @RequestMapping("/homePage")
    public ModelAndView homePage(){
        ModelAndView homePageMv = new ModelAndView("/homepage/homePage");
        try {
            TsUser tsUser = getTsUser();
            try {
                List<TsMenu> menuList = tsMenuService.findTsMenuList(tsUser);
                homePageMv.addObject("menuList",menuList);
                homePageMv.addObject("tsUser", tsUser);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return homePageMv ;
    }
}
