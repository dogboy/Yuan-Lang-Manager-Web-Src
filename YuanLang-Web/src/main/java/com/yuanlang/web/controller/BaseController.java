package com.yuanlang.web.controller;

import com.alibaba.fastjson.JSON;
import com.yuanlang.common.util.AccessToken;
import com.yuanlang.facade.model.system.TsDept;
import com.yuanlang.facade.model.system.TsUser;
import com.yuanlang.services.redis.RedisClientTemplate;
import com.yuanlang.services.system.TsDeptService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpServletRequest;

/**
 * company 重庆源狼软件科技有限责任公司
 * FileName BaseController
 * Package com.soft.link.controller
 * Description
 * author Administrator
 * create 2018-04-25 16:36
 * version V1.0
 */

public class BaseController {
    //不能用private：因为私有变量子类无法访问
    public Logger logger = LoggerFactory.getLogger(this.getClass());
    @Autowired(required = false)
    RedisClientTemplate redisClientTemplate; // 注入redis
    @Autowired(required = false)
    private HttpServletRequest request;
    @Autowired(required = false)
    private TsDeptService tsDeptService;

    /**
     * @param
     * @throws {获取失败}
     * @methodname getTsUser
     * @Description {登录之后获取登录人员方法 }
     * @author 黄学乾
     * @create 2018/4/25/025 17:04
     */
    protected TsUser getTsUser() {
        logger.info("BaseController-->getTsUser");
        TsUser tsUser = null;
        try {
            String tsUserStr = redisClientTemplate.get(request.getSession().getId());
            tsUser = JSON.parseObject(tsUserStr, TsUser.class);
        } catch (Exception e) {
            logger.info("BaseController-->getTsUser");
            e.printStackTrace();
        }
        return tsUser;
    }


    /**
     * 获取登录用户的ID
     * @return
     */
    protected Long getUserId(){
        TsUser tsUser = null;
        try {
            String tsUserStr = redisClientTemplate.get(request.getSession().getId());
            tsUser = JSON.parseObject(tsUserStr, TsUser.class);
        } catch (Exception e) {
            logger.info("BaseController-->getTsUser");
            e.printStackTrace();
        }
        return tsUser.getUserId();
    }

    /**
     * 判断是否为管理员
     * @return
     */
    protected Boolean isAdmin(){
        TsUser tsUser = null;
        try {
            String tsUserStr = redisClientTemplate.get(request.getSession().getId());
            tsUser = JSON.parseObject(tsUserStr, TsUser.class);
        }catch (Exception e){
            logger.info("BaseController-->isAdmin");
            e.printStackTrace();
        }
        return tsUser.getAdmin();
    }

    /**
     * 获取登录人部门信息
     * @return
     */
    protected TsDept getTsDept(){
        TsUser tsUser = null;
        try {
            String tsUserStr = redisClientTemplate.get(request.getSession().getId());
            tsUser = JSON.parseObject(tsUserStr, TsUser.class);
            return tsDeptService.findTsDept(tsUser.getDeptId());
        }catch (Exception e){
            logger.info("BaseController-->isAdmin");
            e.printStackTrace();
        }

        return null;
    }

}
