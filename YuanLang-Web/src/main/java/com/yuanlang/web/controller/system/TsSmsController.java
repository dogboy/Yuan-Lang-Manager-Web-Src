package com.yuanlang.web.controller.system;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
/**
 * company 重庆源狼软件科技有限责任公司
 * FileName TsSmsController
 * Package com.yuanlang.web.controller.system
 * Description 短信短信发送工具；
 * author Administrator
 * create 2018-06-13 16:14
 * version V1.0
 */

@Controller
@RequestMapping("system/tsSms")
public class TsSmsController {

    private  static String  defaultConnectTimeout = "sun.net.client.defaultConnectTimeout";
    private  static String  defaultReadTimeout = "sun.net.client.defaultConnectTimeout";
    private  static String encode = "UTF-8";
    private  static String username = "";
    private  static String password_md5 = "";
    private  static String apikey = "";

    public static void main(String[] args) {
        //连接超时及读取超时设置
        System.setProperty(defaultConnectTimeout, "30000"); //连接超时：30秒
        System.setProperty(defaultReadTimeout, "30000");    //读取超时：30秒
        //新建一个StringBuffer链接
        StringBuffer buffer = new StringBuffer();
        // String encode = "GBK"; //页面编码和短信内容编码为GBK。重要说明：如提交短信后收到乱码，请将GBK改为UTF-8测试。如本程序页面为编码格式为：ASCII/GB2312/GBK则该处为GBK。如本页面编码为UTF-8或需要支持繁体，阿拉伯文等Unicode，请将此处写为：UTF-8
        // String encode = "UTF-8";
        // String username = "";  //用户名
        // String password_md5 = "";  //密码
        String mobile = "";  //手机号,只发一个号码：13800000001。发多个号码：13800000001,13800000002,...N 。使用半角逗号分隔。
        // String apikey = "";  //apikey秘钥（请登录 http://m.5c.com.cn 短信平台-->账号管理-->我的信息 中复制apikey）
        String content = "您好，您的验证码是：12345【美联】";  // 要发送的短信内容，特别注意：签名必须设置，网页验证码应用需要加添加【图形识别码】。
        try {
            String contentUrlEncode = URLEncoder.encode(content,encode);
            // 对短信内容做Urlencode编码操作。注意：如
            // 把发送链接存入buffer中，
            // 如连接超时，可能是您服务器不支持域名解析，
            // 请将下面连接中的：【m.5c.com.cn】修改为IP：【115.28.23.78】
            buffer.append("http://m.5c.com.cn/api/send/index.php?username="+username+"&password_md5="+password_md5+"&mobile="+mobile+"&apikey="+apikey+"&content="+contentUrlEncode+"&encode="+encode);
            //System.out.println(buffer); //调试功能，输入完整的请求URL地址
            URL url = new URL(buffer.toString());   //把buffer链接存入新建的URL中
            HttpURLConnection connection = (HttpURLConnection)url.openConnection();     //打开URL链接
            connection.setRequestMethod("POST");     //使用POST方式发送
            connection.setRequestProperty("Connection", "Keep-Alive");    //使用长链接方式
            BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));    //发送短信内容
            String result = reader.readLine();//获取返回值
            System.out.println(result); //输出result内容，查看返回值，成功为success，错误为error，详见该文档起始注释
            // 将返回值转换成JSON
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            // 五分钟之内只发送一次验证码
        }
    }
}
