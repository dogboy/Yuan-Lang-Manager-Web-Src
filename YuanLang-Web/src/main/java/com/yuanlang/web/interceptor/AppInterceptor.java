package com.yuanlang.web.interceptor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

/**
 * company 重庆源狼软件科技有限责任公司
 * FileName FacadeInterceptor
 * Package com.yuanlang.web.interceptor
 * Description 移动终端，拦截器
 * author huangxueqian
 * create 2018-05-13 14:52
 * version V1.0
 */
public class AppInterceptor extends HandlerInterceptorAdapter {
    private static Logger logger = LoggerFactory.getLogger(AppInterceptor.class);
    /**
     * 在业务处理器处理请求之前被调用
     * 如果返回false  从当前的拦截器往回执行所有拦截器的afterCompletion(),再退出拦截器链
     * 如果返回true  执行下一个拦截器,直到所有的拦截器都执行完毕,再执行被拦截的Controller ,然后进入拦截器链,
     * 从最后一个拦截器往回执行所有的postHandle() ,接着再从最后一个拦截器往回执行所有的afterCompletion()
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {
        logger.info(" AppInterceptor ==============执行顺序: 1、preHandle================");
/*        String contextPath = request.getContextPath();
        HandlerMethod method =(HandlerMethod) handler;
        UserCheck auth = method.getMethodAnnotation( UserCheck.class );
        String url = request.getRequestURI();
        //aes解密移动终端传递过来的参数
        AES aes = new AES(AES.AES_KEY);
        String params = request.getParameter("params");
        if( StringUtils.isNotBlank(params) ){
            System.out.println("----" + aes.decrypt(params.replace("%2B","+")));
            request.setAttribute("paramObj",aes.decrypt(params.replace("%2B","+")));
        }
        logger.info("参数："+ JSONObject.toJSONString(request.getParameterMap()));
        logger.info("headers："+ JSONObject.toJSONString(getHeadersInfo(request)));
        if( auth == null ) {
            logger.info( "移动终端接口，无需验证token。接口地址：" + url );
            return true;
        } else {
            logger.info( "移动终端接口，需要验证token。接口地址：" + url );
            //从header里取到token
            String token = URLDecoder.decode( request.getHeader("token"),"utf-8");
            //String token = "HTOTzH4KC6O%2BisekM/XNKKJNEhhuZY7eqVSfi4lEh7EZHlzwpnd8vPVLMCMaKdHi0Gv6JfNnhT7pbqG1sg0SBw==";
            String userId = userService.checkUser(token);
            //token验证不通过时，登录过期
            if(StringUtils.isBlank(userId)){
                response.sendRedirect(contextPath + TIMEOUT);
                return false;
            }else{
                //token验证通过时放入requset里
                request.setAttribute("userId",userId);
                return true;
            }
        }*/
        return true;
    }

    /**
     * 在业务处理器处理请求执行完成后,生成视图之前执行的动作
     * 可在modelAndView中加入数据，比如当前时间
     */
    @Override
    public void postHandle(
            HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView)
            throws Exception {
        logger.info("==============执行顺序: 2、postHandle================");
    }

    /**
     * 在DispatcherServlet完全处理完请求后被调用,可用于清理资源等
     * 当有拦截器抛出异常时,会从当前拦截器往回执行所有的拦截器的afterCompletion()
     */
    @Override
    public void afterCompletion(
            HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
            throws Exception {
        logger.info("==============执行顺序: 3、afterCompletion================");
    }

    private Map<String, String> getHeadersInfo(HttpServletRequest request) {
        Map<String, String> map = new HashMap<String, String>();
        Enumeration headerNames = request.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            String key = (String) headerNames.nextElement();
            String value = request.getHeader(key);
            map.put(key, value);
        }
        return map;
    }
}
