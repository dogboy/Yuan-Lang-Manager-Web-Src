package com.yuanlang.web.interceptor;

import com.alibaba.fastjson.JSON;
import com.yuanlang.common.ResultMap;
import com.yuanlang.common.util.SystemMsgCode;
import com.yuanlang.services.redis.RedisClientTemplate;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;

/**
 * company 重庆源狼软件科技有限责任公司
 * FileName CommonInterceptor
 * Package com.yuanlang.web.interceptor
 * Description 公共拦截器
 * author Administrator
 * create 2018-05-13 14:52
 * version V1.0
 */
public class CommonInterceptor extends HandlerInterceptorAdapter {

    private static Logger logger = LoggerFactory.getLogger(CommonInterceptor.class);

    @Autowired
    private RedisClientTemplate redisClientTemplate;

    @Value("${sessionTimeOut}")
    private int sessionTimeOut;

    /**
     * 在业务处理器处理请求之前被调用
     * 如果返回false  从当前的拦截器往回执行所有拦截器的afterCompletion(),再退出拦截器链
     * 如果返回true  执行下一个拦截器,直到所有的拦截器都执行完毕,再执行被拦截的Controller ,然后进入拦截器链,
     * 从最后一个拦截器往回执行所有的postHandle() ,接着再从最后一个拦截器往回执行所有的afterCompletion()
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {
        logger.info("==============执行顺序:CommonInterceptor 1、preHandle================");
        String access_token = request.getParameter("access_token");
        logger.info("==============执行顺序:CommonInterceptor 1、access_token================" + access_token);
        if (StringUtils.isEmpty(access_token)) {
            InterceptorFalse(response);
            return false;
        } else {
            if (redisClientTemplate.exists(access_token)) {
                // 更新操作时间
                redisClientTemplate.set(access_token, redisClientTemplate.get(access_token), "XX", "EX", sessionTimeOut);
                // 在范围内
                return true;
            } else {
                // redis 不存在 则过期;
                InterceptorFalse(response);
                return false;
            }
        }

    }

    private void InterceptorFalse(HttpServletResponse response) {
        ResultMap result = new ResultMap();
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json; charset=utf-8");
        PrintWriter out = null;
        try {
            result.setCode(SystemMsgCode.SYS_INTERCEPTOR.getResultCode());
            result.setMsg(SystemMsgCode.getSystemMsg(SystemMsgCode.SYS_INTERCEPTOR.getResultCode()));
            out = response.getWriter();
            out.append(JSON.toJSONString(result));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 在业务处理器处理请求执行完成后,生成视图之前执行的动作
     * 可在modelAndView中加入数据，比如当前时间
     */
    @Override
    public void postHandle(
            HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView)
            throws Exception {
        logger.info("==============执行顺序:CommonInterceptor 2、postHandle================");
    }

    /**
     * 在DispatcherServlet完全处理完请求后被调用,可用于清理资源等
     * 当有拦截器抛出异常时,会从当前拦截器往回执行所有的拦截器的afterCompletion()
     */
    @Override
    public void afterCompletion(
            HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
            throws Exception {
        logger.info("==============执行顺序:CommonInterceptor 3、afterCompletion================");
    }

}
