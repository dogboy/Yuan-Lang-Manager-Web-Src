/**
 * Created by 黄学乾 on 2018/5/17/017.
 * 源狼软件科技有限责任公司， 前端公用方法公开
 */
layui.define(['jquery'], function(exports){
    var $ = layui.jquery;
    var obj = {
        /**
         * 时间戳转换日期
         * @param <int> unixTime    待时间戳(秒)
         * @param <bool> isFull    返回完整时间(Y-m-d 或者 Y-m-d H:i:s)
         * @param <int>  timeZone   时区
         */
        UnixToDate: function (unixTime, isFull, timeZone){
        if (typeof (timeZone) == 'number')
        {
            unixTime = parseInt(unixTime) + parseInt(timeZone) * 60 * 60;
        }
        var time = new Date(unixTime * 1000);
        var ymdhis = "";
        ymdhis += fixZero(time.getUTCFullYear()) + "-";
        ymdhis += fixZero((time.getUTCMonth()+1)) + "-";
        ymdhis += fixZero(time.getUTCDate());
        if (isFull === true)
        {
            ymdhis += " " + fixZero(time.getUTCHours()) + ":";
            ymdhis += fixZero(time.getUTCMinutes()) + ":";
            ymdhis += fixZero(time.getUTCSeconds());
        }
        return ymdhis;
    }
    };

    function fixZero(num){
        var str=""+num;
        var len=str.length;     var s="";
        for(var i=2;i-->len;){
            s+="0";
        }
        return s+str;
    }
    //输出接口
    exports('format', obj);
});