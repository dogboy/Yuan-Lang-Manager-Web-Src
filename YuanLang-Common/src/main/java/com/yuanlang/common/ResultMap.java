package com.yuanlang.common;

/**
 * company 重庆源狼软件科技有限责任公司
 * FileName LayerResult
 * Package com.yuanlang.common
 * Description LayerAdmin定义公共的返回接口
 * author Administrator
 * create 2018-05-15 15:00
 * version V1.0
 */

public class ResultMap {
    // 返回状态码
    private Integer code = 0 ;
    // 返回消息
    private String msg;
    //返回数据条数
    private long count = 0l ;
    //返回数据
    private Object data;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }
}
