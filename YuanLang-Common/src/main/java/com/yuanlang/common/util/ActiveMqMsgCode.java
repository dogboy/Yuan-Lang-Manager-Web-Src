package com.yuanlang.common.util;

/**
 * MQ消息类型定义
 */
public enum ActiveMqMsgCode {

    SYS_UPGRADE(1000000, "系统升级"),
    BIZ_IM_USER(1000001, "同步好友"),
    BIZ_IM_GROUP(1000002, "同步群组"),

    REPLACE(0, ""),;
    private Integer resultCode;
    private String resultMessage;
    private String[] params = null;

    private ActiveMqMsgCode(Integer resultCode, String resultMessage) {
        this.resultCode = resultCode;
        this.resultMessage = resultMessage;
    }

    public static String getSystemMsg(Integer resultCode) {
        for (ActiveMqMsgCode systemMsgCode : ActiveMqMsgCode.values()) {
            if (systemMsgCode.getResultCode() == resultCode)
                return systemMsgCode.getResultMessage();
        }
        return "";
    }

    public static ActiveMqMsgCode replaceMsgParams(ActiveMqMsgCode systemMsgCode, String[] params) {
        String msg = systemMsgCode.getResultMessage();
        for (int i = 0; i < params.length; i++)
            msg = msg.replaceAll("\\{" + (i + 1) + "\\}", params[i]);
        REPLACE.setResultCode(systemMsgCode.getResultCode());
        REPLACE.setResultMessage(msg);
        return REPLACE;

    }

    public Integer getResultCode() {
        return resultCode;
    }

    public void setResultCode(Integer resultCode) {
        this.resultCode = resultCode;
    }

    public String getResultMessage() {
        if (params == null)
            return resultMessage;
        else {
            String msg = resultMessage;
            for (int i = 0; i < params.length; i++)
                msg = msg.replaceAll("\\{" + (i + 1) + "\\}", params[i]);
            return msg;
        }
    }

    public void setResultMessage(String resultMessage) {
        this.resultMessage = resultMessage;
    }

    public ActiveMqMsgCode replaceParams(String[] params) {
        this.params = params;
        return this;
    }

}
