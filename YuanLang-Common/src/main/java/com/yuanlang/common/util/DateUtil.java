package com.yuanlang.common.util;

import javax.xml.crypto.Data;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class DateUtil {
	/**
     * 变量：日期格式化类型 - 格式:yyyy/MM/dd
     */
	public static final int DEFAULT = 0;
	public static final int YM = 1;

    /**
     * 变量：日期格式化类型 - 格式:yyyy-MM-dd
     * 
     */
    public static final int YMR_SLASH = 11;

    /**
     * 变量：日期格式化类型 - 格式:yyyyMMdd
     * 
     */
    public static final int NO_SLASH = 2;

    /**
     * 变量：日期格式化类型 - 格式:yyyyMM
     * 
     */
    public static final int YM_NO_SLASH = 3;

    /**
     * 变量：日期格式化类型 - 格式:yyyy/MM/dd HH:mm:ss
     * 
     */
    public static final int DATE_TIME = 4;

    /**
     * 变量：日期格式化类型 - 格式:yyyyMMddHHmmss
     * 
     */
    public static final int DATE_TIME_NO_SLASH = 5;

    /**
     * 变量：日期格式化类型 - 格式:yyyy/MM/dd HH:mm
     * 
     */
    public static final int DATE_HM = 6;

    /**
     * 变量：日期格式化类型 - 格式:HH:mm:ss
     * 
     */
    public static final int TIME = 7;

    /**
     * 变量：日期格式化类型 - 格式:HH:mm
     * 
     */
    public static final int HM = 8;
    
    /**
     * 变量：日期格式化类型 - 格式:HHmmss
     * 
     */
    public static final int LONG_TIME = 9;
    /**
     * 变量：日期格式化类型 - 格式:HHmm
     * 
     */
    
    public static final int SHORT_TIME = 10;

    public static final String YYYY_MM_DD = "yyyy-MM-dd";
    public static final String YYYYMMDD = "yyyy年MM月dd日";
    public static final String MM_DD = "MM-dd";
    public static final String MMDD = "MM月dd日";
    public static final String YYYY_MM = "yyyy-MM";
    public static final String YYYY = "yyyy";

    /**
     * 变量：日期格式化类型 - 格式:yyyy-MM-dd HH:mm:ss
     */
    public static final int DATE_TIME_LINE = 12;

    /**
     * 变量：日期格式化类型 - 格式:yyyyMMddHHmmssSSS
     */
    public static final int DATE_TIME_MILLISECOND = 13;
    public static String nowDateToStr(int type){
        Calendar cal=Calendar.getInstance();
        return dateToStr(cal.getTime(),type);
    }
	public static String dateToStr(Date date, int type) {
        switch (type) {
        case DEFAULT:
            return dateToStr(date);
        case YM:
            return dateToStr(date, "yyyy/MM");
        case NO_SLASH:
            return dateToStr(date, "yyyyMMdd");
        case YMR_SLASH:
        	return dateToStr(date, "yyyy-MM-dd");
        case YM_NO_SLASH:
            return dateToStr(date, "yyyyMM");
        case DATE_TIME:
            return dateToStr(date, "yyyy/MM/dd HH:mm:ss");
        case DATE_TIME_NO_SLASH:
            return dateToStr(date, "yyyyMMddHHmmss");
        case DATE_HM:
            return dateToStr(date, "yyyy/MM/dd HH:mm");
        case TIME:
            return dateToStr(date, "HH:mm:ss");
        case HM:
            return dateToStr(date, "HH:mm");
        case LONG_TIME:
            return dateToStr(date, "HHmmss");
        case SHORT_TIME:
            return dateToStr(date, "HHmm");
        case DATE_TIME_LINE:
            return dateToStr(date, "yyyy-MM-dd HH:mm:ss");
        case DATE_TIME_MILLISECOND:
            return dateToStr(date,"yyyyMMddHHmmssSSS");
        default:
            throw new IllegalArgumentException("Type undefined : " + type);
        }
    }
	public static String dateToStr(Date date,String pattern) {
	       if (date == null || date.equals(""))
	    	 return null;
	       SimpleDateFormat formatter = new SimpleDateFormat(pattern);
	       return formatter.format(date);
    } 

    public static String dateToStr(Date date) {
        return dateToStr(date, YMR_SLASH);
    }

    public static Date strToDate(String strDate){
        try {
        	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Date date = sdf.parse(strDate);
			return date;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
    }

    public static String nowStrDate() {
        return dateToStr(new Date(),DATE_TIME_LINE);
    }

    public static Date strToDate(String strDate, String pattern){
        if (SysUtils.isNull(strDate)) return null;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(pattern);
            Date date = sdf.parse(strDate);
            return date;
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 获取过去（未来）几天的日期
     * @param num 为负代表过去 为正代表将来
     * @param format 格式化日期
     * @return
     */
    public static ArrayList<String> getPastOrFutrueDates(int num,String format){
        ArrayList<String> result = new ArrayList<String>();
        if(num > 0){
            for(int i = 0;i < num;i ++){
                result.add(calDate(i,format));
            }
        }else{
            for(int i = 0;i > num;num ++){
                result.add(calDate(num+1,format));
            }
        }
        return result;
    }

    public static String calDate(int past,String format){
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_YEAR,calendar.get(Calendar.DAY_OF_YEAR) + past);
        Date date = calendar.getTime();
        SimpleDateFormat spf = new SimpleDateFormat(format);
        return spf.format(date);
    }

    /**
     *  获取过去的第几天的日期
     * @param pastNum 过去的天数
     * @param format
     * @return
     */
    public static String getPastDate(int pastNum,String format){
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_YEAR,calendar.get(Calendar.DAY_OF_YEAR) - pastNum + 1);
        Date date = calendar.getTime();
        SimpleDateFormat spf = new SimpleDateFormat(format);
        return spf.format(date);
    }

    /**
     * 凌晨
     * @param date
     * @flag 0 返回yyyy-MM-dd 00:00:00日期<br>
     *       1 返回yyyy-MM-dd 23:59:59日期
     * @day 后移或前移几天
     * @day 后移或前移几年
     * @return
     */
    public static Date weeHours(Date date, int flag,int day,int year) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE,day);
        cal.add(Calendar.YEAR,year);
        if (flag == 0) {
            return DateUtil.strToDate(DateUtil.dateToStr(cal.getTime())+" 00:00:00","yyyy-MM-dd HH:mm:ss");
        } else {
            return DateUtil.strToDate(DateUtil.dateToStr(cal.getTime())+" 23:59:59","yyyy-MM-dd HH:mm:ss");
        }
    }

    public static void main(String[] args) {

        for (int i = 0; i < 1000; i++) {
            System.out.println(DateUtil.dateToStr(DateUtil.weeHours(new Date(),1,-1,1),12));
        }
    }

}
