package com.yuanlang.common.util;

/**
 * Created by lvluogang on 2017/6/28.
 */
public enum ThirdAPIProvider {
    GECKO("gecko","壁虎车险"),
    LIBERTY("liberty","利宝车险"),
    CPIC("cpic","中国太保"),
    PINGAN("pingan","中国平安"),
    EBAOQUAN("ebaoquan","易保全"),
    CIRC_SZ("circ-sz","深圳保监"),
    E_BAO_QUAN_JUNZIQIAN("e-baoquan-junziqian","易保全-君子签");

    private String providerCode;
    private String providerName;

    private ThirdAPIProvider(String providerCode, String providerName) {
        this.providerCode = providerCode;
        this.providerName = providerName;
    }

    public String getProviderCode() {
        return providerCode;
    }

    public String getProviderName() {
        return providerName;
    }
}
