package com.yuanlang.common.util;

/**
 * Created by lvluogang on 16/9/30.
 */
public class MobileClientConfig {
    private String fileServerUrl;

    private String clientAppAllowEnvironment;

    private String serviceCpicNianshenUrl;

    private String operationLoginUrl;

    public String getServiceCpicNianshenUrl() {
        return serviceCpicNianshenUrl;
    }

    public void setServiceCpicNianshenUrl(String serviceCpicNianshenUrl) {
        this.serviceCpicNianshenUrl = serviceCpicNianshenUrl;
    }

    public String getClientAppAllowEnvironment() {
        return clientAppAllowEnvironment;
    }

    public void setClientAppAllowEnvironment(String clientAppAllowEnvironment) {
        this.clientAppAllowEnvironment = clientAppAllowEnvironment;
    }

    public String getFileServerUrl() {
        return fileServerUrl;
    }

    public void setFileServerUrl(String fileServerUrl) {
        this.fileServerUrl = fileServerUrl;
    }

    public String getOperationLoginUrl() {
        return operationLoginUrl;
    }

    public void setOperationLoginUrl(String operationLoginUrl) {
        this.operationLoginUrl = operationLoginUrl;
    }
}
