package com.yuanlang.common.util;

/**
 * 重庆源狼软件科技有限责任公司
 * 状态码标识
 */
public enum SystemMsgCode {

    SYS_SUCCEED(0,"执行成功"),
    SYS_FAILED(-1,"执行失败"),
    SYS_EXCEPTION(2,"系统异常"),
    SYS_INTERCEPTOR(1001,"登录拦截"),

    SYS_ADD_SUCCESS(0,"添加成功"),
    SYS_ADD_FAILED(31,"添加失败"),
    SYS_EDIT_SUCCESS(0,"编辑成功"),
    SYS_EDIT_FAILED(41,"编辑失败"),

    SYS_DEL_SUCCESS(0,"删除成功"),
    SYS_DEL_FAILED(51,"删除失败"),


    SYS_REDIS_FAILED(1001,"Redis服务器，请联系客服！"),

    REPLACE(0,""),
    // 登录操作
    LOGIN_FAILED_ERR_PWD(13000,"用户名或密码错误"),
    LOGIN_ACCT_NOT_EXISTED(13002,"帐号不存在"),
    LOGIN_ACCT_IS_LOCKED(13002,"帐号锁定"),

    //文件上传下载
    DFS_EXCEED_MAX_FILESIZE(7000,"超过单个文件最大尺寸"),
    DFS_UPLOAD_FAILED(7001,"文件上传失败"),
    DFS_UPLOAD_SUCCESS(0,"文件上传成功"),
    DFS_DOWNLOAD_FAILED(7003,"文件下载失败"),
    DFS_DOWNLOAD_SUCCESS(0,"文件下载成功"),

    //菜单校验返回码
    TS_MENU_OPER_SUCCESS(0,"操作成功"),
    TS_MENU_OPER_FAILED(2000,"操作失败"),
    TS_MENU_NAME_CAN_NOT_BE_NULL(2001,"菜单名称不能为空"),
    TS_MENU_CODE_CAN_NOT_BE_NULL(2002,"菜单编码不能为空"),
    TS_MENU_ORDER_CAN_NOT_BE_NULL(2003,"菜单序号不能为空"),
    TS_MENU_QUERY_SUCCESS(0,"查询菜单信息成功"),
    TS_MENU_QUERY_FAILED(2004,"查询菜单信息失败"),
    TS_MENU_HAS_BE_USED(2005,"此菜单有角色使用"),
    //用户操作
    TS_USER_PASSWORD_SUCCESS(0,"修改密码成功"),
    TS_USER_PASSWORD_FAILED(10001,"修改密码失败"),
    TS_USER_ADD_FAILED(13010,"用户添加失败"),
    TS_USER_ADD_SUCCESS(0,"用户添加成功"),
    TS_USER_UPDATE_FAILED(13015,"修改用户失败"),
    TS_USER_UPDATE_SUCCESS(0,"修改用户成功"),

    // 系统角色返回码
    TS_ROLE_OPER_SUCCESS(0,"操作成功"),
    TS_ROLE_OPER_FAILED(-1,"操作失败"),
    TS_ROLE_NAME_CAN_NOT_BE_NOT(3001,"系统角色名称不能为空"),
    TS_ROLE_CODE_CAN_NOT_BE_NOT(3002,"系统角色名称不能为空"),
    TS_ROLE_ID_CAN_NOT_BE_NULL(3003,"系统角色主键ID不能为空"),
    TS_ROLE_HAVE_USER_BE_USEED(3004,"该系统角色有用户在使用"),

    // T_S_APP 升级成功
    TS_APP_FIND_SUCCESS(0,"APP版本查询"),
    TS_APP_FIND_FAILED(4002,"APP版本失败"),
    ;

    private Integer resultCode;
    private String resultMessage;
    private String[] params=null;



    public Integer getResultCode() {
        return resultCode;
    }

    public void setResultCode(Integer resultCode) {
        this.resultCode = resultCode;
    }

    public String getResultMessage() {
        if (params==null)
            return resultMessage;
        else{
            String msg=resultMessage;
            for (int i=0;i<params.length;i++)
                msg=msg.replaceAll("\\{"+(i+1)+"\\}",params[i]);
            return msg;
        }
    }

    public void setResultMessage(String resultMessage) {
        this.resultMessage = resultMessage;
    }

    private SystemMsgCode(Integer resultCode, String resultMessage) {
        this.resultCode = resultCode;
        this.resultMessage = resultMessage;
    }

    public SystemMsgCode replaceParams(String[] params){
        this.params=params;
        return this;
    }

    public static String getSystemMsg(Integer resultCode){
        for (SystemMsgCode systemMsgCode : SystemMsgCode.values()) {
            if (systemMsgCode.getResultCode()==resultCode)
                return systemMsgCode.getResultMessage();
        }
        return "";
    }

    public static SystemMsgCode replaceMsgParams(SystemMsgCode systemMsgCode,String[] params){
        String msg=systemMsgCode.getResultMessage();
        for (int i=0;i<params.length;i++)
            msg=msg.replaceAll("\\{"+(i+1)+"\\}",params[i]);
        REPLACE.setResultCode(systemMsgCode.getResultCode());
        REPLACE.setResultMessage(msg);
        return REPLACE;

    }

}
