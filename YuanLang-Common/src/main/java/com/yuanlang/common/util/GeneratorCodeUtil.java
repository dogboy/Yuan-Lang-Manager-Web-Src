package com.yuanlang.common.util;

import java.util.Random;

/**
 * company 重庆源狼软件科技有限责任公司
 * FileName GeneratorCodeUtil
 * Package com.yuanlang.common.util
 * Description 验证码生成器-- 统一生产六位验证码
 * author Administrator
 * create 2018-06-25 11:21
 * version V1.0
 */

public class GeneratorCodeUtil {

    /**
     * 随机生成6位验证码
     */
    public static String getCode(){
        Random random = new Random();
        String result="";
        for (int i=0;i<6;i++)
        {
            result+=random.nextInt(10);
        }
        System.out.println(result);
        return  result ;
    }

    public static void main(String[] args) {
        System.out.println(getCode());
    }

}
