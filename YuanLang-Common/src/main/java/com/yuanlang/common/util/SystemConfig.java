package com.yuanlang.common.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component("systemConfig")
public class SystemConfig {
    public static final int APPFORM_REJECT_REASON_LENGH = 1024;
    public static final int OFFER_REJECT_REASON_LENGH = 8192;
    public static final int CAR_GIFT_AMOUNT_SHOW_TYPE_HIDE = 0;//不显示
    public static final int CAR_GIFT_AMOUNT_SHOW_TYPE_OIL_CARD = 1;//显示油卡
    public static final int CAR_GIFT_AMOUNT_SHOW_TYPE_SUB_TOTAL_FEE = 2;//减少总保费

    private Boolean sendJmsAsynMessage;
    private Boolean redisCacheEnable;
    private Integer rememberMeDays;
    private String userProfilePath;
    private String restUrl;
    private String appKey;
    private String sessionKey;
    private String appSecret;
    private String productName;
    private Integer securityCodeExpiration;
    private Integer securityCodeLenth;
    private Integer maxAuthFailedTimesEveryDay;
    private Integer timesOfEveryDay;
    private Integer intervalOfSending;
    private Boolean smsLocalTest;
    private Integer picMaxFileSize;

    private String registerTemplateSign;
    private String registerTemplateCode;
    private String identifyAuthTemplateSign;
    private String identifyAuthTemplateCode;
    private String noticeTemplateSign;
    private String noticeTemplateCode;
    private String approvalUnpassTemplateCode;

    private Integer platformAgentCompyId;
    private Integer carEnquiryRecordSaveDays;
    private Integer carOfferRecordSaveDays;

    private Integer carInsProductId;
    private Integer carRunningLicFontAttachDefId;

    private String fileServerUrl;
    private Boolean bizAuditEnabled;

    private String kdniaoEBusinessID;
    private String kdniaoAppkey;
    private String kdniaoApiUrlQuery;
    private Integer kdniaoApiCallIntervalInSecond;

    private String clientAppAllowEnvironment;

    private String serviceCpicNianshenUrl;

    private String operationLoginUrl;


    private String openapiServerHost;

    private String sztTargetURL;
    private String sztMessageRouter;
    private String sztPartnerCode;
    private String sztDocumentProtocol;
    private String sztUser;
    private String sztPassword;
    private String keyPassword;
    private String keyStorePath;


    private String sztAddAccountSecurityCode;
    private String searchAccountSecurityCode;
    private String downLoadAccountSecurityCode;

    private Boolean enableSysLog = false;

    private String sequenceCargoInsureAppformNo;
    private String huaanGzxInsureAppformNo;
    private String sequenceCarPassengerInsAppformDealNo;
    private String sequenceNonCarInsAppformDealNo;
    private String sequenceCarinsAppformDealNo;

    private String sztInsTrafficAccidentProductCode;
    private String sztInsAccountSecurityProductCode;

    private Integer openApiStampDeviationIntervalInMinute;

    private String mobileServicePhone;

    private String baiduMapAK;
    private String sequenceCpicHyxDealno;
    private String htrqInsProductCode;


    public Integer getOpenApiStampDeviationIntervalInMinute() {
        return openApiStampDeviationIntervalInMinute;
    }

    @Value("#{settings['openapi.stamp.deviation.interval.minute']}")
    public void setOpenApiStampDeviationIntervalInMinute(Integer openApiStampDeviationIntervalInMinute) {
        this.openApiStampDeviationIntervalInMinute = openApiStampDeviationIntervalInMinute;
    }

    public String getOpenapiServerHost() {
        return openapiServerHost;
    }

    @Value("#{settings['openapi.server.host']}")
    public void setOpenapiServerHost(String openapiServerHost) {
        this.openapiServerHost = openapiServerHost;
    }

    public String getSztInsTrafficAccidentProductCode() {
        return sztInsTrafficAccidentProductCode;
    }

    @Value("#{settings['szt.ins.traffic.accident.product.code']}")
    public void setSztInsTrafficAccidentProductCode(String sztInsTrafficAccidentProductCode) {
        this.sztInsTrafficAccidentProductCode = sztInsTrafficAccidentProductCode;
    }

    public String getSztInsAccountSecurityProductCode() {
        return sztInsAccountSecurityProductCode;
    }

    @Value("#{settings['szt.ins.account.security.product.code']}")
    public void setSztInsAccountSecurityProductCode(String sztInsAccountSecurityProductCode) {
        this.sztInsAccountSecurityProductCode = sztInsAccountSecurityProductCode;
    }

    public String getSequenceCarinsAppformDealNo() {
        return sequenceCarinsAppformDealNo;
    }

    @Value("#{settings['sequence.carins.appform.dealno']}")
    public void setSequenceCarinsAppformDealNo(String sequenceCarinsAppformDealNo) {
        this.sequenceCarinsAppformDealNo = sequenceCarinsAppformDealNo;
    }

    public String getSequenceCarPassengerInsAppformDealNo() {
        return sequenceCarPassengerInsAppformDealNo;
    }

    @Value("#{settings['sequence.carpassengerins.appform.dealno']}")
    public void setSequenceCarPassengerInsAppformDealNo(String sequenceCarPassengerInsAppformDealNo) {
        this.sequenceCarPassengerInsAppformDealNo = sequenceCarPassengerInsAppformDealNo;
    }

    public String getSequenceNonCarInsAppformDealNo() {
        return sequenceNonCarInsAppformDealNo;
    }

    @Value("#{settings['sequence.noncarins.appform.dealno']}")
    public void setSequenceNonCarInsAppformDealNo(String sequenceNonCarInsAppformDealNo) {
        this.sequenceNonCarInsAppformDealNo = sequenceNonCarInsAppformDealNo;
    }

    public String getServiceCpicNianshenUrl() {
        return serviceCpicNianshenUrl;
    }

    @Value("#{settings['service.cpic.nianshen.url']}")
    public void setServiceCpicNianshenUrl(String serviceCpicNianshenUrl) {
        this.serviceCpicNianshenUrl = serviceCpicNianshenUrl;
    }

    public String getClientAppAllowEnvironment() {
        return clientAppAllowEnvironment;
    }

    @Value("#{settings['app.client.env']}")
    public void setClientAppAllowEnvironment(String clientAppAllowEnvironment) {
        this.clientAppAllowEnvironment = clientAppAllowEnvironment;
    }

    public String getKdniaoEBusinessID() {
        return kdniaoEBusinessID;
    }

    @Value("#{settings['kdniao.EBusinessID']}")
    public void setKdniaoEBusinessID(String kdniaoEBusinessID) {
        this.kdniaoEBusinessID = kdniaoEBusinessID;
    }

    public String getKdniaoAppkey() {
        return kdniaoAppkey;
    }

    @Value("#{settings['kdniao.appkey']}")
    public void setKdniaoAppkey(String kdniaoAppkey) {
        this.kdniaoAppkey = kdniaoAppkey;
    }

    public String getKdniaoApiUrlQuery() {
        return kdniaoApiUrlQuery;
    }

    @Value("#{settings['kdniao.api.url.query']}")
    public void setKdniaoApiUrlQuery(String kdniaoApiUrlQuery) {
        this.kdniaoApiUrlQuery = kdniaoApiUrlQuery;
    }

    public Integer getKdniaoApiCallIntervalInSecond() {
        return kdniaoApiCallIntervalInSecond;
    }


    @Value("#{settings['kdniao.api.call.interval.second']}")
    public void setKdniaoApiCallIntervalInSecond(Integer kdniaoApiCallIntervalInSecond) {
        this.kdniaoApiCallIntervalInSecond = kdniaoApiCallIntervalInSecond;
    }

    public Integer getCarRunningLicFontAttachDefId() {
        return carRunningLicFontAttachDefId;
    }

    @Value("#{settings['car.running.lic.font.attach_def_id']}")
    public void setCarRunningLicFontAttachDefId(Integer carRunningLicFontAttachDefId) {
        this.carRunningLicFontAttachDefId = carRunningLicFontAttachDefId;
    }

    public Boolean getBizAuditEnabled() {
        return bizAuditEnabled;
    }

    @Value("#{settings['biz.audit.enabled']}")
    public void setBizAuditEnabled(Boolean bizAuditEnabled) {
        this.bizAuditEnabled = bizAuditEnabled;
    }


    public String getFileServerUrl() {
        return fileServerUrl;
    }

    @Value("#{settings['file.server.url']}")
    public void setFileServerUrl(String fileServerUrl) {
        this.fileServerUrl = fileServerUrl;
    }


    public Integer getCarInsProductId() {
        return carInsProductId;
    }

    @Value("#{settings['car.ins.product.id']}")
    public void setCarInsProductId(Integer carInsProductId) {
        this.carInsProductId = carInsProductId;
    }

    public Integer getCarEnquiryRecordSaveDays() {
        if (carEnquiryRecordSaveDays == null)
            return 100;
        else
            return carEnquiryRecordSaveDays;
    }

    @Value("#{settings['car.enquiry.record.save.days']}")
    public void setCarEnquiryRecordSaveDays(Integer carEnquiryRecordSaveDays) {
        this.carEnquiryRecordSaveDays = carEnquiryRecordSaveDays;
    }

    public Integer getCarOfferRecordSaveDays() {
        if (carOfferRecordSaveDays == null)
            return 10;
        else
            return carOfferRecordSaveDays;
    }

    @Value("#{settings['car.offer.record.save.days']}")
    public void setCarOfferRecordSaveDays(Integer carOfferRecordSaveDays) {
        this.carOfferRecordSaveDays = carOfferRecordSaveDays;
    }

    public Boolean getSendJmsAsynMessage() {
        return sendJmsAsynMessage;
    }

    @Value("#{settings['jms.SendAsynMessage']}")
    public void setSendJmsAsynMessage(Boolean sendJmsAsynMessage) {
        this.sendJmsAsynMessage = sendJmsAsynMessage;
    }

    public Boolean getRedisCacheEnable() {
        return redisCacheEnable;
    }

    @Value("#{settings['redis.cacheEnable']}")
    public void setRedisCacheEnable(Boolean redisCacheEnable) {
        this.redisCacheEnable = redisCacheEnable;
    }

    public Integer getMaxAuthFailedTimesEveryDay() {
        return maxAuthFailedTimesEveryDay;
    }

    @Value("#{settings['auth.max_failed_times.everyday']}")
    public void setMaxAuthFailedTimesEveryDay(Integer maxAuthFailedTimesEveryDay) {
        this.maxAuthFailedTimesEveryDay = maxAuthFailedTimesEveryDay;
    }

    public Integer getRememberMeDays() {
        return rememberMeDays;
    }

    @Value("#{settings['security.rememberme.days']}")
    public void setRememberMeDays(Integer rememberMeDays) {
        this.rememberMeDays = rememberMeDays;
    }


    public String getUserProfilePath() {
        return userProfilePath;
    }

    @Value("#{settings['path.pic.userprofile']}")
    public void setUserProfilePath(String userProfilePath) {
        this.userProfilePath = userProfilePath;
    }


    public String getRestUrl() {
        return restUrl;
    }

    @Value("#{settings['sms.restUrl']}")
    public void setRestUrl(String restUrl) {
        this.restUrl = restUrl;
    }

    public Integer getSecurityCodeExpiration() {
        return securityCodeExpiration;
    }

    @Value("#{settings['sms.securityCode.expiration']}")
    public void setSecurityCodeExpiration(Integer securityCodeExpiration) {
        this.securityCodeExpiration = securityCodeExpiration;
    }

    public Integer getSecurityCodeLenth() {
        return securityCodeLenth;
    }

    @Value("#{settings['sms.securityCode.lenth']}")
    public void setSecurityCodeLenth(Integer securityCodeLenth) {
        this.securityCodeLenth = securityCodeLenth;
    }

    public String getAppKey() {
        return appKey;
    }

    @Value("#{settings['sms.appKey']}")
    public void setAppKey(String appKey) {
        this.appKey = appKey;
    }

    public String getSessionKey() {
        return sessionKey;
    }

    @Value("#{settings['sms.sessionKey']}")
    public void setSessionKey(String sessionKey) {
        this.sessionKey = sessionKey;
    }

    public String getAppSecret() {
        return appSecret;
    }

    @Value("#{settings['sms.appSecret']}")
    public void setAppSecret(String appSecret) {
        this.appSecret = appSecret;
    }

    public String getRegisterTemplateSign() {
        return registerTemplateSign;
    }

    @Value("#{settings['sms.template.register.auth.sign']}")
    public void setRegisterTemplateSign(String registerTemplateSign) {
        this.registerTemplateSign = registerTemplateSign;
    }

    public String getRegisterTemplateCode() {
        return registerTemplateCode;
    }

    @Value("#{settings['sms.template.register.auth.code']}")
    public void setRegisterTemplateCode(String registerTemplateCode) {
        this.registerTemplateCode = registerTemplateCode;
    }

    public String getIdentifyAuthTemplateSign() {
        return identifyAuthTemplateSign;
    }

    @Value("#{settings['sms.template.identify.auth.sign']}")
    public void setIdentifyAuthTemplateSign(String identifyAuthTemplateSign) {
        this.identifyAuthTemplateSign = identifyAuthTemplateSign;
    }

    public String getIdentifyAuthTemplateCode() {
        return identifyAuthTemplateCode;
    }

    @Value("#{settings['sms.template.identify.auth.code']}")
    public void setIdentifyAuthTemplateCode(String identifyAuthTemplateCode) {
        this.identifyAuthTemplateCode = identifyAuthTemplateCode;
    }

    public String getNoticeTemplateSign() {
        return noticeTemplateSign;
    }

    @Value("#{settings['sms.template.notice.auth.sign']}")
    public void setNoticeTemplateSign(String noticeTemplateSign) {
        this.noticeTemplateSign = noticeTemplateSign;
    }

    public String getNoticeTemplateCode() {
        return noticeTemplateCode;
    }

    @Value("#{settings['sms.template.notice.auth.code']}")
    public void setNoticeTemplateCode(String noticeTemplateCode) {
        this.noticeTemplateCode = noticeTemplateCode;
    }

    public String getProductName() {
        return productName;
    }

    @Value("#{settings['product.name']}")
    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Boolean getSmsLocalTest() {
        return smsLocalTest;
    }

    @Value("#{settings['sms.localtest']}")
    public void setSmsLocalTest(Boolean smsLocalTest) {
        this.smsLocalTest = smsLocalTest;
    }

    public Integer getTimesOfEveryDay() {
        return timesOfEveryDay;
    }

    @Value("#{settings['sms.timesOfEveryDay']}")
    public void setTimesOfEveryDay(Integer timesOfEveryDay) {
        this.timesOfEveryDay = timesOfEveryDay;
    }

    public Integer getPicMaxFileSize() {
        return picMaxFileSize;
    }

    @Value("#{settings['pic.MaxFileSize']}")
    public void setPicMaxFileSize(Integer picMaxFileSize) {
        this.picMaxFileSize = picMaxFileSize;
    }

    public Integer getPlatformAgentCompyId() {
        return platformAgentCompyId;
    }

    @Value("#{settings['platform.agent_company_id']}")
    public void setPlatformAgentCompyId(Integer platformAgentCompyId) {
        this.platformAgentCompyId = platformAgentCompyId;
    }

    public Integer getIntervalOfSending() {
        return intervalOfSending;
    }

    @Value("#{settings['sms.intervalOfSending']}")
    public void setIntervalOfSending(Integer intervalOfSending) {
        this.intervalOfSending = intervalOfSending;
    }

    public String getOperationLoginUrl() {
        return operationLoginUrl;
    }

    @Value("#{settings['operation.login.url']}")
    public void setOperationLoginUrl(String operationLoginUrl) {
        this.operationLoginUrl = operationLoginUrl;
    }

    public MobileClientConfig newMobileClientConfig() {
        MobileClientConfig mcc = new MobileClientConfig();
        mcc.setFileServerUrl(this.getFileServerUrl());
        mcc.setClientAppAllowEnvironment(this.getClientAppAllowEnvironment());
        mcc.setServiceCpicNianshenUrl(this.getServiceCpicNianshenUrl());
        mcc.setOperationLoginUrl(this.getOperationLoginUrl());
        return mcc;
    }

    public ServerClientConfig newServerClientConfig() {
        ServerClientConfig scc = new ServerClientConfig();
        scc.setFileServerUrl(this.getFileServerUrl());
        return scc;
    }


    public String getSztTargetURL() {
        return sztTargetURL;
    }

    @Value("#{settings['sztAccountSecurityRisk.targetURL']}")
    public void setSztTargetURL(String sztTargetURL) {
        this.sztTargetURL = sztTargetURL;
    }

    public String getSztMessageRouter() {
        return sztMessageRouter;
    }

    @Value("#{settings['sztAccountSecurityRisk.messageRouter']}")
    public void setSztMessageRouter(String sztMessageRouter) {
        this.sztMessageRouter = sztMessageRouter;
    }

    public String getSztPartnerCode() {
        return sztPartnerCode;
    }

    @Value("#{settings['sztAccountSecurityRisk.partnerCode']}")
    public void setSztPartnerCode(String sztPartnerCode) {
        this.sztPartnerCode = sztPartnerCode;
    }

    public String getSztDocumentProtocol() {
        return sztDocumentProtocol;
    }

    @Value("#{settings['sztAccountSecurityRisk.documentProtocol']}")
    public void setSztDocumentProtocol(String sztDocumentProtocol) {
        this.sztDocumentProtocol = sztDocumentProtocol;
    }

    public String getSztUser() {
        return sztUser;
    }

    @Value("#{settings['sztAccountSecurityRisk.user']}")
    public void setSztUser(String sztUser) {
        this.sztUser = sztUser;
    }

    public String getSztPassword() {
        return sztPassword;
    }

    @Value("#{settings['sztAccountSecurityRisk.password']}")
    public void setSztPassword(String sztPassword) {
        this.sztPassword = sztPassword;
    }

    public String getSztAddAccountSecurityCode() {
        return sztAddAccountSecurityCode;
    }

    @Value("#{settings['sztAccountSecurityRisk.sztAddAccountSecurityCode']}")
    public void setSztAddAccountSecurityCode(String sztAddAccountSecurityCode) {
        this.sztAddAccountSecurityCode = sztAddAccountSecurityCode;
    }

    public String getSearchAccountSecurityCode() {
        return searchAccountSecurityCode;
    }

    @Value("#{settings['sztAccountSecurityRisk.sztSearchAccountSecurityCode']}")
    public void setSearchAccountSecurityCode(String searchAccountSecurityCode) {
        this.searchAccountSecurityCode = searchAccountSecurityCode;
    }

    public String getDownLoadAccountSecurityCode() {
        return downLoadAccountSecurityCode;
    }

    @Value("#{settings['sztAccountSecurityRisk.sztDownLoadAccountSecurityCode']}")
    public void setDownLoadAccountSecurityCode(String downLoadAccountSecurityCode) {
        this.downLoadAccountSecurityCode = downLoadAccountSecurityCode;
    }

    public String getKeyPassword() {
        return keyPassword;
    }

    @Value("#{settings['sztAccountSecurityRisk.keyPassword']}")
    public void setKeyPassword(String keyPassword) {
        this.keyPassword = keyPassword;
    }

    public String getKeyStorePath() {
        return keyStorePath;
    }

    @Value("#{settings['sztAccountSecurityRisk.keyStorePath']}")
    public void setKeyStorePath(String keyStorePath) {
        this.keyStorePath = keyStorePath;
    }

    public Boolean getEnableSysLog() {
        return enableSysLog;
    }

    @Value("#{settings['sysLog.isEnable']}")
    public void setEnableSysLog(Boolean enableSysLog) {
        this.enableSysLog = enableSysLog;
    }

    public String getSequenceCargoInsureAppformNo() {
        return sequenceCargoInsureAppformNo;
    }

    @Value("#{settings['sequence.cargo.insure.appformno']}")
    public void setSequenceCargoInsureAppformNo(String sequenceCargoInsureAppformNo) {
        this.sequenceCargoInsureAppformNo = sequenceCargoInsureAppformNo;
    }

    public String getMobileServicePhone() {
        return mobileServicePhone;
    }

    @Value("#{settings['mobile.service.phone']}")
    public void setMobileServicePhone(String mobileServicePhone) {
        this.mobileServicePhone = mobileServicePhone;
    }

    public String getBaiduMapAK() {
        return baiduMapAK;
    }

    @Value("#{settings['baidu.map.ak']}")
    public void setBaiduMapAK(String baiduMapAK) {
        this.baiduMapAK = baiduMapAK;
    }

    public String getHuaanGzxInsureAppformNo() {
        return huaanGzxInsureAppformNo;
    }

    @Value("#{settings['sequence.huaan.gzx.dealno']}")
    public void setHuaanGzxInsureAppformNo(String huaanGzxInsureAppformNo) {
        this.huaanGzxInsureAppformNo = huaanGzxInsureAppformNo;
    }

    public String getApprovalUnpassTemplateCode() {
        return approvalUnpassTemplateCode;
    }

    @Value("#{settings['sms.template.approval.unpass.code']}")
    public void setApprovalUnpassTemplateCode(String approvalUnpassTemplateCode) {
        this.approvalUnpassTemplateCode = approvalUnpassTemplateCode;
    }

    public String getSequenceCpicHyxDealno() {
        return sequenceCpicHyxDealno;
    }

    @Value("#{settings['sequence.cpic.hyx.dealno']}")
    public void setSequenceCpicHyxDealno(String sequenceCpicHyxDealno) {
        this.sequenceCpicHyxDealno = sequenceCpicHyxDealno;
    }

    public String getHtrqInsProductCode() {
        return htrqInsProductCode;
    }

    @Value("#{settings['htrq.ins.product.code']}")
    public void setHtrqInsProductCode(String htrqInsProductCode) {
        this.htrqInsProductCode = htrqInsProductCode;
    }
}
