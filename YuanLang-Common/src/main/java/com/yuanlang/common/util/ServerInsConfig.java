package com.yuanlang.common.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by lvluogang on 2016/11/2.
 */
public class ServerInsConfig {

    private static final Logger log = LoggerFactory.getLogger(ServerInsConfig.class);

    private static final String configFileName="instance.properties";

    private static String serverName;

    static{
        InputStream is=null;
        try {
            is = ServerInsConfig.class.getClassLoader().getResourceAsStream(configFileName);
            loadConfig(is);
        } catch (Exception e) {
            log.error(e.getMessage(),e);
        } finally{
            try {
                if (is!=null)
                    is.close();
            } catch (IOException e) {
                log.error(e.getMessage(),e);
            }
        }
    }

    private static void loadConfig(InputStream fis){
        Properties prop = new Properties();
        try {
            prop.load(fis);
            serverName=SysUtils.nullString(prop.getProperty("server.name"));
        } catch (IOException e) {
            log.error(e.getMessage(),e);
        }
    }

    public static String getServerName() {
        return serverName;
    }
}
