package com.yuanlang.common.util;

import java.io.Serializable;

/**
 * company 重庆源狼软件科技有限责任公司
 * FileName AccessToken
 * Package com.yuanlang.common.util
 * Description 拦截token
 * author Administrator
 * create 2018-05-15 17:08
 * version V1.0
 */

public class AccessToken implements Serializable{

    String  access_token ;  //  前端对应访问的数据值;

    String  customerId ;    //  存放客户Id

    Boolean isAdmin;// 判断是否为管理员

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public Boolean getAdmin() {
        return isAdmin;
    }

    public void setAdmin(Boolean admin) {
        isAdmin = admin;
    }
}
