package com.yuanlang.common.util;

import org.apache.http.HttpStatus;
import org.apache.http.entity.ContentType;

/**
 * Created by lvluogang on 2017/5/9.
 */
public class HttpResult {
    private int statusCode;
    private String responseText;
    private ContentType contentType;

    public HttpResult() {
        statusCode=-1;
        responseText=null;
        contentType=null;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getResponseText() {
        return responseText;
    }

    public void setResponseText(String responseText) {
        this.responseText = responseText;
    }

    public ContentType getContentType() {
        return contentType;
    }

    public void setContentType(ContentType contentType) {
        this.contentType = contentType;
    }

    public boolean isOK(){
        return this.statusCode== HttpStatus.SC_OK;
    }

    public boolean isJSONResonse(){
        return responseText!=null && contentType!=null && ContentType.APPLICATION_JSON.getMimeType().equalsIgnoreCase(contentType.getMimeType());
    }

    public boolean isXMLResponse(){
        return responseText!=null && contentType!=null && ContentType.APPLICATION_XML.getMimeType().equalsIgnoreCase(contentType.getMimeType());
    }

    public boolean isTextResponse(){
        return responseText!=null && contentType!=null && ContentType.DEFAULT_TEXT.getMimeType().equalsIgnoreCase(contentType.getMimeType());
    }
}
