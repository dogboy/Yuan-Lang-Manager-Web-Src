package com.yuanlang.common.util;

import java.util.UUID;

/**
 * company 重庆源狼软件科技有限责任公司
 * FileName t
 * Package com.soft.link.utils
 * Description
 * author Administrator
 * create 2018-04-25 16:33
 * version V1.0
 */
public class UniqueUtil {

    public UniqueUtil() {
    }

    public static String uuid() {
        UUID uuid = UUID.randomUUID();
        return uuid.toString().replace("-", "").toUpperCase();
    }

    public static String uuid16() {
        UUID uuid = UUID.randomUUID();
        return uuid.toString().replace("-", "").toUpperCase().substring(0, 16);
    }

}

