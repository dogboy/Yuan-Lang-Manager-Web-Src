package com.yuanlang.common.util;
/**
 * company 重庆源狼软件科技有限责任公司
 * FileName AliyunSmsMsgCode
 * Package com.yuanlang.common.util
 * Description 阿里云信短信提示
 * author Administrator
 * create 2018-06-23 21:22
 * version V1.0
 */
public enum AliyunSmsMsgCode {

    /**
     * 短信调用系统错误码
     */
    /*
        SignatureDoesNotMatch	Specified signature is not matched with our calculation.	Signature加密错误，如为SDK调用，则需要注意accessKeyId和accessKeySecret字符串赋值正确无误；如自行加密的Signature，则需要检查加密逻辑，对照文档：https://help.aliyun.com/document_detail/56189.html
        InvalidTimeStamp.Expired	Specified time stamp or date value is expired.	时间戳错误，发出请求的时间和服务器接收到请求的时间不在15分钟内。经常出现该错误的原因是时区原因造成的,目前网关使用的时间是GMT时间
        SignatureNonceUsed	Specified signature nonce was used already.	唯一随机数重复，SignatureNonce为唯一随机数，用于防止网络重放攻击。不同请求间要使用不同的随机数值。
        InvalidVersion	Specified parameter Version is not valid.	版本号错误，需要确认接口的版本号，如云通信短信、语音、流量服务的Version=2017-05-25
        InvalidAction.NotFound	Specified api is not found, please check your url and method	接口名错误，需要确认接口地址和接口名，如云通信短信服务短信发送：dysmsapi.aliyuncs.com，接口名Action=SendSms
    */
    ALIYUN_SMS_SYSTEM_SIGNATUREDOESNOTMATCH("SignatureDoesNotMatch","Signature加密错误，如为SDK调用，则需要注意accessKeyId和accessKeySecret字符串赋值正确无误；如自行加密的Signature，则需要检查加密逻辑"),
    ALIYUN_SMS_SYSTEM_INVALIDTIMESTAMP_EXPIRED("InvalidTimeStamp.Expired","时间戳错误，发出请求的时间和服务器接收到请求的时间不在15分钟内。经常出现该错误的原因是时区原因造成的,目前网关使用的时间是GMT时间"),
    ALIYUN_SMS_SYSTEM_INVALIDVERSION("InvalidVersion","版本号错误，需要确认接口的版本号，如云通信短信、语音、流量服务的Version=2017-05-25"),
    ALIYUN_SMS_SYSTEM_INVALIDACTION_NOTFOUND("InvalidAction.NotFound","接口名错误，需要确认接口地址和接口名，如云通信短信服务短信发送：dysmsapi.aliyuncs.com，接口名Action=SendSms"),
    /**
     * 短信调用-业务错误码
     */
    ALIYUN_SMS_OK("OK","请求成功"),
    ALIYUN_SMS_ISP_RAM_PERMISSION_DENY("isp.RAM_PERMISSION_DENY","RAM权限DENY"),
    ALIYUN_SMS_ISV_OUT_OF_SERVICE("isv.OUT_OF_SERVICE","业务停机"),
    ALIYUN_SMS_ISV_PRODUCT_UN_SUBSCRIPT("isv.PRODUCT_UN_SUBSCRIPT","未开通云通信产品的阿里云客户"),
    ALIYUN_SMS_ISV_PRODUCT_UNSUBSCRIBE("isv.PRODUCT_UNSUBSCRIBE","产品未开通"),
    ALIYUN_SMS_ISV_ACCOUNT_NOT_EXISTS("isv.ACCOUNT_NOT_EXISTS","账户不存在"),
    ALIYUN_SMS_ISV_ACCOUNT_ABNORMAL("isv.ACCOUNT_ABNORMAL","账户异常"),
    ALIYUN_SMS_ISV_SMS_TEMPLATE_ILLEGAL("isv.SMS_TEMPLATE_ILLEGAL","短信模板不合法"),
    ALIYUN_SMS_ISV_SMS_SIGNATURE_ILLEGAL("isv.SMS_SIGNATURE_ILLEGAL","短信签名不合法"),
    ALIYUN_SMS_ISV_INVALID_PARAMETERS("isv.INVALID_PARAMETERS","参数异常"),
    ALIYUN_SMS_ISP_SYSTEM_ERROR("isp.SYSTEM_ERROR","系统错误"),
    ALIYUN_SMS_ISV_MOBILE_NUMBER_ILLEGAL("isv.MOBILE_NUMBER_ILLEGAL","非法手机号"),
    ALIYUN_SMS_ISV_MOBILE_COUNT_OVER_LIMIT("isv.MOBILE_COUNT_OVER_LIMIT","手机号码数量超过限制"),
    ALIYUN_SMS_ISV_TEMPLATE_MISSING_PARAMETERS("isv.TEMPLATE_MISSING_PARAMETERS","模板缺少变量"),
    ALIYUN_SMS_ISV_BUSINESS_LIMIT_CONTROL("isv.BUSINESS_LIMIT_CONTROL","业务限流"),
    ALIYUN_SMS_ISV_INVALID_JSON_PARAM("isv.INVALID_JSON_PARAM","JSON参数不合法，只接受字符串值"),
    ALIYUN_SMS_ISV_BLACK_KEY_CONTROL_LIMIT("isv.BLACK_KEY_CONTROL_LIMIT","黑名单管控"),
    ALIYUN_SMS_ISV_PARAM_LENGTH_LIMIT("isv.PARAM_LENGTH_LIMIT","参数超出长度限制"),
    ALIYUN_SMS_ISV_PARAM_NOT_SUPPORT_URL("isv.PARAM_NOT_SUPPORT_URL","不支持URL"),
    ALIYUN_SMS_ISV_AMOUNT_NOT_ENOUGH("isv.AMOUNT_NOT_ENOUGH","账户余额不足"),

    ALIYUN_SMS_SEND_1005("-1005","内容含有违禁词"),
    ALIYUN_SMS_SEND_185("-185","分组手机号每天限制条数"),
    ALIYUN_SMS_SEND_182("-182","内容中超过空格限制"),
   // ALIYUN_SMS_SEND_181("-181","每天验证码拦截"),
    ALIYUN_SMS_SEND_144("-144","组黑名单"),
  //  ALIYUN_SMS_SEND_131("-131","验证码30秒拦截"),
   // ALIYUN_SMS_SEND_128("-128","通道手机号拦截"),
   // ALIYUN_SMS_SEND_119("-119","不符合本用户任何模板"),
   // ALIYUN_SMS_SEND_118("-118","找不到用户"),
   // ALIYUN_SMS_SEND_117("-117","无法区分实时还是批量"),
    ALIYUN_SMS_SEND_116("-116","下发日期格式异常"),
    ALIYUN_SMS_SEND_115("-115","用户状态为测试用户"),
    ALIYUN_SMS_SEND_114("-114","用户每天条数限制"),
    ALIYUN_SMS_SEND_113("-113","账号拦截状态"),
    ALIYUN_SMS_SEND_112("-112","手机号码格式不正确"),
    ALIYUN_SMS_SEND_111("-111","指定扩展号拦截"),
    ALIYUN_SMS_SEND_109("-109","扩展号不在白名单中"),
    ALIYUN_SMS_SEND_108("-108","超过手机号条数限制"),
    ALIYUN_SMS_SEND_107("-107","手机号内容重复"),
    ALIYUN_SMS_SEND_106("-106","内容含有黑内容"),
    ALIYUN_SMS_SEND_101("-101","拦截指定用户运营商信息出错"),
    ALIYUN_SMS_SEND_100("-100","获取运营商出错"),
    ALIYUN_SMS_SEND_99("-99","一分钟拦截"),
    ALIYUN_SMS_SEND_95("-95","空号"),
    ALIYUN_SMS_SEND_74("-74","运营商内部错误"),
    ALIYUN_SMS_SEND_43("-43","内存满"),
    ALIYUN_SMS_SEND_37("-37","关机"),
    ALIYUN_SMS_SEND_15("-15","签名黑名单"),
   // ALIYUN_SMS_SEND_14("-14","手机号在用户黑名单"),
   // ALIYUN_SMS_SEND_13("-13","手机号在平台黑名单"),
    ALIYUN_SMS_SEND_24("-24","计费失败"),
    ALIYUN_SMS_SEND_20("-20","msgid使用错误"),
    ALIYUN_SMS_SEND_19("-19","密码错误"),
    ALIYUN_SMS_SEND_18("-18","指定访问ip错误"),
    ALIYUN_SMS_SEND_17("-17","密码填写错误"),
    ALIYUN_SMS_SEND_16("-16","用户名不存在"),
    ALIYUN_SMS_SEND_14("-14","msg_id超过50个字符"),

    ALIYUN_SMS_SEND_13("-13","手机号码超过200个或合法的手机号码为空"),
    ALIYUN_SMS_SEND_12("-12","短信内容为空或超过500字符"),
    ALIYUN_SMS_SEND_11("-11","账户状态关闭"),
    ALIYUN_SMS_SEND_10("-10","余额不足"),
    ALIYUN_SMS_SEND_018("18","白名单驳回"),
    ALIYUN_SMS_SEND_056("56","重复发送拦截"),
    ALIYUN_SMS_SEND_0116("116","个性化短信提交个数超过200条"),
    ALIYUN_SMS_SEND_0117("117","子账户设置了全部拦截"),
    ALIYUN_SMS_SEND_0124("124","长短信在审核表中24小时内无法拼接完整"),
    ALIYUN_SMS_SEND_0125("125","群发拦截"),
    ALIYUN_SMS_SEND_GL0000("GL:0000","同一个号码下发频次限制。"),
    ALIYUN_SMS_SEND_1007("-1007","针对诈骗信息的空格拦截"),
    ALIYUN_SMS_SEND_181("-181","每天验证码拦截"),
    ALIYUN_SMS_SEND_180("-180","分组限制拦截"),
    ALIYUN_SMS_SEND_131("-131","30秒拦截"),
    ALIYUN_SMS_SEND_128("-128","通道手机号拦截"),
    ALIYUN_SMS_SEND_119("-119","不符合本用户任何模版"),
    ALIYUN_SMS_SEND_118("-118","找不到用户"),
    ALIYUN_SMS_SEND_117("-117","无法区分实时还是批量"),
    /*
    https://help.aliyun.com/document_detail/55323.html?spm=a2c4g.11186623.6.586.bxQqze
    */
    ;
    private String  resultCode;
    private String resultMessage;
    private String[] params=null;

    private AliyunSmsMsgCode(String resultCode, String resultMessage) {
        this.resultCode = resultCode;
        this.resultMessage = resultMessage;
    }
    public String getResultCode() {
        return resultCode;
    }

    public void setResultCode(String resultCode) {
        this.resultCode = resultCode;
    }

    public String getResultMessage() {
        return resultMessage;
    }

    public void setResultMessage(String resultMessage) {
        this.resultMessage = resultMessage;
    }
}
