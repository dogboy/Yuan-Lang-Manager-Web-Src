package com.yuanlang.common.util;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Random;
import java.util.UUID;

public class PicVerifyCodeUtils {
	private static Random random = new Random();

	public static String generateVerifyCode(int verifyLength,
			String verifyCodeSet) {
		int codesLen = verifyCodeSet.length();
		Random rand = new Random(System.currentTimeMillis());
		StringBuilder verifyCode = new StringBuilder(verifyLength);
		for (int i = 0; i < verifyLength; i++) {
			verifyCode.append(verifyCodeSet.charAt(rand.nextInt(codesLen - 1)));
		}
		return verifyCode.toString();
	}

	public static void outputImage(int w, int h, OutputStream os, String code,
			String fontName) throws Exception {
		// TODO Auto-generated method stub
		int verifyLength = code.length();
		BufferedImage image = new BufferedImage(w, h,
				BufferedImage.TYPE_INT_RGB);
		Random rand = new Random();
		Graphics2D g2 = image.createGraphics();
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);
		Color[] colors = new Color[5];
		Color[] colorSpaces = new Color[] { Color.WHITE, Color.CYAN,
				Color.GRAY, Color.LIGHT_GRAY, Color.MAGENTA, Color.ORANGE,
				Color.PINK, Color.YELLOW };
		float[] fractions = new float[colors.length];
		for (int i = 0; i < colors.length; i++) {
			colors[i] = colorSpaces[rand.nextInt(colorSpaces.length)];
			fractions[i] = rand.nextFloat();
		}
		Arrays.sort(fractions);

		g2.setColor(Color.GRAY);// 设置边框色
		g2.fillRect(0, 0, w, h);

		Color c = getRandColor(200, 250);
		g2.setColor(c);// 设置背景色
		g2.fillRect(0, 2, w, h - 4);

		// 绘制干扰线
		Random random = new Random();
		g2.setColor(getRandColor(160, 200));// 设置线条的颜色
		for (int i = 0; i < 20; i++) {
			int x = random.nextInt(w - 1);
			int y = random.nextInt(h - 1);
			int xl = random.nextInt(6) + 1;
			int yl = random.nextInt(12) + 1;
			g2.drawLine(x, y, x + xl + 40, y + yl + 20);
		}

		// 添加噪点
		float yawpRate = 0.05f;// 噪声率
		int area = (int) (yawpRate * w * h);
		for (int i = 0; i < area; i++) {
			int x = random.nextInt(w);
			int y = random.nextInt(h);
			int rgb = getRandomIntColor();
			image.setRGB(x, y, rgb);
		}

		shear(g2, w, h, c);// 使图片扭曲

		g2.setColor(getRandColor(100, 160));
		int fontSize = h - 4;
		Font font = new Font(fontName, Font.ITALIC, fontSize);
		g2.setFont(font);
		char[] chars = code.toCharArray();
		for (int i = 0; i < verifyLength; i++) {
			AffineTransform affine = new AffineTransform();
			affine.setToRotation(
					Math.PI / 4 * rand.nextDouble()
							* (rand.nextBoolean() ? 1 : -1), (w / verifyLength)
							* i + fontSize / 2, h / 2);
			g2.setTransform(affine);
			g2.drawChars(chars, i, 1, ((w - 10) / verifyLength) * i + 5, h / 2
					+ fontSize / 2 - 5);
		}

		g2.dispose();
		ImageIO.write(image, "jpg", os);
	}

	private static Color getRandColor(int fc, int bc) {
		if (fc > 255)
			fc = 255;
		if (bc > 255)
			bc = 255;
		int r = fc + random.nextInt(bc - fc);
		int g = fc + random.nextInt(bc - fc);
		int b = fc + random.nextInt(bc - fc);
		return new Color(r, g, b);
	}

	private static int getRandomIntColor() {
		int[] rgb = getRandomRgb();
		int color = 0;
		for (int c : rgb) {
			color = color << 8;
			color = color | c;
		}
		return color;
	}

	private static int[] getRandomRgb() {
		int[] rgb = new int[3];
		for (int i = 0; i < 3; i++) {
			rgb[i] = random.nextInt(255);
		}
		return rgb;
	}

	private static void shear(Graphics g, int w1, int h1, Color color) {
		shearX(g, w1, h1, color);
		shearY(g, w1, h1, color);
	}

	private static void shearX(Graphics g, int w1, int h1, Color color) {

		int period = random.nextInt(2);

		boolean borderGap = true;
		int frames = 1;
		int phase = random.nextInt(2);

		for (int i = 0; i < h1; i++) {
			double d = (double) (period >> 1)
					* Math.sin((double) i / (double) period
							+ (6.2831853071795862D * (double) phase)
							/ (double) frames);
			g.copyArea(0, i, w1, 1, (int) d, 0);
			if (borderGap) {
				g.setColor(color);
				g.drawLine((int) d, i, 0, i);
				g.drawLine((int) d + w1, i, w1, i);
			}
		}

	}

	private static void shearY(Graphics g, int w1, int h1, Color color) {

		int period = random.nextInt(40) + 10; // 50;

		boolean borderGap = true;
		int frames = 20;
		int phase = 7;
		for (int i = 0; i < w1; i++) {
			double d = (double) (period >> 1)
					* Math.sin((double) i / (double) period
							+ (6.2831853071795862D * (double) phase)
							/ (double) frames);
			g.copyArea(i, 0, 1, h1, 0, (int) d);
			if (borderGap) {
				g.setColor(color);
				g.drawLine(i, (int) d, i, 0);
				g.drawLine(i, (int) d + h1, i, h1);
			}

		}

	}

	public static String outputVerifyImage(int w, int h, OutputStream os,
			int verifyLength, String verifyCodeSet, String fontName)
			throws Exception {
		// TODO Auto-generated method stub
		String verifyCode = generateVerifyCode(verifyLength, verifyCodeSet);
		outputImage(w, h, os, verifyCode, fontName);
		return verifyCode;
	}


	public static String clearVerifyImageNoise(String imageFile,String outPath) throws Exception{
		BufferedImage img = ImageIO.read(new File(imageFile));
		//获取图片的高宽
		int width = img.getWidth();
		int height = img.getHeight();

		ColorModel cm = ColorModel.getRGBdefault();
		for (int x=0;x<width;x++){
			for (int y=0;y<height;y++){

				//img.setRGB(x,y,img.);
			}

		}

		/*
		//循环执行除去干扰像素
		for(int i = 1;i < width;i++){
			Color colorFirst = new Color(img.getRGB(i, 1));
			int numFirstGet = colorFirst.getRed()+colorFirst.getGreen()+colorFirst.getBlue();
			for (int x = 0; x < width; x++)
			{
				for (int y = 0; y < height; y++)
				{
					Color color = new Color(img.getRGB(x, y));
					//System.out.println("red:"+color.getRed()+" | green:"+color.getGreen()+" | blue:"+color.getBlue());
					int num = color.getRed()+color.getGreen()+color.getBlue();
					if(num >= numFirstGet){
						img.setRGB(x, y, Color.WHITE.getRGB());
					}
				}
			}
		}

		//图片背景变黑色
		for(int i = 1;i<width;i++){
			Color color1 = new Color(img.getRGB(i, 1));
			int num1 = color1.getRed()+color1.getGreen()+color1.getBlue();
			for (int x = 0; x < width; x++)
			{
				for (int y = 0; y < height; y++)
				{
					Color color = new Color(img.getRGB(x, y));
					//System.out.println("red:"+color.getRed()+" | green:"+color.getGreen()+" | blue:"+color.getBlue());
					int num = color.getRed()+color.getGreen()+color.getBlue();
					if(num==num1){
						img.setRGB(x, y, Color.BLACK.getRGB());
					}else{
						img.setRGB(x, y, Color.WHITE.getRGB());
					}
				}
			}
		}
		*/


		//保存图片
		String outFileName=outPath+"/"+ UUID.randomUUID().toString()+".jpg";
		File file = new File(outFileName);
		if (!file.exists())
		{
			File dir = file.getParentFile();
			if (!dir.exists())
			{
				dir.mkdirs();
			}
			try
			{
				file.createNewFile();
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
		}
		ImageIO.write(img, "jpg", file);
		return outFileName;
	}
}
