package com.yuanlang.common.util;

/**
 * Created by lvluogang on 16/9/30.
 */
public class ServerClientConfig {
    private String fileServerUrl;

    public String getFileServerUrl() {
        return fileServerUrl;
    }

    public void setFileServerUrl(String fileServerUrl) {
        this.fileServerUrl = fileServerUrl;
    }
}
