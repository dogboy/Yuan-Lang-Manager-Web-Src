package com.yuanlang.facade.model.system;

import java.io.Serializable;
import java.util.List;

/**
 * @author hanhq
 * @return {code:success;msg;msgContent:data}
 * @throws {如果有异常说明请填写}
 * @methodname
 * @Description 组装成页面左侧菜单对象
 * @create 2018/6/3
 */
public class MenuModel implements Serializable {

    private Long menuId;// 菜单ID
    private String title;// 标题
    private String icon;// 图标
    private String jump;// 跳转路径
    private List<MenuModel> list;

    public Long getMenuId() {
        return menuId;
    }

    public void setMenuId(Long menuId) {
        this.menuId = menuId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getJump() {
        return jump;
    }

    public void setJump(String jump) {
        this.jump = jump;
    }

    public List<MenuModel> getList() {
        return list;
    }

    public void setList(List<MenuModel> list) {
        this.list = list;
    }
}
