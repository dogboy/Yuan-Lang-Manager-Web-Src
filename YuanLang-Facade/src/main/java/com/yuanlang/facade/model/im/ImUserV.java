package com.yuanlang.facade.model.im;

import java.io.Serializable;

/**
 * layIM用户信息表，自动添加 或者 系统同步
 */
public class ImUserV extends ImUser implements Serializable {

    private String  content ; // 消息内容

    private String type ;

    private boolean mine ;

    private Long fromid ;

    private String cid ;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isMine() {
        return mine;
    }

    public void setMine(boolean mine) {
        this.mine = mine;
    }


    public Long getFromid() {
        return fromid;
    }

    public void setFromid(Long fromid) {
        this.fromid = fromid;
    }

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }
}