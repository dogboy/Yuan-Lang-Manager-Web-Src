package com.yuanlang.facade.model.system;

import java.io.Serializable;

/**
 * 短信发送
 */
public class TsSMS implements Serializable{

    private Long id;

    private String phonenumbers;

    private String signname;

    private String templatecode;

    private String templateparam;

    private String smsupextendcode;

    private Long outid;

    private String requestid;

    private String code;

    private String message;

    private String bizid;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPhonenumbers() {
        return phonenumbers;
    }

    public void setPhonenumbers(String phonenumbers) {
        this.phonenumbers = phonenumbers == null ? null : phonenumbers.trim();
    }

    public String getSignname() {
        return signname;
    }

    public void setSignname(String signname) {
        this.signname = signname == null ? null : signname.trim();
    }

    public String getTemplatecode() {
        return templatecode;
    }

    public void setTemplatecode(String templatecode) {
        this.templatecode = templatecode == null ? null : templatecode.trim();
    }

    public String getTemplateparam() {
        return templateparam;
    }

    public void setTemplateparam(String templateparam) {
        this.templateparam = templateparam == null ? null : templateparam.trim();
    }

    public String getSmsupextendcode() {
        return smsupextendcode;
    }

    public void setSmsupextendcode(String smsupextendcode) {
        this.smsupextendcode = smsupextendcode == null ? null : smsupextendcode.trim();
    }

    public Long getOutid() {
        return outid;
    }

    public void setOutid(Long outid) {
        this.outid = outid;
    }

    public String getRequestid() {
        return requestid;
    }

    public void setRequestid(String requestid) {
        this.requestid = requestid == null ? null : requestid.trim();
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code == null ? null : code.trim();
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message == null ? null : message.trim();
    }

    public String getBizid() {
        return bizid;
    }

    public void setBizid(String bizid) {
        this.bizid = bizid == null ? null : bizid.trim();
    }
}