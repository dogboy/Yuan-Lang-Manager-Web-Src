package com.yuanlang.facade.model.socket;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by chenpeng on 2017\8\18 0018.
 * 页面消息返回结果
 */
public class MessageResultVo {

    //是否是重要消息，只有通知公告才有消息级别划分
    //用来区别显示的方式
    private boolean isImportantNotice;
    //待办消息数量
    private int stayDealWithNum;
    //待我知晓数量
    private int  stayKnowNum;
    //消息是重要消息还是紧急消息
    private int messageLevel;
    //通知公告数量
    private int noticeAnnouncementNum;
    //token
    private String pfToken;
    //消息详情主体,如果是重要消息，里面只存一个元素，如果是不重要的消息，里面就存所有元素
    //private List<NoticeAnnouncementInfo> noticeAnnouncementInfo;

    public String getPfToken() {
        return pfToken;
    }

    public void setPfToken(String pfToken) {
        this.pfToken = pfToken;
    }

    /**
     * @return
     */
    public int getMessageLevel() {
        return messageLevel;
    }
    /**
     * @return
     */
    public void setMessageLevel(int messageLevel) {
        this.messageLevel = messageLevel;
    }
    /**
     * @return
     */
    public int getNoticeAnnouncementNum() {
        return noticeAnnouncementNum;
    }
    /**
     * @return
     */
    public void setNoticeAnnouncementNum(int noticeAnnouncementNum) {
        this.noticeAnnouncementNum = noticeAnnouncementNum;
    }
    /**
     * @return
     */
    public int getStayDealWithNum() {
        return stayDealWithNum;
    }
    /**
     * @return
     */
    public void setStayDealWithNum(int stayDealWithNum) {
        this.stayDealWithNum = stayDealWithNum;
    }
    /**
     * @return
     */
    public int getStayKnowNum() {
        return stayKnowNum;
    }
    /**
     * @return
     */
    public void setStayKnowNum(int stayKnowNum) {
        this.stayKnowNum = stayKnowNum;
    }
    /**
     * @return
     */
    public boolean getIsImportantNotice() {
        return isImportantNotice;
    }
    /**
     * @return
     */
    public void setIsImportantNotice(boolean isImportantNotice) {
        this.isImportantNotice = isImportantNotice;
    }
    /**
     * @return
     */
/*    public List<NoticeAnnouncementInfo> getNoticeAnnouncementInfo() {
        return noticeAnnouncementInfo;
    }
    *//**
     * @return
     *//*
    public void setNoticeAnnouncementInfos(List<NoticeAnnouncementInfo> noticeAnnouncementInfo) {
        this.noticeAnnouncementInfo = noticeAnnouncementInfo;
    }*/

}
