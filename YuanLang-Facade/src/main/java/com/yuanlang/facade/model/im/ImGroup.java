package com.yuanlang.facade.model.im;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * layIM分组表
 */
public class ImGroup implements Serializable {

    private Long id;

    private String groupname;

    private String avatar;

    private String sign;

    private Date timestamp;

    private Integer status; // 1:好友分组 ; 2:群聊分组 ；3:私聊分组

    private Integer grouptype;

    private Long ower;

    List <ImUser> list = new ArrayList<ImUser>(); // 组下面的人员


    public String getGroupname() {
        return groupname;
    }

    public void setGroupname(String groupname) {
        this.groupname = groupname == null ? null : groupname.trim();
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar == null ? null : avatar.trim();
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign == null ? null : sign.trim();
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getGrouptype() {
        return grouptype;
    }

    public void setGrouptype(Integer grouptype) {
        this.grouptype = grouptype;
    }


    public List<ImUser> getList() {
        return list;
    }

    public void setList(List<ImUser> list) {
        this.list = list;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOwer() {
        return ower;
    }

    public void setOwer(Long ower) {
        this.ower = ower;
    }
}