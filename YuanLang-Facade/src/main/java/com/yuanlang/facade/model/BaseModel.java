package com.yuanlang.facade.model;

import java.io.Serializable;

/**
 * company 重庆源狼软件科技有限责任公司
 * FileName BaseModel
 * Package com.yuanlang.facade.model.system
 * Description
 * author Administrator
 * create 2018-05-16 19:47
 * version V1.0
 */

public class BaseModel implements Serializable {
    int page = 0 ;
    int limit = 10 ;
    public int getPage() {
        return page;
    }
    public void setPage(int page) {
        this.page = page;
    }
    public int getLimit() {
        return limit;
    }
    public void setLimit(int limit) {
        this.limit = limit;
    }
}
