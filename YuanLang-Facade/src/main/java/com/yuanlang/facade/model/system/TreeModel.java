package com.yuanlang.facade.model.system;

import java.io.Serializable;
import java.util.List;

/**
 * @author hanhq
 * @return {code:success;msg;msgContent:data}
 * @throws {如果有异常说明请填写}
 * @methodname
 * @Description 树形数据模型
 * @create 2018/5/19
 */
public class TreeModel implements Serializable {


    private String name;// 节点名称
    private Long id;// 节点ID
    private String code;// 编码
    private List<TreeModel> children;// 子节点
    private Boolean checked;// 默认是否选中，true为选中，false与不填都为不选中 （选填）
    private Boolean disabled;// 是否可用，true为不可用，false与不填都为可用 （选填）


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public List<TreeModel> getChildren() {
        return children;
    }

    public void setChildren(List<TreeModel> children) {
        this.children = children;
    }

    public Boolean getChecked() {
        return checked;
    }

    public void setChecked(Boolean checked) {
        this.checked = checked;
    }

    public Boolean getDisabled() {
        return disabled;
    }

    public void setDisabled(Boolean disabled) {
        this.disabled = disabled;
    }
}
