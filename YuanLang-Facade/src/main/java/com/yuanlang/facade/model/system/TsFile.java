package com.yuanlang.facade.model.system;

import java.io.Serializable;

/**
 * company 重庆源狼软件科技有限责任公司
 * FileName TsFile
 * Package com.yuanlang.facade.model.system
 * Description 文件相关信息
 * author Administrator
 * create 2018-06-01 18:57
 * version V1.0
 */

/**
 * 源狼软件上传返回文件数据封装类
 */
public class TsFile implements Serializable {


    public String fileName ; //   文件名称 header

    private String filePath ; //  文件路径 /XX/XXX/XX/XX.jpg

    private String fileType ; //  文件类型 .jpg

    private Long fileSize ;   //  文件大小 10000

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public Long getFileSize() {
        return fileSize;
    }

    public void setFileSize(Long fileSize) {
        this.fileSize = fileSize;
    }
}
