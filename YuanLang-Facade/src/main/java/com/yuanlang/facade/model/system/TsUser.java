package com.yuanlang.facade.model.system;

import com.yuanlang.facade.model.BaseModel;

import java.io.Serializable;
import java.util.Date;

/**
* @TsUser
* @Description 人员组织机构分页
* @author 黄学乾
* @create 2018/4/23/023 15:47
* @param  * @param null
* @throws {如果有异常说明请填写}
* @return
*/
public class TsUser extends BaseModel implements Serializable {
    // 用户ID
    private Long userId;
    // 姓名
    private String userName;
    // 登录名
    private String loginName;
    // 密码
    private String password;
    // 联系电话
    private String telNumber;
    // 微信
    private String weixinId;
    // QQ
    private String qqNumber;
    // 邮件
    private String email;
    // 组织机构
    private long orgId;
    // 组织名称
    private String orgName;
    // 部门名称
    private long deptId;
    // 部门名称
    private String deptName;
    // 是否显示
    private Integer isShow;
    // 最后一次登录时间
    private Date lastLoginTime;
    //登录方式
    private Integer lastLoginType;
    //账号状态 0：正常 ； 1：锁定
    private Integer islock;
    //创建人Id
    private long creatorId;
    //创建人名称
    private String creator;
    // 创建时间
    private Date createTime;
    //修改人ID
    private long modifyerId;
    // 修改时间
    private Date modifyTime;
    //修改人
    private String modifyer;
    //状态 0：正常；1删除
    private Integer status;
    // 备注
    private String remark;
    // 拓展1
    private String remark1;
    // 拓展2
    private String remark2;
    // 拓展3
    private String remark3;
    // 拓展4
    private String remark4;

    private Boolean isAdmin;// 扩展字段，是否为管理员


    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName == null ? null : loginName.trim();
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password == null ? null : password.trim();
    }

    public String getTelNumber() {
        return telNumber;
    }

    public void setTelNumber(String telNumber) {
        this.telNumber = telNumber == null ? null : telNumber.trim();
    }

    public String getWeixinId() {
        return weixinId;
    }

    public void setWeixinId(String weixinId) {
        this.weixinId = weixinId == null ? null : weixinId.trim();
    }

    public String getQqNumber() {
        return qqNumber;
    }

    public void setQqNumber(String qqNumber) {
        this.qqNumber = qqNumber == null ? null : qqNumber.trim();
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email == null ? null : email.trim();
    }


    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName == null ? null : orgName.trim();
    }

    public long getDeptId() {
        return deptId;
    }

    public void setDeptId(long deptId) {
        this.deptId = deptId;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public Integer getIsShow() {
        return isShow;
    }

    public void setIsShow(Integer isShow) {
        this.isShow = isShow;
    }

    public Date getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(Date lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    public Integer getLastLoginType() {
        return lastLoginType;
    }

    public void setLastLoginType(Integer lastLoginType) {
        this.lastLoginType = lastLoginType;
    }

    public Integer getIslock() {
        return islock;
    }

    public void setIslock(Integer islock) {
        this.islock = islock;
    }


    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator == null ? null : creator.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    public String getModifyer() {
        return modifyer;
    }

    public void setModifyer(String modifyer) {
        this.modifyer = modifyer == null ? null : modifyer.trim();
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getRemark1() {
        return remark1;
    }

    public void setRemark1(String remark1) {
        this.remark1 = remark1 == null ? null : remark1.trim();
    }

    public String getRemark2() {
        return remark2;
    }

    public void setRemark2(String remark2) {
        this.remark2 = remark2 == null ? null : remark2.trim();
    }

    public String getRemark3() {
        return remark3;
    }

    public void setRemark3(String remark3) {
        this.remark3 = remark3 == null ? null : remark3.trim();
    }

    public String getRemark4() {
        return remark4;
    }

    public void setRemark4(String remark4) {
        this.remark4 = remark4 == null ? null : remark4.trim();
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public long getOrgId() {
        return orgId;
    }

    public void setOrgId(long orgId) {
        this.orgId = orgId;
    }



    public long getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(long creatorId) {
        this.creatorId = creatorId;
    }

    public long getModifyerId() {
        return modifyerId;
    }

    public void setModifyerId(long modifyerId) {
        this.modifyerId = modifyerId;
    }

    public Boolean getAdmin() {
        return isAdmin;
    }

    public void setAdmin(Boolean admin) {
        isAdmin = admin;
    }
}