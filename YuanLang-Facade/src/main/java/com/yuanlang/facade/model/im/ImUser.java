package com.yuanlang.facade.model.im;

import java.io.Serializable;
import java.util.Date;

/**
 * layIM用户信息表，自动添加 或者 系统同步
 */
public class ImUser implements Serializable {

    private long id; //用户id 与 系统用户一致

    private String username; // 用户名称

    private String status; // 用户状态 online,hide

    private String sign; // 头像

    private Date timestamp; // 服务时间戳

    private String avatar; // 签名

    private String datafrom; //用户来源

    private String pconline; // 用户pc端在线

    private String mobileonline; // 用户移动端在线

    private Long userStatus ; // 用户状态，2是属于小组 还是属于1好友

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username == null ? null : username.trim();
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status == null ? null : status.trim();
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign == null ? null : sign.trim();
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar == null ? null : avatar.trim();
    }

    public String getDatafrom() {
        return datafrom;
    }

    public void setDatafrom(String datafrom) {
        this.datafrom = datafrom == null ? null : datafrom.trim();
    }

    public String getPconline() {
        return pconline;
    }

    public void setPconline(String pconline) {
        this.pconline = pconline == null ? null : pconline.trim();
    }

    public String getMobileonline() {
        return mobileonline;
    }

    public void setMobileonline(String mobileonline) {
        this.mobileonline = mobileonline == null ? null : mobileonline.trim();
    }


    public Long getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(Long userStatus) {
        this.userStatus = userStatus;
    }
}