package com.yuanlang.facade.model.system;

/**
 * @author hanhq
 * @return {code:success;msg;msgContent:data}
 * @throws {如果有异常说明请填写}
 * @methodname
 * @Description 菜单属性扩展类
 * @create 2018/5/21
 */
public class TsMenuV extends TsMenu {

    private String parentMenuName;// 上级菜单名称

    public String getParentMenuName() {
        return parentMenuName;
    }

    public void setParentMenuName(String parentMenuName) {
        this.parentMenuName = parentMenuName;
    }
}
