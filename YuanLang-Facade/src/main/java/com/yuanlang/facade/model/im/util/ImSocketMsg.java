package com.yuanlang.facade.model.im.util;

import java.io.Serializable;

/**
 * company 重庆源狼软件科技有限责任公司
 * FileName SocketMsg
 * Package com.yuanlang.web.socket
 * Description 消息类型数据格式
 * author Administrator
 * create 2018-05-19 14:11
 * version V1.0
 */
public class ImSocketMsg implements Serializable{

    /**
     * type = onOpen 连接
     * type = sendMessage 发送消息
     * type = onClose 关闭
     * type = onMessage 接受信息
     * type = sys // 系统消息
     */
    String type ;  // 消息类型 : // sendMessage layim 发送消息

    Object data ;  // 消息数据 :存放对象类型

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
